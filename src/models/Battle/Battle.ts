import MonsterCard from "../Card/Monster/MonsterCard"
import Deck from "../Card/Deck"
import Game from "../Game"
import Player from "../Player/Player"
import BattleModifier from "./Modifier"
import PassiveSkill from "../Player/Skill/PassiveSkill"
import PlayerRaceType from "../Player/PlayerRace/PlayerRaceType"
import PlayerClassType from "../Player/PlayerClass/PlayerClassType"
import Gender from "../Player/Gender"
import MODIFIERS from "./MODIFIERS"
import IMonsterPassiv from "../Card/Monster/IMonsterPassiv"
import TurnPhase from "../TurnPhase"
import GameController from "../../controllers/GameController"
import RemovalAction from "../Card/Consumable/RemovalAction"

class Battle {
    private _game: Game
    private _allies: Array<{ player: Player, treasureRewards: Array<number> }> = []
    enemies: Deck<MonsterCard> = new Deck(
        [],
        (card: MonsterCard) => this.addEnemy(card),
        (card: MonsterCard, index: number) => this.removeEnemy(card, index)
    )
    alliesModifiers: Array<BattleModifier> = []
    enemiesModifiers: Array<BattleModifier> = []

    get canHelp() {
        return this.alliesModifiers.find(modifier => modifier?.id === MODIFIERS.NO_HELP().id) === undefined && this._allies.length < 2
    }

    get allies() {
        return this._allies
    }

    get enemiesPassivs(): Array<IMonsterPassiv> {
        const passivs: Array<IMonsterPassiv> = []
        this.enemies.cards.forEach(card => {
            passivs.push(...(card.passivs || []))
        })

        return passivs
    }

    get alliesPower() {
        const basicPower = this._allies.map(({ player }) => player.level).reduce((a, b) => a + b, 0)
        const totalPower = this._allies.map(({ player }) => player.power).reduce((a, b) => a + b, 0)
        const modifiedPower = (this.alliesModifiers || []).map(modifier => (modifier.power || 0)).reduce(
            (a, b) => a + b,
            this._allies[0].player.hasPassiveSkill(PassiveSkill.WIN_TIES_IN_COMBAT) || (this._allies[1] && this._allies[1].player.hasPassiveSkill(PassiveSkill.WIN_TIES_IN_COMBAT)) ? 1 : 0
        )

        if (this.alliesModifiers.find(modifier => modifier?.id === MODIFIERS.NO_BONUS().id)) {
            return basicPower + modifiedPower
        }

        if (this.alliesModifiers.find(modifier => modifier?.id === MODIFIERS.NO_LEVEL().id)) {
            return totalPower - basicPower + modifiedPower
        }

        return totalPower + modifiedPower
    }

    get enemiesPower() {
        const basicPower = this.enemies.cards.map(monster => monster.level).reduce((a, b) => a + b, 0)
        let modifiedPower = (this.enemiesModifiers || []).map(modifier => (modifier.power || 0)).reduce((a, b) => a + b, 0)
        return basicPower + modifiedPower
    }

    get enemiesTreasures() {
        const basicTreasures = this.enemies.cards.map(monster => monster.reward.treasures).reduce((a, b) => a + b, 0)
        let modifiedTreasures = (this.enemiesModifiers || []).map(modifier => (modifier.treasures ? modifier.treasures[0] : 0)).reduce((a, b) => a + b, 0)
        return basicTreasures + modifiedTreasures
    }

    constructor(
        game: Game,
        player: Player,
        monster: MonsterCard
    ) {
        this._game = game
        this.newAlly(player, [monster.reward.treasures])
        this.enemies.add(monster)
    }

    checkForMonsterPassivsApplication(monster: MonsterCard) {
        if (!monster.passivs) {
            return
        }

        for (let index = 0; index < monster.passivs.length; index++) {
            const passiv = monster.passivs[index]

            if (passiv.restriction) {
                const alliesLevel: Array<number> = []
                const alliesRaces: Array<PlayerRaceType> = []
                const alliesClasses: Array<PlayerClassType> = []
                const alliesGenders: Array<Gender> = []
                const alliesPassivSkills: Array<PassiveSkill> = []
                this._allies.forEach(({ player }) => {
                    alliesLevel.push(player.level)
                    alliesRaces.push(...player.races)
                    alliesClasses.push(...player.classes)
                    alliesGenders.push(player.gender)
                    alliesPassivSkills.push(...player.passivSkills)
                })
                this.alliesModifiers
                    .filter(modifier => modifier.passiv)
                    .forEach(modifier => {
                        if (modifier.passiv) {
                            alliesPassivSkills.push(modifier.passiv)
                        }
                    })

                switch (typeof passiv.restriction) {
                    case "string":
                        if (alliesRaces.find(race => passiv?.restriction === race) === undefined && alliesClasses.find(_class => passiv?.restriction === _class) === undefined && alliesGenders.find(gender => passiv?.restriction === gender) === undefined && alliesPassivSkills.find(skill => passiv?.restriction === skill) === undefined) {
                            continue
                        }
                        break
                    case "object":
                        if (!passiv.restriction.filter(classOrRace => alliesRaces.find(race => classOrRace === race) !== undefined && alliesClasses.find(_class => classOrRace === _class) !== undefined).length) {
                            continue
                        }
                        break
                    case "number":
                        if (alliesLevel.find(level => level <= (passiv.restriction as number)) === undefined) {
                            continue
                        }
                        break
                    default:
                        break
                }
            }

            if (passiv.modifier && !this.alliesModifiers.find(modifier => passiv.modifier?.id === modifier.id) && !this.enemiesModifiers.find(modifier => passiv.modifier?.id === modifier.id)) {
                return this.newModifier(passiv.modifier, MonsterCard.modifierInEnemySide(passiv.modifier))
            }
        }
    }

    addEnemy(card: MonsterCard) {
        this._game.websocket.broadcast({ title: 'battle-add-enemy', data: { cardId: card.id } })
        this.checkForMonsterPassivsApplication(card)
        if (card.encounter) {
            card.encounter(this._allies.map(({ player }) => player))
        }
        if (this._allies[0].treasureRewards.length > this.enemies.cards.length) {
            this._allies[0].treasureRewards.splice(1, 0, card.reward.treasures)
        } else {
            this._allies[0].treasureRewards.push(card.reward.treasures)
        }
    }

    removeEnemy(card: MonsterCard, index: number) {
        this._game.websocket.broadcast({ title: 'battle-remove-enemy', data: { cardId: card.id } })
        if (this.enemies.cards.length === 0) {
            this._game.turnPhase = TurnPhase.STANDBY
            GameController.annonceNewTurn(this._game)
        } else if (this._game.turnPhase === TurnPhase.COMBAT) {
            card.passivs?.forEach(passiv => {
                if (passiv.modifier) {
                    if (this.alliesModifiers.find(modifier => modifier.id === passiv.modifier?.id)) {
                        this.alliesModifiers = this.alliesModifiers.filter(modifier => modifier.id !== passiv.modifier?.id)
                        this._game.websocket.broadcast({ title: 'battle-remove-ally-modifier', data: { modifierId: passiv.modifier?.id } })
                    } else if (this.enemiesModifiers.find(modifier => modifier.id === passiv.modifier?.id)) {
                        this.enemiesModifiers = this.enemiesModifiers.filter(modifier => modifier.id !== passiv.modifier?.id)
                        this._game.websocket.broadcast({ title: 'battle-remove-enemy-modifier', data: { modifierId: passiv.modifier?.id } })
                    }
                }
            })
            this._allies.forEach(ally => {
                ally.treasureRewards.splice(index, 1)
            })
        }
    }

    newAlly(player: Player, treasureRewards: Array<number>): boolean {
        if (this.alliesModifiers.find(modifier => modifier?.id === MODIFIERS.NO_HELP().id)) {
            return false
        }

        if (this._allies.length >= 2) {
            return false
        }

        const totalTreasureReward = treasureRewards.reduce((a, b) => a + b, 0)
        if (this._allies.length > 0 && totalTreasureReward > this.enemiesTreasures) {
            return false
        }

        if (this._allies[0]) {
            treasureRewards.forEach((treasureReward, index) => {
                this._allies[0].treasureRewards[index] -= treasureReward
            })
        }

        this._allies.push({ player, treasureRewards: this._allies.length ? treasureRewards : [] })
        this._game.websocket.broadcast({ title: 'battle-add-ally', playerId: player.deviantUser?.id })
        this.enemies.cards.forEach(monster => this.checkForMonsterPassivsApplication(monster))

        return true
    }

    removeAlly(player: Player): boolean {
        this._allies = this._allies.filter(({ player }) => player.deviantUser?.id !== player.deviantUser?.id)
        this._game.websocket.broadcast({ title: 'battle-remove-ally', playerId: player.deviantUser?.id })

        if (this._allies.length === 0) {
            GameController.standByPhase(this._game)
            GameController.annonceNewTurn(this._game)
        } else if (this._game.turnPhase === TurnPhase.COMBAT) {
            this.enemies.cards.forEach((monster, index) => {
                this._allies[0].treasureRewards[index] = monster.reward.treasures
            })
            this.enemies.cards.forEach(monster => this.checkForMonsterPassivsApplication(monster))
        }

        return true
    }

    replaceAlly(oldPlayer: Player, newPlayer: Player) {
        const allyIndex = this._allies.findIndex(ally => ally.player.deviantUser?.id === oldPlayer.deviantUser?.id)
        this._allies[allyIndex].player = newPlayer
        this._game.websocket.broadcast({ title: 'battle-replace-ally', data: { players: [(oldPlayer.deviantUser?.id || 'X'), (newPlayer.deviantUser?.id || 'X')] } })
    }

    newModifier(modifier: BattleModifier, isEnemySide: boolean, override?: Partial<BattleModifier>) {
        modifier = { ...modifier, ...override } as BattleModifier
        (isEnemySide ? this.enemiesModifiers : this.alliesModifiers).push(modifier)
        if (modifier.treasures && modifier.treasures[0] > 0) {
            this._allies[0].treasureRewards.push(modifier.treasures[0])
        }
        this._game.websocket.broadcast({ title: 'battle-add-' + (isEnemySide ? 'enemy' : 'ally') + '-modifier', data: { modifierId: modifier.id, ...override } })
        if (modifier.passiv) {
            this.enemies.cards.forEach(monster => this.checkForMonsterPassivsApplication(monster))
        }
    }

    negotiateReward(treasureReward: Array<number>) {
        // if (this._allies[0].player.deviantUser?.id === this._allies[1].player.deviantUser?.id) {
        //     return
        // }

        // if (this._allies.length === 2 && this._allies[0].treasureRewards >= treasureReward) {
        //     this._allies[0].treasureRewards -= treasureReward
        //     this._allies[1].treasureRewards += treasureReward
        // }
    }

    fight(): boolean {
        if (this._allies[0].player.deviantUser?.id !== this._game.whoIsPlaying().deviantUser?.id) {
            this._game.whoIsPlaying().draw(1, false)
        }
        if (this.alliesAreWinning) {
            this.win()
            return true
        } else {
            this.lose()
            return false
        }
    }

    get alliesAreWinning() {
        return this.alliesPower > this.enemiesPower
    }

    removeResidualCursesAfterBattle() {
        this._allies
            .filter(({ player }) => player.curses.cards.length)
            .forEach(({ player }) => {
                const afterBattleCurses = player.curses.cards
                    .filter(card => card.residualEffect && card.residualEffect.removalAction === RemovalAction.AFTER_BATTLE)
                afterBattleCurses.forEach(card => this._game.discard.add(card))
            })
    }

    win() {
        this._game.websocket.broadcast({ title: 'battle-win', playerId: this._allies[0].player.deviantUser?.id })
        this.removeResidualCursesAfterBattle()

        this._allies.forEach(({ player, treasureRewards: treasureReward }) => {
            player.draw(treasureReward.reduce((a, b) => a + b, 0), true)
        })
        this.enemies.cards.forEach((monster, index) => {
            this._allies[0].player.kill(monster)
            if (this.enemies.cards.length === 2 && this.enemies.cards[0].id === this.enemies.cards[1].id) {
                this.removeEnemy(monster, index)
            } else {
                this.enemies.removeCardCallback = undefined
                this._game.discard.add(monster)
                this.enemies.removeCardCallback = (card: MonsterCard, index: number) => this.removeEnemy(card, index)
            }
        })
        this.alliesModifiers.filter(modifier => modifier.experience).forEach(modifier => {
            this._allies[0].player.level += modifier.experience as number
        })
        if (this._allies[1] && this._allies[1].player.hasPassiveSkill(PassiveSkill.LEVEL_UP_ON_KILL_HELP)) {
            this._allies[1].player.level++
        }

        this._game.turnPhase = TurnPhase.STANDBY
    }

    lose() {
        this._game.websocket.broadcast({ title: 'battle-lose', playerId: this._allies[0].player.deviantUser?.id })
        this.removeResidualCursesAfterBattle()

        this.enemies.cards.filter(monster => monster.passivs?.find(passiv => passiv.modifier?.id === MODIFIERS.NO_FLEE().id) !== undefined).forEach((monster, index) => {
            this._allies.forEach(({ player }, index) => {
                if (!(index === 1 && player.deviantUser?.id === this._game.whoIsPlaying().deviantUser?.id)) {
                    monster.explode(player, false)
                }
            })
            if (this.enemies.cards.length === 2 && this.enemies.cards[0].id === this.enemies.cards[1].id) {
                this.removeEnemy(monster, index)
            } else {
                this._game.discard.add(monster)
            }
        })

        this.enemies.cards.filter(monster => monster.passivs?.find(passiv => passiv.modifier?.id === MODIFIERS.AUTO_FLEE().id) !== undefined).forEach((monster, index) => {
            const levelRestriction = monster.passivs?.find(passiv => passiv.modifier?.id === MODIFIERS.AUTO_FLEE().id)?.restriction as number
            const playersAffected = this._allies.filter(({ player }) => player.level <= levelRestriction)
            if (playersAffected.length === this._allies.length) {
                this._allies.forEach(({ player }, index) => {
                    if (!(index === 1 && player.deviantUser?.id === this._game.whoIsPlaying().deviantUser?.id)) {
                        monster.explode(player, true)
                    }
                })
                if (this.enemies.cards.length === 2 && this.enemies.cards[0].id === this.enemies.cards[1].id) {
                    this.removeEnemy(monster, index)
                } else {
                    this._game.discard.add(monster)
                }
            }
        })

        if (!this.enemies.cards.length) {
            GameController.standByPhase(this._game)
        } else {
            this._allies.forEach(({ player }) => {
                player.roll()
            })
            this._game.turnPhase = TurnPhase.FLEE
        }
    }

    concludeFleeRolls() {
        const monster = this.enemies.cards[0]
        if (!monster) {
            return
        }

        this._allies.forEach(({ player }) => {
            if (player.rolling) {
                monster.explode(player, player.rolling?.dice + player.rolling?.modifier >= 5)
                if (!player.rolling.callback) {
                    player.rolling = undefined
                }
            }
        })

        if (this.enemies.cards.length === 2 && this.enemies.cards[0].id === this.enemies.cards[1].id) {
            this.removeEnemy(monster, 0)
        } else if (this.enemies.cards.length === 1) {
            this.enemies.removeCardCallback = undefined
            this._game.discard.add(monster)
            this.enemies.removeCardCallback = (card: MonsterCard, index: number) => this.removeEnemy(card, index)
        } else {
            this._game.discard.add(monster)
        }
    }

}

export default Battle