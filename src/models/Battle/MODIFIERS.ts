import PassiveSkill from "../Player/Skill/PassiveSkill";
import BattleModifier from "./Modifier";

/**
 * ID nomenclature :
 * 1-99 = from spells
 * 100-999 = from cards
 * 1000+ = from monsters
 */

const MODIFIERS = {
    // 1-100 SKILL
    PRIEST_TURNING: () => new BattleModifier(1, 3),
    THIEF_BACKSTAB: () => new BattleModifier(2, -2),
    WAR_BERSERKER_RAGE: () => new BattleModifier(3, 1),
    MAGE_FLIGHT: () => new BattleModifier(4, undefined, undefined, 1),
    // 100-999 CONSUMABLES
    PRETTY_BALLOONS: () => new BattleModifier(100, 5),
    INTELLIGENT: () => new BattleModifier(101, 5, [1]),
    POLLYMORPH_POTION: () => new BattleModifier(102),
    POTION_OF_IDIOTIC_BRAVERY: () => new BattleModifier(103, 2),
    YUPPIE_WATER: () => new BattleModifier(104, 0),
    FREEZING_EXPLOSIVE_POTION: () => new BattleModifier(105, 3),
    FLAMING_POISON_POTION: () => new BattleModifier(106, 3, undefined, undefined, undefined, PassiveSkill.FIRE_DAMAGE),
    SLEEP_POTION: () => new BattleModifier(107, 2),
    ELECTRIC_RADIOACTIVE_ACID_POTION: () => new BattleModifier(108, 5),
    NASTY_TASTING_SPORTS_DRINK: () => new BattleModifier(109, 2),
    COTION_OF_PONFUSION: () => new BattleModifier(110, 3),
    POTION_OF_HALITOSIS: () => new BattleModifier(111, 2),
    MAGIC_MISSILE: () => new BattleModifier(112, 5),
    BABY: () => new BattleModifier(113, -5),
    ENRAGED: () => new BattleModifier(114, 5, [1]),
    ANCIENT: () => new BattleModifier(115, 10, [2]),
    HUMONGOUS: () => new BattleModifier(116, 10, [2]),
    SWAP_SEX: () => new BattleModifier(117, -5),
    CHICKEN_ON_THE_HEAD: () => new BattleModifier(118, undefined, undefined, -1),
    // 1000+ MONSTERS
    DROOLING_SLIME: () => new BattleModifier(1000, 4),
    FLYING_FROGS: () => new BattleModifier(1001, undefined, undefined, -1),
    GELATINOUS_OCTAHEDRON: () => new BattleModifier(1002, undefined, undefined, 1),
    LAME_GOBLIN: () => new BattleModifier(1003, undefined, undefined, 1),
    HARPIES: () => new BattleModifier(1004, 5),
    LARGE_ANGRY_CHICKEN: () => new BattleModifier(1005, undefined, undefined, undefined, 1),
    LEPERCHAUN: () => new BattleModifier(1006, 5),
    MAUL_RAT: () => new BattleModifier(1007, 3),
    MR_BONES: () => new BattleModifier(1008, undefined, undefined, -1),
    PLATYCORE: () => new BattleModifier(1009, 6),
    POTTED_PLANT: () => new BattleModifier(1010, undefined, [1]),
    SHRIEKING_GEEK: () => new BattleModifier(1011, 6),
    SNAILS_ON_SPEED: () => new BattleModifier(1012, undefined, undefined, -2),
    UNDEAD_HORSE: () => new BattleModifier(1013, 5),
    FACE_SUCKER: () => new BattleModifier(1014, 6),
    ORCS: () => new BattleModifier(1015, 6),
    TONGUE_DEMON: () => new BattleModifier(1016, 4),
    BIG_FOOT: () => new BattleModifier(1017, 3),
    UAIH: () => new BattleModifier(1018, 4),
    SQUIDILLA: () => new BattleModifier(1019, 4),
    MEAN_TREANT: () => new BattleModifier(1020, 5),
    NO_BONUS_EXCEPT_ARMOR: () => new BattleModifier(9994),
    NO_LEVEL: () => new BattleModifier(9995),
    NO_HELP: () => new BattleModifier(9996),
    NO_BONUS: () => new BattleModifier(9997),
    NO_FLEE: () => new BattleModifier(9998),
    AUTO_FLEE: () => new BattleModifier(9999),
}

export default MODIFIERS