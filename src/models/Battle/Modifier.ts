import PassiveSkill from "../Player/Skill/PassiveSkill"

class BattleModifier {
    id: number
    power?: number
    treasures?: Array<number>
    flee?: number
    experience?: number
    passiv?: PassiveSkill

    constructor(
        id: number,
        power?: number,
        treasures?: Array<number>,
        flee?: number,
        experience?: number,
        passiv?: PassiveSkill
    ) {
        this.id = id
        this.flee = flee
        this.treasures = treasures
        this.power = power
        this.experience = experience
        this.passiv = passiv
    }
}

export default BattleModifier