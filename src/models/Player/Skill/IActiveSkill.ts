import Player from "../Player";
import TurnPhase from "../../TurnPhase";
import ICard from "../../Card/ICard";
import Target from "../../Card/Target";

interface IActiveSkill {
  id: number
  usageByTurn: number
  canBeTriggered: Array<TurnPhase>

  activate(launcher: Player, targets: Target[]): boolean
  getTargets(launcher: Player): Target[]
}

export default IActiveSkill