import IActiveSkill from "./IActiveSkill";
import Player from "../Player";
import TurnPhase from "../../TurnPhase";
import MODIFIERS from "../../Battle/MODIFIERS";
import Target from "../../Card/Target";
import Choice from "../../Card/Choice";

class Backstab implements IActiveSkill {
  id: number = 3
  usageByTurn: number = 1
  canBeTriggered: Array<TurnPhase> = [TurnPhase.COMBAT]

  activate(launcher: Player, targets: Target[]): boolean {
    if (!this.getTargets(launcher).length) {
      throw "You can't launch this spell in this phase !"
    }

    if (launcher.currentGame.currentBattle) {
      launcher.currentGame.choiceCallbacks.push({
        sendChoices: () => {
          const choices: Choice[] = launcher.hand.cards.map(card => ({ name: card.name, cardId: card.id, isVisible: true }))

          launcher.websocket?.send(
            {
              title: 'show-choices',
              data: {
                choices,
                allowCancel: true,
                skills: [this.id],
                name: 'DEFAUSSE UNE CARTE'
              }
            }
          )
        },
        choiceCallback: (choices: Choice[]) => {
          if (!choices.length) {
            return false
          }

          let card = launcher.hand.cards.find(cardItem => cardItem.id === choices[0].cardId)

          if (card) {
            launcher.currentGame.currentBattle?.newModifier(MODIFIERS.THIEF_BACKSTAB(), false)
            launcher.activeSkillsUsedThisTurn.push(this)
            launcher.currentGame.discard.add(card)
            launcher.websocket?.broadcast({ title: "player-skill-triggered", data: { skills: [this.id], targets, cards: [card.id] }, playerId: launcher.deviantUser?.id })
          }

          return false
        }
      })

      return true
    }

    return false
  }

  getTargets(launcher: Player): Target[] {
    if (this.canBeTriggered.find(phase => phase === launcher.currentGame.turnPhase) === undefined) {
      return []
    }

    if (launcher.activeSkillsUsedThisTurn.filter(skill => skill.id === this.id).length >= this.usageByTurn) {
      return []
    }

    return [
      new Target('Le groupe')
    ]
  }

}

export default Backstab