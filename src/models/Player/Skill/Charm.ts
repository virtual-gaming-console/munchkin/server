import IActiveSkill from "./IActiveSkill";
import Player from "../Player";
import TurnPhase from "../../TurnPhase";
import Target from "../../Card/Target";
import Choice from "../../Card/Choice";

class Charm implements IActiveSkill {
  id: number = 6
  usageByTurn: number = 1
  canBeTriggered: Array<TurnPhase> = [TurnPhase.COMBAT, TurnPhase.FLEE]

  activate(launcher: Player, targets: Target[]): boolean {
    if (!this.getTargets(launcher).length) {
      throw "You can't launch this spell in this phase !"
    }

    const target = targets[0]
    const monsterId = target?.cards ? target?.cards[0] : undefined
    const monster = launcher.currentGame.currentBattle?.enemies.cards.find(monster => monster.id === monsterId)

    if (monster) {
      launcher.hand.cards.forEach(card => launcher.currentGame.discard.add(card))
      launcher.currentGame.discard.add(monster)
      launcher.draw(monster.reward.treasures, true)

      return true
    }

    return false
  }

  getTargets(launcher: Player): Target[] {
    if (this.canBeTriggered.find(phase => phase === launcher.currentGame.turnPhase) === undefined) {
      return []
    }

    if (launcher.activeSkillsUsedThisTurn.filter(skill => skill.id === this.id).length >= this.usageByTurn) {
      return []
    }

    if (launcher.currentGame.currentBattle?.allies.find(({ player }) => player.deviantUser?.id === launcher.deviantUser?.id) === undefined) {
      return []
    }

    return launcher.currentGame.currentBattle.enemies.cards.map(monster => new Target(monster.name, undefined, [monster.id]))
  }

}

export default Charm