import IActiveSkill from "./IActiveSkill";
import Player from "../Player";
import TurnPhase from "../../TurnPhase";
import Target from "../../Card/Target";
import Choice from "../../Card/Choice";
import GameController from "../../../controllers/GameController";

class Theft implements IActiveSkill {
  id: number = 4
  usageByTurn: number = 1
  canBeTriggered: Array<TurnPhase> = [TurnPhase.STANDBY]

  activate(launcher: Player, targets: Target[]): boolean {
    if (!this.getTargets(launcher).length) {
      throw "You can't launch this spell in this phase !"
    }

    const target = targets[0]
    const playerId = target?.players ? target?.players[0] : undefined
    const player = launcher.currentGame.players.find(player => player.deviantUser?.id === playerId)

    if (player && player.arsenalCards.length) {
      launcher.currentGame.choiceCallbacks.push({
        sendChoices: () => {
          const choices: Choice[] = launcher.hand.cards.map(card => ({ name: card.name, cardId: card.id, isVisible: true }))

          launcher.websocket?.send(
            {
              title: 'show-choices',
              data: {
                choices,
                name: 'DEFAUSSE UNE CARTE'
              }
            }
          )
        },
        choiceCallback: (choices: Choice[]) => {
          if (!choices.length) {
            return false
          }

          let cardToMil = launcher.hand.cards.find(cardItem => cardItem.id === choices[0].cardId)

          if (cardToMil) {
            launcher.currentGame.choiceCallbacks.push({
              sendChoices: () => {
                const choices: Choice[] = player.arsenalCards.map(card => ({ name: card.name, cardId: card.id, isVisible: true }))

                launcher.websocket?.send(
                  {
                    title: 'show-choices',
                    data: {
                      choices,
                      allowCancel: true,
                      skills: [this.id],
                      name: 'VOL UN OBJET DE ' + player.deviantUser?.nickname
                    }
                  }
                )
              },
              choiceCallback: (choices: Choice[]) => {
                if (!choices.length) {
                  return false
                }

                let cardToSteal = player.arsenalCards.find(card => card.id === choices[0].cardId)
                if (cardToSteal && cardToMil) {
                  launcher.currentGame.discard.add(cardToMil)

                  launcher.roll((dice: number) => {
                    if (dice >= 4 && cardToSteal) {
                      const target = targets[0]
                      target.cards = [cardToSteal.id]
                      launcher.websocket?.broadcast({ title: "player-skill-triggered", data: { skills: [this.id], targets, cards: [cardToMil?.id || 0] }, playerId: launcher.deviantUser?.id })
                      launcher.hand.add(cardToSteal)
                    } else {
                      launcher.level--
                    }
                  })
                }
                return false
              }
            })
          }

          return true
        }
      })

      return true
    }

    return false
  }

  getTargets(launcher: Player): Target[] {
    if (this.canBeTriggered.find(phase => phase === launcher.currentGame.turnPhase) === undefined) {
      return []
    }

    if (launcher.activeSkillsUsedThisTurn.filter(skill => skill.id === this.id).length >= this.usageByTurn) {
      return []
    }

    return launcher.currentGame.players
      .filter(player => player.deviantUser?.id !== launcher.deviantUser?.id && player.arsenalCards.length)
      .map(player => new Target((player.deviantUser?.nickname || 'X'), [(player.deviantUser?.id || 'X')]))
  }

}

export default Theft