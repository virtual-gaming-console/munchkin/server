import IActiveSkill from "./IActiveSkill";
import Player from "../Player";
import TurnPhase from "../../TurnPhase";
import Target from "../../Card/Target";
import Choice from "../../Card/Choice";
import MODIFIERS from "../../Battle/MODIFIERS";

class Flight implements IActiveSkill {
  id: number = 5
  usageByTurn: number = 1
  canBeTriggered: Array<TurnPhase> = [TurnPhase.FLEE]

  activate(launcher: Player, targets: Target[]): boolean {
    if (!this.getTargets(launcher).length) {
      throw "You can't launch this spell in this phase !"
    }

    if (launcher.currentGame.currentBattle) {
      launcher.currentGame.choiceCallbacks.push({
        sendChoices: () => {
          const choices: Choice[] = launcher.hand.cards.map(card => ({ name: card.name, cardId: card.id, isVisible: true }))

          launcher.websocket?.send(
            {
              title: 'show-choices',
              data: {
                choices,
                max: 3,
                allowCancel: true,
                skills: [this.id],
                name: 'DEFAUSSE JUSQU\'A TROIS CARTES'
              }
            }
          )
        },
        choiceCallback: (choices: Choice[]) => {
          if (!choices.length) {
            return false
          }

          choices.forEach(choice => {
            let card = launcher.hand.cards.find(cardItem => cardItem.id === choice.cardId)

            if (card) {
              launcher.currentGame.currentBattle?.newModifier(MODIFIERS.MAGE_FLIGHT(), false)
              launcher.activeSkillsUsedThisTurn.push(this)
              launcher.currentGame.discard.add(card)
            }
          })
          launcher.websocket?.broadcast({ title: "player-skill-triggered", data: { skills: [this.id], targets, cards: choices.map(choice => choice.cardId || 0) }, playerId: launcher.deviantUser?.id })


          return false
        }
      })

      return true
    }

    return false
  }

  getTargets(launcher: Player): Target[] {
    if (this.canBeTriggered.find(phase => phase === launcher.currentGame.turnPhase) === undefined) {
      return []
    }

    if (launcher.activeSkillsUsedThisTurn.filter(skill => skill.id === this.id).length >= this.usageByTurn) {
      return []
    }

    if (launcher.currentGame.currentBattle?.allies.find(({ player }) => player.deviantUser?.id === launcher.deviantUser?.id) === undefined) {
      return []
    }

    return [
      new Target('Moi')
    ]
  }

}

export default Flight