import IActiveSkill from "./IActiveSkill";
import Player from "../Player";
import TurnPhase from "../../TurnPhase";
import Target from "../../Card/Target";
import Choice from "../../Card/Choice";
import TreasureCard from "../../Card/TreasureCard";
import PassiveSkill from "./PassiveSkill";

class Sell implements IActiveSkill {
  id: number = 0
  usageByTurn: number = 0 /// TEMPORARY TO ZERO BECAUSE OF CLIENT BUG
  canBeTriggered: Array<TurnPhase> = [TurnPhase.FIRST_STANDBY, TurnPhase.STANDBY]

  activate(launcher: Player, targets: Target[]): boolean {
    if (!this.getTargets(launcher).length) {
      throw "You can't launch this spell in this phase !"
    }

    const allCards = [
      ...launcher.arsenalCards.filter(card => card.sellingPrice),
      ...launcher.hand.cards.filter(card => (card as TreasureCard).sellingPrice)
    ]
    const sellingMultiplier = launcher.hasPassiveSkill(PassiveSkill.SELL_ONE_ITEM_AT_DOUBLE_PRICE) ? 2 : 1
    if (allCards.map(cardItem => (cardItem as TreasureCard).sellingPrice).reduce((a, b) => a + b, 0) * sellingMultiplier >= 1000) {
      launcher.currentGame.choiceCallbacks.push({
        sendChoices: () => {
          const choices: Choice[] = allCards.map(card => ({ name: card.name, cardId: card.id, isVisible: true }))

          launcher.websocket?.send(
            {
              title: 'show-choices',
              data: {
                choices,
                min: 1000,
                allowCancel: true,
                skills: [this.id],
                name: 'VENDS DES OBJETS POUR 1000 GOLDS'
              }
            }
          )
        },
        choiceCallback: (choices: Choice[]) => {
          if (!choices.length) {
            return false
          }

          let cards = choices.map(choice => allCards.find(card => card.id === choice.cardId))
          console.log(choices)
          if (cards.map(cardItem => (cardItem as TreasureCard).sellingPrice).reduce((a, b) => a + b, 0) * sellingMultiplier >= 1000) {
            cards.forEach(card => {
              if (card) {
                launcher.currentGame.discard.add(card)
              }
            })
            launcher.level++
            launcher.activeSkillsUsedThisTurn.push(this)
            launcher.websocket?.broadcast({ title: "player-skill-triggered", data: { skills: [this.id], targets, cards: cards.map(cardItem => cardItem?.id || 0) }, playerId: launcher.deviantUser?.id })
          }

          return false
        }
      })
    }

    return true
  }

  getTargets(launcher: Player): Target[] {
    if (this.canBeTriggered.find(phase => phase === launcher.currentGame.turnPhase) === undefined) {
      return []
    }

    if (launcher.activeSkillsUsedThisTurn.filter(skill => skill.id === this.id).length >= this.usageByTurn) {
      return []
    }

    if (launcher.currentGame.whoIsPlaying().deviantUser?.id !== launcher.deviantUser?.id) {
      return []
    }

    const allCards = [
      ...launcher.arsenalCards.filter(card => card.sellingPrice),
      ...launcher.hand.cards.filter(card => (card as TreasureCard).sellingPrice)
    ]
    const sellingMultiplier = launcher.hasPassiveSkill(PassiveSkill.SELL_ONE_ITEM_AT_DOUBLE_PRICE) ? 2 : 1
    if (allCards.map(cardItem => (cardItem as TreasureCard).sellingPrice).reduce((a, b) => a + b, 0) * sellingMultiplier < 1000) {
      return []
    }

    return [
      new Target('Moi')
    ]
  }

}

export default Sell