import IActiveSkill from "./IActiveSkill";
import Player from "../Player";
import TurnPhase from "../../TurnPhase";
import Target from "../../Card/Target";
import Choice from "../../Card/Choice";
import Game from "../../Game";
import GameController from "../../../controllers/GameController";

class Resurect implements IActiveSkill {
  id: number = 1
  usageByTurn: number = 1
  canBeTriggered: Array<TurnPhase> = [TurnPhase.FIRST_STANDBY]

  activate(launcher: Player, targets: Target[]): boolean {
    if (!this.getTargets(launcher).length) {
      throw "You can't launch this spell in this phase !"
    }

    const target = targets[0]
    const cardId = target?.cards ? target?.cards[0] : undefined
    const cardToAdd = launcher.currentGame.discard.cards.find(card => card.id === cardId)

    if (cardToAdd) {
      launcher.currentGame.choiceCallbacks.push({
        sendChoices: () => {
          const choices: Choice[] = launcher.hand.cards.map(card => ({ name: card.name, cardId: card.id, isVisible: true }))

          launcher.websocket?.send(
            {
              title: 'show-choices',
              data: {
                choices,
                allowCancel: true,
                skills: [this.id],
                name: 'DEFAUSSE UNE CARTE'
              }
            }
          )
        },
        choiceCallback: (choices: Choice[]) => {
          let choice = choices[0]
          if (!choice) {
            const choices: Choice[] = launcher.hand.cards.map(card => ({ name: card.name, cardId: card.id }))
            choice = choices[Math.floor(Math.random() * choices.length)]
          }

          let card = launcher.hand.cards.find(cardItem => cardItem.id === choice.cardId)

          if (card) {
            launcher.activeSkillsUsedThisTurn.push(this)
            launcher.currentGame.discard.add(card)
            launcher.hand.add(cardToAdd)
            launcher.websocket?.broadcast({ title: "player-skill-triggered", data: { skills: [this.id], targets, cards: [card.id] }, playerId: launcher.deviantUser?.id })
          }

          return true
        }
      })

      return true
    }

    return false
  }

  getTargets(launcher: Player): Target[] {
    if (this.canBeTriggered.find(phase => phase === launcher.currentGame.turnPhase) === undefined) {
      return []
    }

    if (launcher.activeSkillsUsedThisTurn.filter(skill => skill.id === this.id).length >= this.usageByTurn) {
      return []
    }

    if (!launcher.currentGame.discard.cards.length) {
      return []
    }

    if (launcher.currentGame.whoIsPlaying().deviantUser?.id !== launcher.deviantUser?.id) {
      return []
    }

    return [
      new Target(
        launcher.currentGame.discard.cards[launcher.currentGame.discard.cards.length - 1].name,
        undefined,
        [launcher.currentGame.discard.cards[launcher.currentGame.discard.cards.length - 1].id]
      )
    ]
  }

}

export default Resurect