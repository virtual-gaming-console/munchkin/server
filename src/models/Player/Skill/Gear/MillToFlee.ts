import Target from "../../../Card/Target"
import TurnPhase from "../../../TurnPhase"
import Player from "../../Player"
import IActiveSkill from "../IActiveSkill"

class MillToFlee implements IActiveSkill {
    id: number = 8
    usageByTurn: number = 1
    canBeTriggered: Array<TurnPhase> = [TurnPhase.COMBAT]

    activate(launcher: Player, targets: Target[]): boolean {
        if (!this.getTargets(launcher).length) {
            throw "You can't launch this spell in this phase !"
        }

        const relatedCard = launcher.arsenalCards.find(gear => gear.skills?.find(skill => (skill as IActiveSkill).id === this.id))
        if (relatedCard) {
            launcher.currentGame.discard.add(relatedCard)
            launcher.currentGame.currentBattle?.removeAlly(launcher)
            return true
        }

        return false
    }

    getTargets(launcher: Player): Target[] {
        if (this.canBeTriggered.find(phase => phase === launcher.currentGame.turnPhase) === undefined) {
            return []
        }

        if (launcher.currentGame.currentBattle?.allies.find(({ player }) => player.deviantUser?.id === launcher.deviantUser?.id) === undefined) {
            return []
        }

        return [
            new Target('Moi')
        ]
    }

}

export default MillToFlee