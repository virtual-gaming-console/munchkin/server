import Target from "../../../Card/Target"
import TurnPhase from "../../../TurnPhase"
import Player from "../../Player"
import IActiveSkill from "../IActiveSkill"

class Seduce implements IActiveSkill {
    id: number = 10
    usageByTurn: number = 1
    canBeTriggered: Array<TurnPhase> = [TurnPhase.COMBAT]

    activate(launcher: Player, targets: Target[]): boolean {
        if (!this.getTargets(launcher).length) {
            throw "You can't launch this spell in this phase !"
        }

        const target = targets[0]
        const playerId = target.players ? target.players[0] : undefined
        const player = launcher.currentGame.players.find(player => player.deviantUser?.id === playerId)

        if (player) {
            launcher.currentGame.currentBattle?.newAlly(player, launcher.currentGame.currentBattle.enemies.cards.map(() => 0))
            return true
        }

        return false
    }

    getTargets(launcher: Player): Target[] {
        if (this.canBeTriggered.find(phase => phase === launcher.currentGame.turnPhase) === undefined) {
            return []
        }

        if (launcher.currentGame.currentBattle?.allies.find(({ player }) => player.deviantUser?.id === launcher.deviantUser?.id) === undefined) {
            return []
        }

        if (launcher.currentGame.currentBattle?.allies.length === 2) {
            return []
        }

        return launcher.currentGame.players
            .filter(player => player.deviantUser?.id !== launcher.deviantUser?.id)
            .map(player => new Target((player.deviantUser?.nickname || 'X'), [(player.deviantUser?.id || 'X')]))
    }

}

export default Seduce