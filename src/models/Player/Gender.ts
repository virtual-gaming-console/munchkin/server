enum Gender {
    NONE = "GENDER_NONE",
    MALE = "GENDER_MALE",
    FEMALE = "GENDER_FEMALE",
}

export default Gender