import IPlayerAbility from "../IPlayerAbility";
import PlayerRaceType from "./PlayerRaceType";

interface IPlayerRace extends IPlayerAbility {
    type: PlayerRaceType
}

export default IPlayerRace