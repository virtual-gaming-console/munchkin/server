import PassiveSkill from "../Skill/PassiveSkill";
import IActiveSkill from "../Skill/IActiveSkill";
import IPlayerRace from "./IPlayerRace";
import PlayerRaceType from "./PlayerRaceType";

class Elven implements IPlayerRace {
    type: PlayerRaceType = PlayerRaceType.ELVEN
    passives: Array<PassiveSkill> = [
        PassiveSkill.ADD_ONE_TO_FLEE,
        PassiveSkill.LEVEL_UP_ON_KILL_HELP,
    ]
    actives: Array<IActiveSkill> = []

}

export default Elven