import Dwarf from "./Dwarf";
import Elven from "./Elven";
import Hobbit from "./Hobbit";
import Human from "./Human";
import PlayerRaceType from "./PlayerRaceType";

const RACES = {
    [PlayerRaceType.DWARF]: Dwarf,
    [PlayerRaceType.ELVEN]: Elven,
    [PlayerRaceType.HOBBIT]: Hobbit,
    [PlayerRaceType.HUMAN]: Human,
}

export default RACES