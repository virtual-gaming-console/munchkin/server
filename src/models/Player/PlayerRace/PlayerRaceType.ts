enum PlayerRaceType {
    ELVEN = "PLAYER_RACE_TYPE_ELVEN",
    DWARF = "PLAYER_RACE_TYPE_DWARF",
    HOBBIT = "PLAYER_RACE_TYPE_HOBBIT",
    HUMAN = "PLAYER_RACE_TYPE_HUMAN",
}

export default PlayerRaceType