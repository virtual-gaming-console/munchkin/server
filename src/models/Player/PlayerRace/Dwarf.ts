import PassiveSkill from "../Skill/PassiveSkill";
import IActiveSkill from "../Skill/IActiveSkill";
import IPlayerRace from "./IPlayerRace";
import PlayerRaceType from "./PlayerRaceType";

class Dwarf implements IPlayerRace {
    type: PlayerRaceType = PlayerRaceType.DWARF
    passives: Array<PassiveSkill> = [
        PassiveSkill.DRAW_ONE_TREASURE_ON_KILL_HELP,
        PassiveSkill.CAN_HAVE_SIX_CARDS_IN_HAND,
    ]
    actives: Array<IActiveSkill> = []

}

export default Dwarf