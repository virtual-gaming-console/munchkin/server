import PassiveSkill from "../Skill/PassiveSkill";
import IActiveSkill from "../Skill/IActiveSkill";
import IPlayerRace from "./IPlayerRace";
import PlayerRaceType from "./PlayerRaceType";

class Hobbit implements IPlayerRace {
    type: PlayerRaceType = PlayerRaceType.HOBBIT
    passives: Array<PassiveSkill> = [
        PassiveSkill.REMOVE_ONE_TO_FLEE,
        PassiveSkill.SELL_ONE_ITEM_AT_DOUBLE_PRICE,
    ]
    actives: Array<IActiveSkill> = []

}

export default Hobbit