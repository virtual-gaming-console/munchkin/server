import PassiveSkill from "../Skill/PassiveSkill";
import IActiveSkill from "../Skill/IActiveSkill";
import IPlayerRace from "./IPlayerRace";
import PlayerRaceType from "./PlayerRaceType";

class Human implements IPlayerRace {
    type: PlayerRaceType = PlayerRaceType.HUMAN
    passives: Array<PassiveSkill> = []
    actives: Array<IActiveSkill> = []

}

export default Human