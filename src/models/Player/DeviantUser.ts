class DeviantUser {
    id: string
    nickname: string
    avatarImage: string

    constructor(id: string, nickname: string, avatarImage: string) {
        this.id = id
        this.nickname = nickname
        this.avatarImage = avatarImage
    }
}

export default DeviantUser