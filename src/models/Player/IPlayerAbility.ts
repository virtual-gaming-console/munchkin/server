import PassiveSkill from "./Skill/PassiveSkill";
import IActiveSkill from "./Skill/IActiveSkill";

interface IPlayerAbility {
  passives: Array<PassiveSkill>
  actives: Array<IActiveSkill>
}

export default IPlayerAbility