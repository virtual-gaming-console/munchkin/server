import IPlayerAbility from "../IPlayerAbility";
import PlayerClassType from "./PlayerClassType";

interface IPlayerClass extends IPlayerAbility {
    type: PlayerClassType
}

export default IPlayerClass