import IPlayerClass from "./IPlayerClass";
import PlayerClassType from "./PlayerClassType";
import PassiveSkill from "../Skill/PassiveSkill";
import IActiveSkill from "../Skill/IActiveSkill";
import Resurect from "../Skill/Resurect";
import Turning from "../Skill/Turning";

class Priest implements IPlayerClass {
    type: PlayerClassType = PlayerClassType.PRIEST
    passives: Array<PassiveSkill> = []
    actives: Array<IActiveSkill> = [
        new Resurect(),
        new Turning(),
    ]

}

export default Priest