enum PlayerClassType {
    PRIEST = "PLAYER_CLASS_TYPE_PRIEST",
    MAGE = "PLAYER_CLASS_TYPE_MAGE",
    WAR = "PLAYER_CLASS_TYPE_WAR",
    THIEF = "PLAYER_CLASS_TYPE_THIEF"
}

export default PlayerClassType