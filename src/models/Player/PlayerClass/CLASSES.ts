import Mage from "./Mage";
import Priest from "./Priest";
import Thief from "./Thief";
import Warrior from "./Warrior";
import PlayerClassType from "./PlayerClassType";

const CLASSES = {
    [PlayerClassType.MAGE]: Mage,
    [PlayerClassType.PRIEST]: Priest,
    [PlayerClassType.THIEF]: Thief,
    [PlayerClassType.WAR]: Warrior,
}

export default CLASSES