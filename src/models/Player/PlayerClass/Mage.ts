import IPlayerClass from "./IPlayerClass";
import PlayerClassType from "./PlayerClassType";
import PassiveSkill from "../Skill/PassiveSkill";
import IActiveSkill from "../Skill/IActiveSkill";
import Flight from "../Skill/Flight";
import Charm from "../Skill/Charm";

class Mage implements IPlayerClass {
    type: PlayerClassType = PlayerClassType.MAGE
    passives: Array<PassiveSkill> = []
    actives: Array<IActiveSkill> = [
        new Flight(),
        new Charm(),
    ]

}

export default Mage