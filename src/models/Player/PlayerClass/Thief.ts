import IPlayerClass from "./IPlayerClass";
import PlayerClassType from "./PlayerClassType";
import PassiveSkill from "../Skill/PassiveSkill";
import IActiveSkill from "../Skill/IActiveSkill";
import Backstab from "../Skill/Backstab";
import Theft from "../Skill/Theft";

class Thief implements IPlayerClass {
    type: PlayerClassType = PlayerClassType.THIEF
    passives: Array<PassiveSkill> = []
    actives: Array<IActiveSkill> = [
        new Backstab(),
        new Theft(),
    ]

}

export default Thief