import IPlayerClass from "./IPlayerClass";
import PlayerClassType from "./PlayerClassType";
import PassiveSkill from "../Skill/PassiveSkill";
import IActiveSkill from "../Skill/IActiveSkill";
import BerserkerRage from "../Skill/BerserkerRage";

class Warrior implements IPlayerClass {
    type: PlayerClassType = PlayerClassType.WAR
    passives: Array<PassiveSkill> = [
        PassiveSkill.WIN_TIES_IN_COMBAT,
    ]
    actives: Array<IActiveSkill> = [
        new BerserkerRage(),
    ]

}

export default Warrior