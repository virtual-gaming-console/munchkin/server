import DeviantUser from "./DeviantUser"
import MyWebSocket from "../WebSocket/MyWebSocket"
import GearCard from "../Card/Gear/GearCard"
import Gender from "./Gender"
import PassiveSkill from "./Skill/PassiveSkill"
import Deck from "../Card/Deck"
import Game from "../Game"
import IPlayerClass from "./PlayerClass/IPlayerClass"
import IPlayerRace from "./PlayerRace/IPlayerRace"
import ICard from "../Card/ICard"
import TreasureCard from "../Card/TreasureCard"
import PlayerRaceType from "./PlayerRace/PlayerRaceType"
import PlayerClassType from "./PlayerClass/PlayerClassType"
import GearPlacement from "../Card/Gear/GearPlacement"
import MonsterCard from "../Card/Monster/MonsterCard"
import TurnPhase from "../TurnPhase"
import Choice from "../Card/Choice"
import Equipable from "../Card/Gear/Equipable"
import DUNGEON_CONSUMABLES from "../../mocks/CARDS/CONSUMABLES/DUNGEONS"
import RaceCard from "../Card/Consumable/RaceCard"
import ClassCard from "../Card/Consumable/ClassCard"
import Human from "./PlayerRace/Human"
import CardType from "../Card/CardType"
import CurseCard from "../Card/Consumable/CurseCard"
import MODIFIERS from "../Battle/MODIFIERS"
import RemovalAction from "../Card/Consumable/RemovalAction"
import IActiveSkill from "./Skill/IActiveSkill"
import Sell from "./Skill/Sell"
import GameController from "../../controllers/GameController"

class Player {
    deviantUser?: DeviantUser
    websocket?: MyWebSocket
    currentGame: Game

    private _level: number = 1
    get level(): number {
        return this._level
    }
    set level(newLevel: number) {
        newLevel = newLevel > 0 ? newLevel : 1
        if (newLevel !== this._level) {
            this._level = newLevel
            this.websocket?.broadcast({ title: 'player-update', data: { level: newLevel, power: this.power }, playerId: this.deviantUser?.id })
        }
        if (newLevel > 9) {
            GameController.endGame(this.currentGame)
        }
    }

    wasDead: boolean = false
    private _isDead: boolean = false
    get isDead(): boolean {
        return this._isDead
    }
    set isDead(isDead: boolean) {
        if (this._isDead !== isDead) {
            this.websocket?.broadcast({ title: 'player-update', data: { isDead }, playerId: this.deviantUser?.id })
        }
        this._isDead = isDead
    }

    gender: Gender = Gender.NONE

    private _race: IPlayerRace = new Human()
    get race(): IPlayerRace {
        return this._race
    }
    set race(race: IPlayerRace) {
        if (this._race.type === race.type) {
            return
        }
        this.websocket?.broadcast({
            title: 'player-race-' + (race.type === PlayerRaceType.HUMAN ? 'remove' : 'add'),
            data: {
                additionnalClassRace: false,
                classOrRaceType: race?.type,
            },
            playerId: this.deviantUser?.id
        })
        if (this._race.type !== PlayerRaceType.HUMAN) {
            const raceCard = DUNGEON_CONSUMABLES.map(instantiator => instantiator.instantiate()).find(card => (card as RaceCard).value === this._race?.type)
            this.currentGame.discard.add(raceCard)
        }
        this._race = race
        this.checkArsenalForForbidenCards()
    }

    private _halvenRace?: IPlayerRace
    get halvenRace(): IPlayerRace | undefined {
        return this._halvenRace
    }
    set halvenRace(race: IPlayerRace | undefined) {
        if (this._halvenRace?.type === race?.type) {
            return
        }
        this.websocket?.broadcast({
            title: 'player-race-' + (race?.type === undefined ? 'remove' : 'add'),
            data: {
                additionnalClassRace: true,
                classOrRaceType: race?.type,
            },
            playerId: this.deviantUser?.id
        })
        if (this._halvenRace?.type !== PlayerRaceType.HUMAN) {
            const raceCard = DUNGEON_CONSUMABLES.map(instantiator => instantiator.instantiate()).find(card => (card as RaceCard).value === this._halvenRace?.type)
            this.currentGame.discard.add(raceCard)
            const halvenCard = DUNGEON_CONSUMABLES.find(instantiator => instantiator.cardId === 38)?.instantiate()
            this.currentGame.discard.add(halvenCard)
        }
        this._halvenRace = race
        this.checkArsenalForForbidenCards()
    }

    private _class?: IPlayerClass
    get class(): IPlayerClass | undefined {
        return this._class
    }
    set class(_class: IPlayerClass | undefined) {
        if (this._class?.type === _class?.type) {
            return
        }
        this.websocket?.broadcast({
            title: 'player-class-' + (_class === undefined ? 'remove' : 'add'),
            data: {
                additionnalClassRace: false,
                classOrRaceType: _class?.type,
            },
            playerId: this.deviantUser?.id
        })
        if (this.class) {
            const classCard = DUNGEON_CONSUMABLES.map(instantiator => instantiator.instantiate()).find(card => (card as ClassCard).value === this.class?.type)
            this.currentGame.discard.add(classCard)
        }
        this._class = _class
        this.checkArsenalForForbidenCards()
    }

    private _superMunchkinClass?: IPlayerClass
    get superMunchkinClass(): IPlayerClass | undefined {
        return this._superMunchkinClass
    }
    set superMunchkinClass(_class: IPlayerClass | undefined) {
        if (this._superMunchkinClass?.type === _class?.type) {
            return
        }
        this.websocket?.broadcast({
            title: 'player-class-' + (_class === undefined ? 'remove' : 'add'),
            data: {
                additionnalClassRace: true,
                classOrRaceType: _class?.type,
            },
            playerId: this.deviantUser?.id
        })
        if (this.superMunchkinClass) {
            const classCard = DUNGEON_CONSUMABLES.map(instantiator => instantiator.instantiate()).find(card => (card as ClassCard).value === this.superMunchkinClass?.type)
            this.currentGame.discard.add(classCard)
            const superMunchinCard = DUNGEON_CONSUMABLES.find(instantiator => instantiator.cardId === 66)?.instantiate()
            this.currentGame.discard.add(superMunchinCard)
        }
        this._superMunchkinClass = _class
        this.checkArsenalForForbidenCards()
    }

    hand: Deck<ICard> = new Deck<ICard>(
        [],
        (card: ICard) => this.websocket?.broadcast({ title: 'player-hand-add', data: { cardId: card.id, isVisible: card.isVisible }, playerId: this.deviantUser?.id }),
        (card: ICard) => this.websocket?.broadcast({ title: 'player-hand-remove', data: { cardId: card.id }, playerId: this.deviantUser?.id })
    )

    cheat: Deck<ICard> = new Deck<ICard>(
        [],
        (card: ICard) => {
            this.websocket?.broadcast({ title: 'player-arsenal-add-cheat', data: { cardId: card.id }, playerId: this.deviantUser?.id })
            this.websocket?.broadcast({ title: 'player-update', data: { power: this.power }, playerId: this.deviantUser?.id })
        },
        (card: ICard) => {
            this.websocket?.broadcast({ title: 'player-arsenal-remove', data: { cardId: card.id }, playerId: this.deviantUser?.id })
            this.websocket?.broadcast({ title: 'player-update', data: { power: this.power }, playerId: this.deviantUser?.id })
            this.currentGame.discard.add(DUNGEON_CONSUMABLES.find(instantiator => instantiator.cardId === 7)?.instantiate())
        },
    )
    arsenal: Deck<GearCard> = new Deck<GearCard>(
        [],
        (card: GearCard) => {
            this.websocket?.broadcast({ title: 'player-arsenal-add', data: { cardId: card.id }, playerId: this.deviantUser?.id })
            this.websocket?.broadcast({ title: 'player-update', data: { power: this.power }, playerId: this.deviantUser?.id })
        },
        (card: GearCard) => {
            this.websocket?.broadcast({ title: 'player-arsenal-remove', data: { cardId: card.id }, playerId: this.deviantUser?.id })
            this.websocket?.broadcast({ title: 'player-update', data: { power: this.power }, playerId: this.deviantUser?.id })
            const losingHeadCurses = this.curses.cards.filter(card => card.residualEffect && card.residualEffect.removalAction === RemovalAction.LOSING_HEAD)
            if (card.placement === GearPlacement.HEAD && losingHeadCurses.length) {
                losingHeadCurses.forEach(card => this.currentGame.discard.add(card))
            }
        }
    )
    get arsenalCards(): Array<GearCard> {
        let cards = []
        for (let index = 0; index < this.cheat.cards.length; index++) {
            const element = this.cheat.cards[index];
            if ((element as GearCard).placement) {
                cards.push(element as GearCard)
            }
        }
        for (let index = 0; index < this.arsenal.cards.length; index++) {
            const element = this.arsenal.cards[index];
            cards.push(element)
        }
        return cards
    }

    curses: Deck<CurseCard> = new Deck<CurseCard>(
        [],
        (card: ICard) => {
            this.websocket?.broadcast({ title: 'player-curses-add', data: { cardId: card.id }, playerId: this.deviantUser?.id })
            this.websocket?.broadcast({ title: 'player-update', data: { power: this.power }, playerId: this.deviantUser?.id })
        },
        (card: ICard) => {
            this.websocket?.broadcast({ title: 'player-curses-remove', data: { cardId: card.id }, playerId: this.deviantUser?.id })
            this.websocket?.broadcast({ title: 'player-update', data: { power: this.power }, playerId: this.deviantUser?.id })
        }
    )

    activeSkillsUsedThisTurn: Array<IActiveSkill> = []
    passivSkillsUsedThisTurn: Array<PassiveSkill> = []

    rolling?: {
        dice: number,
        modifier: number,
        callback?: Function
    }

    constructor(game: Game, deviantUser?: DeviantUser) {
        this.currentGame = game
        this.deviantUser = deviantUser
    }

    get races(): Array<PlayerRaceType> {
        let ret = []

        if (this._race) {
            ret.push(this._race.type)
        }

        if (this._halvenRace) {
            ret.push(this._halvenRace.type)
        }

        return ret
    }

    get classes(): Array<PlayerClassType> {
        let ret = []

        if (this._class) {
            ret.push(this._class.type)
        }

        if (this._superMunchkinClass) {
            ret.push(this._superMunchkinClass.type)
        }

        return ret
    }

    get power(): number {
        const cursesModifiers = this.curses.cards.map(curse => curse.residualEffect?.modifier)
        const powerModifiers = cursesModifiers
            .filter(modifier => modifier && modifier.power)
            .map(modifier => modifier && modifier.power)
            .reduce((prev, curr) => (prev || 0) + (curr || 0), 0)
        if (cursesModifiers.find(modifier => modifier && MODIFIERS.NO_BONUS().id === modifier.id)) {
            return this.level + (powerModifiers || 0)
        }
        if (cursesModifiers.find(modifier => modifier && MODIFIERS.NO_BONUS_EXCEPT_ARMOR().id === modifier.id)) {
            return this.arsenalCards
                .filter(card => card.placement === GearPlacement.ARMOR)
                .map(gear => gear.getTotalBonus(this))
                .reduce((prev, curr) => prev + curr, this.level) + (powerModifiers || 0)
        }

        return this.arsenalCards
            .map(gear => gear.getTotalBonus(this))
            .reduce((prev, curr) => prev + curr, cursesModifiers.find(modifier => modifier && MODIFIERS.NO_LEVEL().id === modifier.id) ? 0 : this.level) + (powerModifiers || 0)
    }

    get fleeModifier(): number {
        let modifer = 0
        this.arsenalCards
            .filter(gear => gear.skills)
            .map(gear => gear.skills)
            .forEach(skills => {
                if (skills?.find(skill => skill === PassiveSkill.BOOTS_OF_RUNNING_REALLY_FAST)) {
                    modifer += 2
                }
            })
        return modifer
    }

    get activeSkills(): IActiveSkill[] {
        let ret: IActiveSkill[] = [
            new Sell()
        ]

        if (this.class) {
            ret = [
                ...ret,
                ...this.class.actives
            ]
        }

        if (this.superMunchkinClass) {
            ret = [
                ...ret,
                ...this.superMunchkinClass.actives
            ]
        }

        if (this.race) {
            ret = [
                ...ret,
                ...this.race.actives
            ]
        }

        if (this.halvenRace) {
            ret = [
                ...ret,
                ...this.halvenRace.actives
            ]
        }

        const itemsWithSkills = this.arsenalCards.filter(card => card.skills?.find(skill => typeof skill === "object"))
        if (itemsWithSkills.length) {
            itemsWithSkills.forEach(gear => {
                const activSkills = gear.skills?.filter(skill => typeof skill === "object")
                ret = [
                    ...ret,
                    ...activSkills as IActiveSkill[]
                ]
            })
        }

        return ret
    }

    hasActiveSkill(activeSkill: IActiveSkill): boolean {
        return this.activeSkills.find(active => active.id === activeSkill.id) !== undefined
    }

    get usableSkillIds() {
        return this.activeSkills.filter(skill => skill.getTargets(this).length).map(skill => skill.id)
    }

    get passivSkills(): PassiveSkill[] {
        let ret: PassiveSkill[] = []

        if (this.class) {
            ret = [
                ...ret,
                ...this.class.passives
            ]
        }

        if (this.superMunchkinClass) {
            ret = [
                ...ret,
                ...this.superMunchkinClass.passives
            ]
        }

        if (this.race) {
            ret = [
                ...ret,
                ...this.race.passives
            ]
        }

        if (this.halvenRace) {
            ret = [
                ...ret,
                ...this.halvenRace.passives
            ]
        }

        const itemsWithSkills = this.arsenalCards.filter(card => card.skills?.find(skill => typeof skill === "string"))
        if (itemsWithSkills.length) {
            itemsWithSkills.forEach(gear => {
                const passivSkills = gear.skills?.filter(skill => typeof skill === "string")
                ret = [
                    ...ret,
                    ...passivSkills as PassiveSkill[]
                ]
            })
        }

        return ret
    }

    hasPassiveSkill(passiveSkill: PassiveSkill): boolean {
        return this.passivSkills.find(passiv => passiv === passiveSkill) !== undefined
    }

    checkArsenalForForbidenCards() {
        this.arsenal.cards
            .filter(card => card.forbiden || card.restriction)
            .forEach(card => {
                if (card.canEquip(this) === Equipable.NO) {
                    this.hand.add(card)
                }
            })
    }

    equip(card: GearCard, cardIdToSwap?: number, cheat: boolean = false): boolean {
        if (cheat) {
            this.cheat.add(card)
            return true
        }

        const canEquip = card.canEquip(this)
        if (canEquip === Equipable.NO) {
            return false
        }

        if (canEquip === Equipable.WITH_SWAP) {
            switch (card.placement) {
                case GearPlacement.ARMOR:
                case GearPlacement.BOOTS:
                case GearPlacement.HEAD:
                    const cardToSwap = this.arsenalCards.find(gearCard => gearCard.placement === card.placement)
                    if (cardToSwap) {
                        this.hand.add(cardToSwap)
                    }
                    break
                case GearPlacement.ONE_HAND:
                    const sameSlotGears = this.arsenalCards
                        .filter(gear => gear.placement === card.placement)
                        .sort((a, b) => a.bonus - b.bonus)
                    const twoHandGearEquipped = this.arsenalCards.find(gear => gear.placement === GearPlacement.TWO_HAND)
                    if (twoHandGearEquipped) {
                        this.hand.add(twoHandGearEquipped)
                    } else if (sameSlotGears.length === 2) {
                        const gearToSwap = sameSlotGears.find(card => card.id === cardIdToSwap)
                        if (gearToSwap) {
                            this.hand.add(gearToSwap)
                        } else {
                            this.hand.add(sameSlotGears[0])
                        }
                    }
                    break
                case GearPlacement.TWO_HAND:
                    const oneHandGearEquipped = this.arsenalCards.filter(gear => gear.placement === GearPlacement.ONE_HAND)
                    oneHandGearEquipped.forEach(gear => {
                        this.hand.add(gear)
                    })
                    break
                default:
                    break
            }
        }

        this.arsenal.add(card)
        return true
    }

    kill(monster: MonsterCard) {
        this.level += monster.reward.experience
    }

    draw(numberOfCard: number, isTreasure: boolean) {
        const deck = isTreasure ? this.currentGame.treasureDeck : this.currentGame.dungeonDeck
        for (let index = 0; index < numberOfCard; index++) {
            this.hand.add(deck.cards[0])
        }
    }

    drawStartingHand() {
        // TMP
        if (this.deviantUser?.id === "5f1174a68fd0290079c2b574") {
            // this.level += 8
            // const prettyBaloons = this.currentGame.treasureDeck.cards.find(card => card.id === 127)
            // if (prettyBaloons) {
            //     this.hand.add(prettyBaloons)
            // }
            // console.log(this.currentGame.dungeonDeck.cards.filter(card => card.id === 72))
            // const loseAnItem = this.currentGame.dungeonDeck.cards.find(card => card.id === 22)
            // if (loseAnItem) {
            //     this.hand.add(loseAnItem)
            // }
            // const cheat = this.currentGame.dungeonDeck.cards.find(card => card.id === 7)
            // if (cheat) {
            //     this.hand.add(cheat)
            // }
            // const loseAnItemm = this.currentGame.dungeonDeck.cards.find(card => card.id === 18)
            // if (loseAnItemm) {
            //     this.hand.add(loseAnItemm)
            // }
            // const wanderingMonster = this.currentGame.dungeonDeck.cards.find(card => card.id === 44)
            // if (wanderingMonster) {
            //     this.hand.add(wanderingMonster)
            // }
            // const wanderingMonsterTwo = this.currentGame.dungeonDeck.cards.find(card => card.id === 72)
            // if (wanderingMonsterTwo) {
            //     this.hand.add(wanderingMonsterTwo)
            // }
            // const intel = this.currentGame.dungeonDeck.cards.find(card => card.id === 46)
            // if (intel) {
            //     this.hand.add(intel)
            // }
            // const mate = this.currentGame.dungeonDeck.cards.find(card => card.id === 52)
            // if (mate) {
            //     this.hand.add(mate)
            // }
            // const elven = this.currentGame.dungeonDeck.cards.find(card => card.id === 30)
            // if (elven) {
            //     this.hand.add(elven)
            // }
            // const dwarf = this.currentGame.dungeonDeck.cards.find(card => card.id === 29)
            // if (dwarf) {
            //     this.hand.add(dwarf)
            // }
            // const hobbit = this.currentGame.dungeonDeck.cards.find(card => card.id === 39)
            // if (hobbit) {
            //     // this.hand.add(hobbit)
            //     this.currentGame.discard.add(hobbit)
            // }
            // const halfBreed = this.currentGame.dungeonDeck.cards.find(card => card.id === 38)
            // if (halfBreed) {
            //     this.hand.add(halfBreed)
            // }
            // const mage = this.currentGame.dungeonDeck.cards.find(card => card.id === 76)
            // if (mage) {
            //     this.hand.add(mage)
            // }
            // const thief = this.currentGame.dungeonDeck.cards.find(card => card.id === 67)
            // if (thief) {
            //     this.hand.add(thief)
            //     // this.currentGame.discard.add(thief)

            // }
            // const priest = this.currentGame.dungeonDeck.cards.find(card => card.id === 8)
            // if (priest) {
            //     // this.currentGame.discard.add(priest)
            //     this.hand.add(priest)
            // }
            // const war = this.currentGame.dungeonDeck.cards.find(card => card.id === 74)
            // if (war) {
            //     this.hand.add(war)
            // }
            // const KneepadsOfAllure = this.currentGame.treasureDeck.cards.find(card => card.id === 131)
            // if (KneepadsOfAllure) {
            //     this.hand.add(KneepadsOfAllure)
            // }
            // const WishingRing = this.currentGame.treasureDeck.cards.find(card => card.id === 147)
            // if (WishingRing) {
            //     this.hand.add(WishingRing)
            // }
            // const Sword = this.currentGame.treasureDeck.cards.find(card => card.id === 134)
            // if (Sword) {
            //     this.hand.add(Sword)
            // }
            // const superMunchinCard = this.currentGame.dungeonDeck.cards.find(card => card.id === 66)
            // if (superMunchinCard) {
            //     this.hand.add(superMunchinCard)
            // }
            // const wanderingMonsterOne = this.currentGame.dungeonDeck.cards.find(card => card.id === 72)
            // console.log(wanderingMonsterOne)
            // if (wanderingMonsterOne) {
            //     this.hand.add(wanderingMonsterOne)
            // }
            // const wanderingMonsterTwo = this.currentGame.dungeonDeck.cards.find(card => card.id === 72)
            // console.log(wanderingMonsterTwo)
            // if (wanderingMonsterTwo) {
            //     this.hand.add(wanderingMonsterTwo)
            // }
            // const monster = this.currentGame.dungeonDeck.cards.find(card => card.type === CardType.MONSTER)
            // if (monster) {
            //     this.hand.add(monster)
            // }
            // const testCard = this.currentGame.treasureDeck.cards.find(card => card.id === 5555)
            // if (testCard) {
            //     this.hand.add(testCard)
            // }
            // const WandOfDowsing = this.currentGame.treasureDeck.cards.find(card => card.id === 145)
            // if (WandOfDowsing) {
            //     this.hand.add(WandOfDowsing)
            // }
            // const FriendshipPotion = this.currentGame.treasureDeck.cards.find(card => card.id === 148)
            // if (FriendshipPotion) {
            //     this.hand.add(FriendshipPotion)
            // }
            // const MagicLamp = this.currentGame.treasureDeck.cards.find(card => card.id === 103)
            // if (MagicLamp) {
            //     this.hand.add(MagicLamp)
            // }
            // const flamingGear = this.currentGame.treasureDeck.cards.find(card => card.id === 95)
            // if (flamingGear) {
            //     this.hand.add(flamingGear)
            // }
            // const flamingPotion = this.currentGame.treasureDeck.cards.find(card => card.id === 96)
            // if (flamingPotion) {
            //     this.hand.add(flamingPotion)
            // }
            // const armorGear = this.currentGame.treasureDeck.cards.find(card => (card as GearCard).placement === GearPlacement.BOOTS)
            // if (armorGear) {
            //     this.hand.add(armorGear)
            // }
            // const doppleGanger = this.currentGame.treasureDeck.cards.find(card => card.id === 92)
            // if (doppleGanger) {
            //     this.hand.add(doppleGanger)
            // }
            // for (let index = 0; index < 5; index++) {
            //     const gearCard = this.currentGame.treasureDeck.cards.find(card => (card as GearCard).bonus <= 2 && !(card as GearCard).restriction && this.hand.cards.map(card => (card as GearCard).placement).filter(placement => placement === (card as GearCard).placement).length === 0)
            //     // const gearCard = this.currentGame.treasureDeck.cards.find(card => card.type === CardType.GEAR)
            //     if (gearCard) {
            //         this.hand.add(gearCard)
            //     }
            // }

            this.draw(2, true)
            this.draw(2, false)
        } else {
            this.draw(2, true)
            this.draw(2, false)

            // HELP ME OUT HERE TESTING

            // for (let index = 0; index < 2; index++) {
            //     const gearCard = this.currentGame.treasureDeck.cards
            //         .find(card => card.type === CardType.GEAR && !(card as GearCard).restriction && (card as GearCard).bonus > 2)
            //     if (gearCard) {
            //         this.equip(gearCard as GearCard)
            //     }
            // }
        }
    }

    revive() {
        this.hand.cards.forEach(card => {
            this.currentGame.discard.add(card)
        })
        this.arsenalCards.forEach(card => {
            this.currentGame.discard.add(card)
        })
    }

    die() {
        this.isDead = true
        this.wasDead = true
        this.currentGame.turnPhase = TurnPhase.DEATH

        if (this.currentGame.currentBattle?.allies.find(({ player }) => player.deviantUser?.id === this.deviantUser?.id)) {
            this.currentGame.currentBattle.removeAlly(this)
        }

        if (this.currentGame.lowestLevelPlayers().find(player => player.deviantUser?.id === this.deviantUser?.id)) {
            // GameController.nextTurnPhase(this.currentGame)
            return
        }

        const otherPlayers = this.currentGame.players
            .filter(player => player.deviantUser?.id !== this.deviantUser?.id && !player.isDead)
            .sort((a, b) => b.level - a.level)
        const choiceCount = this.hand.cards.length + this.arsenalCards.length

        for (let index = 0; index < (choiceCount >= otherPlayers.length ? otherPlayers.length : choiceCount); index++) {
            const player = otherPlayers[index]
            this.currentGame.choiceCallbacks.push({
                sendChoices: () => {
                    const cards = [
                        ...this.hand.cards,
                        ...this.arsenalCards
                    ]

                    const choices = cards.map(card => ({ name: card.name, cardId: card.id }))

                    player.websocket?.send(
                        {
                            title: 'show-choices',
                            data: {
                                choices,
                                name: 'PILLE UNE CARTE DANS LA MAIN DE ' + this.deviantUser?.nickname.toUpperCase()
                            }
                        }
                    )
                },
                choiceCallback: (choice: Choice) => {
                    if (!choice) {
                        const cards = [
                            ...this.hand.cards,
                            ...this.arsenalCards
                        ]

                        const choices = cards.map(card => ({ name: card.name, cardId: card.id }))
                        choice = choices[Math.floor(Math.random() * choices.length)]
                    }

                    let card = this.hand.cards.find(cardItem => cardItem.id === choice.cardId)
                    if (!card) {
                        card = this.arsenalCards.find(cardItem => cardItem.id === choice.cardId)
                    }

                    if (card) {
                        this.giveCard(card, player)
                    }

                    return false
                }
            })
        }
    }

    giveCard(card: ICard, player: Player) {
        player.hand.add(card)
        this.currentGame.websocket.broadcast({
            title: 'card-given',
            data: {
                players: [
                    (this.deviantUser?.id || ''),
                    (player.deviantUser?.id || '')
                ],
                cardId: card.id
            }
        })
    }

    playableCardIds() {
        if (this.currentGame.choiceCallbacks.length) {
            return []
        }
        return this.hand.cards.filter(card => card.isPlayable(this)).map(card => card.id)
    }

    roll(callback?: Function, dice?: number, sendMessage: boolean = true) {
        const cursesModifiers = this.curses.cards.map(curse => curse.residualEffect?.modifier)
        const fleeModifiers = cursesModifiers
            .filter(modifier => modifier && modifier.flee)
            .map(modifier => modifier && modifier.flee)
            .reduce((prev, curr) => (prev || 0) + (curr || 0), 0)
        let monsterFleeModifer = 0
        if (this.currentGame.currentBattle?.enemies.cards[0] && this.currentGame.currentBattle?.enemies.cards[0].passivs) {
            for (let index = 0; index < this.currentGame.currentBattle?.enemies.cards[0].passivs.length; index++) {
                const passiv = this.currentGame.currentBattle?.enemies.cards[0].passivs[index];
                if (!passiv.restriction && passiv.modifier?.flee) {
                    monsterFleeModifer += passiv.modifier.flee
                    break
                }
            }
        }
        this.rolling = {
            dice: dice || 1 + Math.round(Math.random() * 5),
            modifier: (fleeModifiers || 0) + monsterFleeModifer,
            callback
        }
        if (sendMessage) {
            this.websocket?.broadcast({
                title: 'player-update',
                data: {
                    dice: this.rolling.dice,
                    flee: this.rolling.modifier
                },
                playerId: this.deviantUser?.id
            })

            // if (!callback && [TurnPhase.COMBAT, TurnPhase.FLEE].find(phase => phase === this.currentGame.turnPhase)) {
            //     const monster = this.currentGame.currentBattle?.enemies.cards[0]
            //     if (monster) {
            //         this.currentGame.websocket.broadcast({
            //             title: 'player-flee-' + (this.successfullyFleeingFromMonsterWithDice(monster, this.rolling.dice) ? 'success' : 'fail'),
            //             data: { dice: this.rolling.dice },
            //             playerId: this.deviantUser?.id
            //         })
            //     }
            // } else {
            //     this.currentGame.websocket.broadcast({ title: 'player-roll', data: { dice: this.rolling.dice } })
            // }
        }
    }
}

export default Player