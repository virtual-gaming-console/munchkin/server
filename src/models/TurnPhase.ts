enum TurnPhase {
  NONE,
  FIRST_STANDBY,
  STANDBY,
  COMBAT,
  CHARITY,
  FLEE,
  DEATH,
  ROLL,
  CURSE,
}

export default TurnPhase