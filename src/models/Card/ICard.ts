import Deck from "./Deck";
import CardType from "./CardType";

interface ICard {
  id: number
  name: string
  type: CardType
  isTreasure: boolean
  isVisible: boolean
  from?: Deck<any>

  isPlayable: Function
}

export default ICard