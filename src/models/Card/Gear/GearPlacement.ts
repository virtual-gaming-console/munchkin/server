enum GearPlacement {
    ONE_HAND = "GEAR_PLACEMENT_ONE_HAND",
    TWO_HAND = "GEAR_PLACEMENT_TWO_HAND",
    HEAD = "GEAR_PLACEMENT_HEAD",
    ARMOR = "GEAR_PLACEMENT_ARMOR",
    BOOTS = "GEAR_PLACEMENT_BOOTS",
    NONE = "GEAR_PLACEMENT_NONE"
}

export default GearPlacement