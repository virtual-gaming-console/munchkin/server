import CardType from "../CardType";
import PlayerRaceType from "../../Player/PlayerRace/PlayerRaceType";
import PlayerClassType from "../../Player/PlayerClass/PlayerClassType";
import GearPlacement from "./GearPlacement";
import Gender from "../../Player/Gender";
import TreasureCard from "../TreasureCard";
import Player from "../../Player/Player";
import TurnPhase from "../../TurnPhase";
import Equipable from "./Equipable";
import IActiveSkill from "../../Player/Skill/IActiveSkill";
import PassiveSkill from "../../Player/Skill/PassiveSkill";

class GearRestriction {
    type: PlayerRaceType | PlayerClassType | Gender
    bonus?: number
    skills?: Array<IActiveSkill | PassiveSkill>

    constructor(
        type: PlayerRaceType | PlayerClassType | Gender,
        bonus?: number,
        skills?: Array<IActiveSkill | PassiveSkill>
    ) {
        this.type = type
        this.bonus = bonus
        this.skills = skills
    }
}

class GearCard extends TreasureCard {
    bonus: number
    placement: GearPlacement
    restriction?: PlayerRaceType | PlayerClassType | Gender
    forbiden?: PlayerRaceType | PlayerClassType | Gender
    skills?: Array<IActiveSkill | PassiveSkill>
    additionalRestriction?: GearRestriction

    getTotalBonus: Function = (handler: Player) => {
        let bonus = this.bonus

        if (this.additionalRestriction && this.additionalRestriction.bonus) {
            if (this.additionalRestriction.type === handler.gender) {
                bonus += this.additionalRestriction.bonus
            }
            if (handler.classes.find(classType => classType === this.additionalRestriction?.type)) {
                bonus += this.additionalRestriction.bonus
            }
            if (handler.races.find(raceType => raceType === this.additionalRestriction?.type)) {
                bonus += this.additionalRestriction.bonus
            }
        }

        return bonus
    }

    isPlayable: Function = (handler: Player) => {
        if (handler.deviantUser?.id !== handler.currentGame.whoIsPlaying().deviantUser?.id) {
            return false
        }

        if ([TurnPhase.STANDBY, TurnPhase.FIRST_STANDBY].find(phase => handler.currentGame.turnPhase === phase) === undefined) {
            return false
        }

        return this.canEquip(handler) !== Equipable.NO
    }

    canEquip: Function = (handler: Player) => {
        if (this.forbiden && this.forbiden === handler.gender) {
            return Equipable.NO
        }
        if (this.forbiden && handler.classes.find(classType => classType === this.forbiden)) {
            return Equipable.NO
        }
        if (this.forbiden && handler.races.find(raceType => raceType === this.forbiden)) {
            return Equipable.NO
        }

        let acheiveRestriction = true
        if (this.restriction) {
            acheiveRestriction = false
            if (this.restriction === handler.gender) {
                acheiveRestriction = true
            }
            if (handler.classes.find(classType => classType === this.restriction)) {
                acheiveRestriction = true
            }
            if (handler.races.find(raceType => raceType === this.restriction)) {
                acheiveRestriction = true
            }
        }

        if (!acheiveRestriction) {
            return Equipable.NO
        }

        switch (this.placement) {
            case GearPlacement.ARMOR:
            case GearPlacement.BOOTS:
            case GearPlacement.HEAD:
                if (handler.arsenalCards.find(gearCard => gearCard.placement === this.placement)) {
                    return Equipable.WITH_SWAP
                }
                break
            case GearPlacement.ONE_HAND:
                if (handler.arsenalCards.find(gearCard => gearCard.placement === GearPlacement.TWO_HAND)) {
                    return Equipable.WITH_SWAP
                } else if (handler.arsenalCards.filter(gear => gear.placement === this.placement).length === 2) {
                    return Equipable.WITH_SWAP
                }
                break
            case GearPlacement.TWO_HAND:
                if (handler.arsenalCards.find(gear => gear.placement === GearPlacement.ONE_HAND)) {
                    return Equipable.WITH_SWAP
                }
                break
            default:
                break
        }

        return Equipable.YES
    }

    constructor(
        id: number,
        name: string,
        bonus: number,
        placement: GearPlacement,
        sellingPrice: number,
        restriction?: PlayerRaceType | PlayerClassType | Gender,
        forbiden?: PlayerRaceType | PlayerClassType | Gender,
        skills?: Array<IActiveSkill | PassiveSkill>,
        additionalRestriction?: GearRestriction,
    ) {
        super(id, name, CardType.GEAR, sellingPrice)
        this.bonus = bonus
        this.placement = placement
        this.restriction = restriction
        this.forbiden = forbiden
        this.skills = skills
        this.additionalRestriction = additionalRestriction
    }
}

export default GearCard