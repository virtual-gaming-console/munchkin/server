enum Equipable {
    YES = "EQUIPABLE_YES",
    WITH_SWAP = "EQUIPABLE_WITH_SWAP",
    NO = "EQUIPABLE_NO",
}

export default Equipable