class Choice {
    name: string
    cardId?: number
    isVisible?: boolean = true

    constructor(
        name: string,
        cardId?: number,
        isVisible?: boolean,
    ) {
        this.name = name
        this.cardId = cardId
        if (isVisible !== undefined) {
            this.isVisible = isVisible
        }
    }
}

export default Choice