import ICard from "./ICard";
import CardType from "./CardType";
import Deck from "./Deck";
import Player from "../Player/Player";

abstract class DungeonCard implements ICard {
    id: number;
    name: string;
    type: CardType;
    isTreasure: boolean = false
    isVisible: boolean = false
    from?: Deck<any>

    isPlayable: Function = (handler: Player) => false

    constructor(id: number, name: string, type: CardType) {
        this.id = id
        this.name = name
        this.type = type
    }
}

export default DungeonCard