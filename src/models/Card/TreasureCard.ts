import ICard from "./ICard";
import CardType from "./CardType";
import Deck from "./Deck";
import Player from "../Player/Player";

abstract class TreasureCard implements ICard {
    id: number;
    name: string;
    type: CardType;
    isTreasure: boolean = true
    isVisible: boolean = false
    sellingPrice: number
    from?: Deck<any>

    isPlayable: Function = (handler: Player) => false

    constructor(id: number, name: string, type: CardType, sellingPrice: number) {
        this.id = id
        this.name = name
        this.type = type
        this.sellingPrice = sellingPrice
    }
}

export default TreasureCard