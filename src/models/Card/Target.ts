class Target {
    name: string
    players?: Array<string>
    cards?: Array<number>

    constructor(
        name: string,
        players?: Array<string>,
        cards?: Array<number>,
    ) {
        this.name = name
        this.cards = cards
        this.players = players
    }
}

export default Target