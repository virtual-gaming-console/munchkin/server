class CardInstantiator {
    cardId: number
    instantiate: Function

    constructor(
        cardId: number,
        instantiate: Function
    ) {
        this.cardId = cardId
        this.instantiate = instantiate
    }
}

export default CardInstantiator