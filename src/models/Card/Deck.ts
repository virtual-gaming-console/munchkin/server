import ICard from "./ICard";

class Deck<T> {
    private _cards: Array<T>
    addCardCallback?: Function
    removeCardCallback?: Function
    whenEmpty?: Function
    isVisible: boolean

    get cards(): Array<T> {
        return this._cards.map(card => ({ ...card, isVisible: this.isVisible }))
    }

    constructor(
        cards: Array<T>,
        addCardCallback?: Function,
        removeCardCallback?: Function,
        isVisible: boolean = false,
        whenEmpty?: Function,
    ) {
        this._cards = cards
        this._cards.forEach(card => {
            let cardItem = (card as unknown as ICard);
            cardItem.from = this
        });
        this.addCardCallback = addCardCallback
        this.removeCardCallback = removeCardCallback
        this.isVisible = isVisible
        this.whenEmpty = whenEmpty
    }

    remove(card: ICard) {
        const cardIndex = this.cards.findIndex(cardItem => (cardItem as unknown as ICard).id === card.id)
        this._cards.splice(cardIndex, 1)
        if (this.removeCardCallback) {
            this.removeCardCallback(card, cardIndex)
        }

        if (this._cards.length === 0 && this.whenEmpty) {
            this.whenEmpty()
        }
    }

    add(card: T): void {
        this._cards.push(card);
        let cardItem = (card as unknown as ICard);
        if (cardItem.from) {
            cardItem.from.remove(cardItem);
        }
        if (this.addCardCallback) {
            this.addCardCallback(card)
        }
        cardItem.from = this
    }

    toString(): string {
        return this._cards.map(card => (card as unknown as ICard).name).join(',');
    }

    shuffle(): void {
        var currentIndex = this._cards.length, temporaryValue, randomIndex;

        while (0 !== currentIndex) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            temporaryValue = this._cards[currentIndex];
            this._cards[currentIndex] = this._cards[randomIndex];
            this._cards[randomIndex] = temporaryValue;
        }
    }

    randomCard(): T {
        return this._cards[Math.floor(Math.random() * this._cards.length)]
    }
}

export default Deck