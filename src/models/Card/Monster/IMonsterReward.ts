interface IMonsterReward {
    treasures: number;
    experience: number;
}

export default IMonsterReward
