import DungeonCard from "../DungeonCard"
import CardType from "../CardType"
import MonsterAction from "./MonsterAction"
import MonsterType from "./MonsterType"
import Player from "../../Player/Player"
import TurnPhase from "../../TurnPhase"
import IMonsterPassiv from "./IMonsterPassiv"
import IMonsterReward from "./IMonsterReward"
import BattleModifier from "../../Battle/Modifier"

class MonsterCard extends DungeonCard {
    level: number
    reward: IMonsterReward
    passivs?: Array<IMonsterPassiv>
    action?: MonsterAction
    monsterType?: MonsterType

    explode: Function
    encounter?: Function

    isPlayable: Function = (handler: Player) => handler.currentGame.turnPhase === TurnPhase.STANDBY && handler.currentGame.phasesThisTurn.find(x => x === TurnPhase.COMBAT) === undefined && handler.currentGame.whoIsPlaying().deviantUser?.id === handler.deviantUser?.id

    static modifierInEnemySide(modifier: BattleModifier) {
        return modifier.power !== undefined
    }

    constructor(
        id: number,
        name: string,
        level: number,
        reward: IMonsterReward,
        explode: Function,
        passivs?: Array<IMonsterPassiv>,
        action?: MonsterAction,
        monsterType?: MonsterType
    ) {
        super(id, name, CardType.MONSTER)
        this.level = level
        this.reward = reward
        this.explode = explode
        this.passivs = passivs
        this.action = action
        this.monsterType = monsterType
    }
}

export default MonsterCard