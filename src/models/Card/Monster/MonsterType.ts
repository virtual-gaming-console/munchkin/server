enum MonsterType {
    UNDEAD = "MONSTER_TYPE_UNDEAD",
    NONE = "MONSTER_TYPE_NONE"
}

export default MonsterType