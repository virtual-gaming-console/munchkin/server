import PlayerRaceType from "../../Player/PlayerRace/PlayerRaceType";
import PlayerClassType from "../../Player/PlayerClass/PlayerClassType";
import Gender from "../../Player/Gender";
import MonsterAction from "./MonsterAction";
import BattleModifier from "../../Battle/Modifier";
import PassiveSkill from "../../Player/Skill/PassiveSkill";

interface IMonsterPassiv {
    restriction?: PlayerRaceType | PlayerClassType | Array<PlayerClassType | PlayerRaceType> | PassiveSkill | Gender | number
    modifier?: BattleModifier
    action?: MonsterAction
}

export default IMonsterPassiv
