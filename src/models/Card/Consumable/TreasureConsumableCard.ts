import Player from "../../Player/Player";
import TurnPhase from "../../TurnPhase";
import CardType from "../CardType";
import TreasureCard from "../TreasureCard";
import IConsumableCard from "./ConsumableCard";

class TreasureConsumableCard extends TreasureCard implements IConsumableCard {
    triggerPhases?: Array<TurnPhase>
    triggerEffect: Function
    customIsPlayable: Function
    getTargets: Function

    isPlayable: Function = (handler: Player) => {
        if (this.triggerPhases && this.triggerPhases.find(phase => handler.currentGame.turnPhase === phase) === undefined) {
            return false
        }

        return this.customIsPlayable(handler)
    }

    constructor(
        id: number,
        name: string,
        sellingPrice: number,
        customIsPlayable: Function,
        getTargets: Function,
        triggerEffect: Function,
        triggerPhases?: Array<TurnPhase>,
    ) {
        super(id, name, CardType.CONSUMABLE, sellingPrice)
        this.customIsPlayable = customIsPlayable
        this.getTargets = getTargets
        this.triggerEffect = triggerEffect
        if (triggerPhases) {
            this.triggerPhases = triggerPhases
        }
    }
}

export default TreasureConsumableCard