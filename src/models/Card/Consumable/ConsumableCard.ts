import TurnPhase from "../../TurnPhase";
import ICard from "../ICard";

interface IConsumableCard extends ICard {
    triggerEffect: Function
    customIsPlayable: Function
    getTargets: Function
    triggerPhases?: Array<TurnPhase>
}

export default IConsumableCard