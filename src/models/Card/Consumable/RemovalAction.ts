enum RemovalAction {
    AFTER_BATTLE = 'REMOVAL_ACTION_AFTER_BATTLE',
    LOSING_HEAD = 'REMOVAL_ACTION_LOSING_HEAD',
}

export default RemovalAction