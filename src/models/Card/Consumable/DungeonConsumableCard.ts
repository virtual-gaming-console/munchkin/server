import Player from "../../Player/Player";
import TurnPhase from "../../TurnPhase";
import CardType from "../CardType";
import DungeonCard from "../DungeonCard";
import IConsumableCard from "./ConsumableCard";

class DungeonConsumableCard extends DungeonCard implements IConsumableCard {
    triggerPhases?: Array<TurnPhase>
    triggerEffect: Function
    customIsPlayable: Function
    getTargets: Function

    isPlayable: Function = (handler: Player) => {
        if (this.triggerPhases && this.triggerPhases.find(phase => handler.currentGame.turnPhase === phase) === undefined) {
            return false
        }

        return this.customIsPlayable(handler)
    }

    constructor(
        id: number,
        name: string,
        customIsPlayable: Function,
        getTargets: Function,
        triggerEffect: Function,
        triggerPhases?: Array<TurnPhase>,
    ) {
        super(id, name, CardType.CONSUMABLE)
        this.customIsPlayable = customIsPlayable
        this.getTargets = getTargets
        this.triggerEffect = triggerEffect
        if (triggerPhases) {
            this.triggerPhases = triggerPhases
        }
    }
}

export default DungeonConsumableCard