import Game from "../../Game";
import Player from "../../Player/Player";
import PlayerRaceType from "../../Player/PlayerRace/PlayerRaceType";
import RACES from "../../Player/PlayerRace/RACES";
import TurnPhase from "../../TurnPhase";
import CardType from "../CardType";
import DungeonCard from "../DungeonCard";
import Target from "../Target";
import IConsumableCard from "./ConsumableCard";

class RaceCard extends DungeonCard implements IConsumableCard {
    triggerPhases: Array<TurnPhase> = [TurnPhase.STANDBY, TurnPhase.FIRST_STANDBY]
    triggerEffect: Function = (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
        const race = RACES[this.value]
        launcher.race = new race()
        launcher.hand.remove(this)

        return true
    }
    customIsPlayable: Function = (handler: Player) => {
        return true
    }
    getTargets: Function = (handler: Player) => {
        return [
            new Target('Moi', undefined, undefined)
        ]
    }
    value: PlayerRaceType

    isPlayable: Function = (handler: Player) => {
        if (handler.currentGame.whoIsPlaying().deviantUser?.id !== handler.deviantUser?.id) {
            return false
        }

        if (this.triggerPhases && this.triggerPhases.find(phase => handler.currentGame.turnPhase === phase) === undefined) {
            return false
        }

        if (handler.race.type !== PlayerRaceType.HUMAN) {
            return false
        }

        return this.customIsPlayable(handler)
    }

    constructor(
        id: number,
        name: string,
        value: PlayerRaceType
    ) {
        super(id, name, CardType.RACE)
        this.value = value
    }
}

export default RaceCard