import Game from "../../Game";
import Player from "../../Player/Player";
import CLASSES from "../../Player/PlayerClass/CLASSES";
import PlayerClassType from "../../Player/PlayerClass/PlayerClassType";
import TurnPhase from "../../TurnPhase";
import CardType from "../CardType";
import DungeonCard from "../DungeonCard";
import Target from "../Target";
import IConsumableCard from "./ConsumableCard";

class ClassCard extends DungeonCard implements IConsumableCard {
    triggerPhases: Array<TurnPhase> = [TurnPhase.STANDBY, TurnPhase.FIRST_STANDBY]
    triggerEffect: Function = (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
        const pClass = CLASSES[this.value]
        launcher.class = new pClass()
        launcher.hand.remove(this)

        return true
    }
    customIsPlayable: Function = (handler: Player) => {
        return true
    }
    getTargets: Function = (handler: Player) => {
        return [
            new Target('Moi', undefined, undefined)
        ]
    }
    value: PlayerClassType

    isPlayable: Function = (handler: Player) => {
        if (handler.currentGame.whoIsPlaying().deviantUser?.id !== handler.deviantUser?.id) {
            return false
        }

        if (this.triggerPhases && this.triggerPhases.find(phase => handler.currentGame.turnPhase === phase) === undefined) {
            return false
        }

        if (handler.class !== undefined) {
            return false
        }

        return this.customIsPlayable(handler)
    }

    constructor(
        id: number,
        name: string,
        value: PlayerClassType
    ) {
        super(id, name, CardType.CLASS)
        this.value = value
    }
}

export default ClassCard