import BattleModifier from "../../Battle/Modifier";
import Player from "../../Player/Player";
import CardType from "../CardType";
import DungeonCard from "../DungeonCard";
import IConsumableCard from "./ConsumableCard";
import RemovalAction from "./RemovalAction";

class CurseCard extends DungeonCard implements IConsumableCard {
    triggerEffect: Function
    customIsPlayable: Function
    getTargets: Function
    residualEffect?: {
        modifier: BattleModifier,
        removalAction: RemovalAction
    }

    isPlayable: Function = (handler: Player) => {
        return this.customIsPlayable(handler)
    }

    constructor(
        id: number,
        name: string,
        customIsPlayable: Function,
        getTargets: Function,
        triggerEffect: Function,
        residualEffect?: {
            modifier: BattleModifier,
            removalAction: RemovalAction
        }
    ) {
        super(id, name, CardType.CURSE)
        this.customIsPlayable = customIsPlayable
        this.getTargets = getTargets
        this.triggerEffect = triggerEffect
        this.residualEffect = residualEffect
    }
}

export default CurseCard