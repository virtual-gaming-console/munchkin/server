import Choice from "../Card/Choice";
import Target from "../Card/Target";
import DeviantUser from "../Player/DeviantUser";
import PlayerClassType from "../Player/PlayerClass/PlayerClassType";
import PlayerRaceType from "../Player/PlayerRace/PlayerRaceType";
import TurnPhase from "../TurnPhase";

interface WsMessageData {
    cards?: Array<number>
    skills?: Array<number>
    players?: Array<string>
    lobbyCode?: string
    error?: string
    player?: DeviantUser
    cardId?: number
    isVisible?: boolean
    turnPhase?: TurnPhase
    time?: number
    level?: number
    dice?: number
    min?: number
    max?: number
    allowCancel?: boolean

    additionnalClassRace?: boolean
    classOrRaceType?: PlayerClassType | PlayerRaceType | undefined

    modifierId?: number
    power?: number
    treasures?: Array<number>
    flee?: number
    experience?: number

    targets?: Array<Target>
    isDead?: boolean
    choices?: Array<Choice>
    name?: string
}

export default WsMessageData