import uniqid from 'uniqid'
import WebSocket from 'ws'
import Game from '../Game'
import WsMessage from './WsMessage'

class MyWebSocket {
    static sockets: Array<MyWebSocket> = []

    socket: WebSocket
    id: string = uniqid()
    rooms: Array<string> = []
    isAlive: boolean = true
    lastMessageTime: number = Date.now()
    playerId?: string
    currentGame?: Game

    constructor(socket: WebSocket) {
        this.socket = socket
        MyWebSocket.sockets.push(this)
    }

    broadcast = (message: WsMessage, criteria?: Function) => {
        MyWebSocket.sockets.forEach((client) => {
            if (client.socket.readyState === WebSocket.OPEN && client.rooms[0] === this.rooms[0] && (criteria ? criteria(client, this) : true)) {
                client.send(message);
            }
        });
    }

    send = (message: WsMessage) => {
        if (!message.playerId && this.playerId) {
            message.playerId = this.playerId
        }
        this.lastMessageTime = Date.now()
        this.socket.send(JSON.stringify(message))
    }

    broadcastToOthers = (message: WsMessage, criteria?: Function) => {
        this.broadcast(message, (client: MyWebSocket, socket: MyWebSocket) => client.id !== socket.id && (criteria ? criteria(client, socket) : true))
    }

    join = (roomCode: string) => {
        this.rooms = [...this.rooms.filter(x => x !== roomCode), roomCode]
    }

    leave = (roomCode: string) => {
        this.rooms = [...this.rooms.filter(x => x !== roomCode)]
    }

    close = () => {
        MyWebSocket.sockets = MyWebSocket.sockets.filter(socket => socket.id !== this.id)
        this.socket.terminate()
    }
}

export default MyWebSocket