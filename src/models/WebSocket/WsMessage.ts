import WsMessageData from "./WsMessageData"

class IWsMessage {
    title: string
    data?: WsMessageData
    playerId?: string

    constructor(title: string) {
        this.title = title
    }
}

export default IWsMessage