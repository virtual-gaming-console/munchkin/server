import Player from "./Player/Player"
import Deck from "./Card/Deck"
import CARDS from "../mocks/CARDS"
import TreasureCard from "./Card/TreasureCard"
import ICard from "./Card/ICard"
import TurnPhase from "./TurnPhase"
import MyWebSocket from "./WebSocket/MyWebSocket"
import DungeonCard from "./Card/DungeonCard"
import Battle from "./Battle/Battle"

class Game {
    code: string
    websocket: MyWebSocket
    players: Array<Player>
    dungeonDeck: Deck<ICard> = new Deck<ICard>(
        [...CARDS.DUNGEONS.map(instantiator => instantiator && instantiator.instantiate())],
        undefined,
        undefined,
        false,
        () => {
            this.discard.cards.filter(card => !card.isTreasure).forEach(card => this.dungeonDeck.add(card as DungeonCard))
            this.dungeonDeck.shuffle()
        }
    )
    treasureDeck: Deck<TreasureCard> = new Deck<TreasureCard>(
        [...CARDS.TREASURES.map(instantiator => instantiator && instantiator.instantiate())],
        undefined,
        undefined,
        false,
        () => {
            this.discard.cards.filter(card => card.isTreasure).forEach(card => this.treasureDeck.add(card as TreasureCard))
            this.treasureDeck.shuffle()
        }
    )
    turnPhase: TurnPhase = TurnPhase.NONE
    phasesThisTurn: Array<TurnPhase> = []
    currentPlayerIndex: number = 0
    currentBattle?: Battle
    discard: Deck<ICard> = new Deck(
        [],
        (card: ICard) => this.websocket?.broadcast({ title: 'game-discard-add', data: { cardId: card.id } }),
        (card: ICard) => this.websocket?.broadcast({ title: 'game-discard-remove', data: { cardId: card.id } }),
        true
    )
    timeout?: any
    choiceCallbacks: Array<{
        sendChoices: Function,
        choiceCallback: Function,
        sent?: Boolean,
        isMultiple?: Boolean
    }> = []

    constructor(
        code: string,
        players: Array<Player>,
        websocket: MyWebSocket
    ) {
        this.code = code
        this.players = players
        this.websocket = websocket
        this.dungeonDeck.shuffle()
        this.treasureDeck.shuffle()
    }

    whoIsPlaying(): Player {
        return this.players[this.currentPlayerIndex]
    }

    playerWithId(playerId: string): Player | undefined {
        return this.players.find(player => player.deviantUser?.id === playerId)
    }

    lowestLevelPlayers(): Array<Player> {
        const minLevel = Math.min(...this.players.map(player => player.level))
        return this.players.filter(player => player.level === minLevel)
    }

    highestLevelPlayers(): Array<Player> {
        const maxLevel = Math.max(...this.players.map(player => player.level))
        return this.players.filter(player => player.level === maxLevel)
    }

    nextPlayerIndex(player?: Player): number {
        let index = this.currentPlayerIndex
        if (player) {
            index = this.players.findIndex(playerItem => playerItem.deviantUser?.id === player.deviantUser?.id)
        }

        return (index + 1) % this.players.length
    }

    previousPlayerIndex(player?: Player): number {
        let index = this.currentPlayerIndex
        if (player) {
            index = this.players.findIndex(playerItem => playerItem.deviantUser?.id === player.deviantUser?.id)
        }

        return index - 1 >= 0 ? index - 1 : this.players.length - 1
    }

    nextPlayer(player?: Player): Player {
        return this.players[this.nextPlayerIndex(player)]
    }

    previousPlayer(player?: Player): Player {
        return this.players[this.previousPlayerIndex(player)]
    }

    goToNextPlayer(): void {
        this.players.filter(player => player.wasDead).forEach(deadPlayer => {
            deadPlayer.revive()
            this.whoIsPlaying().wasDead = false
        })
        this.players.forEach(player => {
            player.passivSkillsUsedThisTurn = []
            player.activeSkillsUsedThisTurn = []
        })
        this.currentPlayerIndex = (this.currentPlayerIndex + 1) % this.players.length
        this.phasesThisTurn = []
        if (this.whoIsPlaying().isDead) {
            this.whoIsPlaying().isDead = false
            this.whoIsPlaying().drawStartingHand()
        }
    }
}

export default Game