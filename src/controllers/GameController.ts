import MyWebSocket from "../models/WebSocket/MyWebSocket"
import WsMessage from "../models/WebSocket/WsMessage"
import Game from "../models/Game"
import Player from "../models/Player/Player"
import TurnPhase from "../models/TurnPhase"
import GearCard from "../models/Card/Gear/GearCard"
import CardType from "../models/Card/CardType"
import MonsterCard from "../models/Card/Monster/MonsterCard"
import PassiveSkill from "../models/Player/Skill/PassiveSkill"
import Battle from "../models/Battle/Battle"
import IConsumableCard from "../models/Card/Consumable/ConsumableCard"
import Target from "../models/Card/Target"
import Choice from "../models/Card/Choice"
import CurseCard from "../models/Card/Consumable/CurseCard"
import WishingRing from "../mocks/CARDS/CONSUMABLES/TREASURES/WishingRing"
import https from 'https'
import DeviantUser from "../models/Player/DeviantUser"
import Resurect from "../models/Player/Skill/Resurect"

const SECONDS_BETWEEN_TURNS = 30

class GameController {
    static games: Array<Game> = []

    static onMessage = (myWs: MyWebSocket, message: WsMessage) => {
        switch (message.title) {
            case "init-game":
                if (message?.data?.lobbyCode) {
                    GameController.initGame(myWs, message.data.lobbyCode)
                }
                break
            case "join-game":
                if (message?.data?.lobbyCode && message.playerId) {
                    GameController.joinGame(myWs, message.data.lobbyCode, message.playerId)
                }
                break
            case "equip-gear":
                if (message?.data?.cards) {
                    GameController.equipGear(myWs, message?.data?.cards)
                }
                break
            case "give-card":
                if (message?.data?.cardId && message.data.targets) {
                    GameController.giveCard(myWs, message.data.targets[0])
                }
                break
            case "try-fleeing":
                if (myWs.currentGame) {
                    GameController.tryFleeing(myWs.currentGame)
                    GameController.annonceNewTurn(myWs.currentGame)
                }
                break
            case "ask-target":
                if (message.data?.cardId && message.data?.cardId > 0) {
                    GameController.askTarget(myWs, message.data?.cardId)
                } else if (message.data?.skills) {
                    GameController.askTarget(myWs, undefined, message.data?.skills[0])
                }
                break
            case "play-skill":
                if (message.data?.skills && message.data.targets) {
                    GameController.playSkill(myWs, message.data?.skills[0], message.data.targets)
                }
                break
            case "play-consumable":
                if (message.data?.cardId && message.data.targets) {
                    GameController.playConsumable(myWs, message.data?.cardId, message.data.targets[0])
                }
                break
            case "play-curse":
                if (message.data?.cardId && message.data.targets) {
                    GameController.playCurse(myWs, message.data?.cardId, message.data.targets[0])
                }
                break
            case "play-monster":
                if (message.data?.cardId) {
                    GameController.playMonster(myWs, message.data?.cardId)
                }
                break
            case "choice-made":
                if (myWs.currentGame && message.data?.choices) {
                    GameController.choiceMade(myWs.currentGame, message.data?.choices)
                }
                break
            case "player-help-propose":
                if (myWs.currentGame && message.data?.treasures) {
                    GameController.proposeHelp(myWs, message.data?.treasures)
                }
                break
            case "player-help-accept":
                if (myWs.currentGame && message.data?.players && message.data?.treasures) {
                    GameController.acceptHelp(myWs, message.data?.players[0], message.data?.treasures)
                }
                break
            case "end-turn":
                GameController.tryEndTurn(myWs)
                break
            case "disconnect":
                GameController.onDisconnect(myWs)
                break
            default:
                break
        }

        console.log(message)
    }

    static initGame = (myWs: MyWebSocket, lobbyCode: string) => {
        const game = new Game(lobbyCode, [], myWs);
        GameController.games = [
            ...GameController.games.filter(game => game.code !== lobbyCode),
            game
        ]
        myWs.currentGame = game
        myWs.join(lobbyCode)
        myWs.send({ title: "init-game-success" })
    }

    static joinGame = (myWs: MyWebSocket, lobbyCode: string, playerId: string) => {
        const game = GameController.games.find(game => game.code === lobbyCode)

        if (!game) {
            myWs.send({ title: "error", data: { error: "Player not found !" } })
        } else {
            const player = new Player(game)

            if (player) {
                if (game.players.length >= 1) {
                    game.players.forEach(playerItem => {
                        myWs.send({ title: "join-game-success", data: { player: playerItem.deviantUser } })
                    })
                }

                game?.players.push(player)
                myWs.join(lobbyCode)
                myWs.playerId = playerId
                myWs.currentGame = game
                player.websocket = myWs
                player.deviantUser = {
                    id: playerId,
                    nickname: 'Joueur ' + game.players.length,
                    avatarImage: ''
                }

                myWs.broadcast({ title: "join-game-success", data: { player: player.deviantUser } })

                if (game.players.length >= 2 && !player.hand.cards.length) {
                    setTimeout(() => {
                        GameController.drawStartingHands(game)
                    }, 3000)
                }
            } else {
                myWs.send({ title: "error", data: { error: "Player not found !" } })
            }
        }
    }

    static drawStartingHands = (game: Game) => {
        game.players.forEach(player => {
            player.drawStartingHand()
        })
        game.websocket.broadcast({ title: "game-start" })
        GameController.nextTurnPhase(game)
    }

    static resetCountDownToNextTurnPhase = (game: Game, sendMessage: boolean = true) => {
        clearTimeout(game.timeout)
        if (sendMessage) {
            game.websocket.send({
                title: "new-timer",
                data: {
                    time: SECONDS_BETWEEN_TURNS
                },
                playerId: game.players[game.currentPlayerIndex || 0].deviantUser?.id
            })
            game.players.forEach(player => {
                player.websocket?.send({
                    title: "new-timer",
                    data: {
                        cards: player.playableCardIds(),
                        skills: player.usableSkillIds,
                        time: SECONDS_BETWEEN_TURNS
                    },
                    playerId: game.players[game.currentPlayerIndex || 0].deviantUser?.id
                })
            })
        }

        if (game.choiceCallbacks.length && !game.choiceCallbacks[0].sent) {
            game.choiceCallbacks[0].sent = true
            game.phasesThisTurn.push(game.turnPhase)
            game.choiceCallbacks[0].sendChoices()
        }

        game.timeout = setTimeout(() => {
            GameController.nextTurnPhase(game)
        }, SECONDS_BETWEEN_TURNS * 1000)
    }

    static nextTurnPhase = (game: Game) => {
        if (game.choiceCallbacks.length && game.choiceCallbacks[0].sent) {
            const endTurn = game.choiceCallbacks[0].choiceCallback([])
            game.choiceCallbacks.shift()
            if (!endTurn) {
                return GameController.resetCountDownToNextTurnPhase(game)
                // if (game.choiceCallbacks.length && !game.choiceCallbacks[0].sent) {
                //     game.choiceCallbacks[0].sent = true
                //     game.phasesThisTurn.push(game.turnPhase)
                //     game.choiceCallbacks[0].sendChoices()
                // }
                // return
            }
        }

        if (game.choiceCallbacks.length && !game.choiceCallbacks[0].sent) {
            return GameController.resetCountDownToNextTurnPhase(game)
            // game.choiceCallbacks[0].sent = true
            // game.phasesThisTurn.push(game.turnPhase)
            // return game.choiceCallbacks[0].sendChoices()
        }

        switch (game.turnPhase) {
            case TurnPhase.FIRST_STANDBY:
                GameController.firstStandByPhase(game)
                break;
            case TurnPhase.COMBAT:
                GameController.combatPhase(game)
                break;
            case TurnPhase.STANDBY:
                GameController.standByPhase(game)
                break;
            case TurnPhase.FLEE:
                GameController.fleePhase(game)
                break;
            case TurnPhase.CHARITY:
                GameController.charityPhase(game)
                break;
            case TurnPhase.DEATH:
                GameController.deathPhase(game)
                break;
            case TurnPhase.ROLL:
                GameController.rollPhase(game)
                break;
            default:
                game.turnPhase = TurnPhase.FIRST_STANDBY
                break;
        }

        game.phasesThisTurn.push(game.turnPhase)
        GameController.annonceNewTurn(game)

        if (game.choiceCallbacks.length && !game.choiceCallbacks[0].sent) {
            game.choiceCallbacks[0].sendChoices()
            game.choiceCallbacks[0].sent = true
            game.phasesThisTurn.push(game.turnPhase)
        }
    }

    static annonceNewTurn = (game: Game) => {
        game.websocket.send({
            title: "new-turn-phase",
            data: {
                turnPhase: game.turnPhase,
                time: SECONDS_BETWEEN_TURNS
            },
            playerId: game.players[game.currentPlayerIndex || 0].deviantUser?.id
        })
        game.players.forEach(player => {
            player.websocket?.send({
                title: "new-turn-phase",
                data: {
                    turnPhase: game.turnPhase,
                    cards: player.playableCardIds(),
                    skills: player.usableSkillIds,
                    time: SECONDS_BETWEEN_TURNS
                },
                playerId: game.players[game.currentPlayerIndex || 0].deviantUser?.id
            })
        })
        GameController.resetCountDownToNextTurnPhase(game, false)
    }

    static firstStandByPhase = (game: Game) => {
        const currentPlayer = game.whoIsPlaying()

        if (currentPlayer.activeSkillsUsedThisTurn.find(skill => skill.id === new Resurect().id) !== undefined) {
            game.turnPhase = TurnPhase.STANDBY
            return
        }

        if (game.dungeonDeck.cards.length === 0) {
            game.discard.cards.filter(card => !card.isTreasure).forEach(card => game.dungeonDeck.add(card))
        }
        let dungeonCard = game.dungeonDeck.cards[0]
        game.websocket.broadcast({ title: "open-the-door", data: { cardId: dungeonCard.id }, playerId: currentPlayer.deviantUser?.id })
        if (dungeonCard.type === CardType.MONSTER) {
            game.currentBattle = new Battle(game, currentPlayer, dungeonCard as MonsterCard)
            game.turnPhase = TurnPhase.COMBAT
        } else if (dungeonCard.type === CardType.CURSE) {
            if (currentPlayer.hasPassiveSkill(PassiveSkill.NO_CURSE_ON_DUNGEON)) {
                const cardInDeck = game.dungeonDeck.cards.find(card => card.id === dungeonCard.id)
                if (cardInDeck) {
                    game.discard.add(cardInDeck)
                }
                game.turnPhase = TurnPhase.STANDBY
            } else if (currentPlayer.hand.cards.find(card => card.id === WishingRing.cardId) !== undefined) {
                game.choiceCallbacks.push({
                    sendChoices: () => {
                        const choices: Choice[] = currentPlayer.hand.cards.filter(card => card.id === WishingRing.cardId)
                            .map(card => ({ name: card.name, cardId: card.id, isVisible: true }))

                        currentPlayer.websocket?.send(
                            {
                                title: 'show-choices',
                                data: {
                                    choices,
                                    allowCancel: true,
                                    name: 'CONTRER LA MALEDICTION?'
                                }
                            }
                        )
                    },
                    choiceCallback: (choices: Choice[]) => {
                        const cardInDeck = game.dungeonDeck.cards.find(card => card.id === dungeonCard.id)
                        if (!choices.length) {
                            const effetTriggered = (dungeonCard as CurseCard).triggerEffect(
                                game,
                                currentPlayer,
                                [currentPlayer]
                            )
                            if (cardInDeck) {
                                if ((cardInDeck as CurseCard).residualEffect) {
                                    game.whoIsPlaying().curses.add(cardInDeck as CurseCard)
                                } else {
                                    game.discard.add(cardInDeck)
                                }
                            }
                        } else {
                            let card = currentPlayer.hand.cards.find(cardItem => cardItem.id === choices[0].cardId)
                            if (card && cardInDeck) {
                                game.discard.add(card)
                                game.discard.add(cardInDeck)
                                game.websocket.broadcast({ title: "card-played", data: { cardId: card.id }, playerId: currentPlayer.deviantUser?.id })
                            }
                        }
                        game.turnPhase = TurnPhase.STANDBY
                        GameController.annonceNewTurn(game)

                        return false
                    }
                })
            } else {
                const effetTriggered = (dungeonCard as CurseCard).triggerEffect(
                    game,
                    currentPlayer,
                    [currentPlayer]
                )
                const cardInDeck = game.dungeonDeck.cards.find(card => card.id === dungeonCard.id)
                if (cardInDeck) {
                    if ((cardInDeck as CurseCard).residualEffect) {
                        game.whoIsPlaying().curses.add(cardInDeck as CurseCard)
                    } else {
                        game.discard.add(cardInDeck)
                    }
                }
                game.turnPhase = TurnPhase.STANDBY
            }
        } else {
            if ((dungeonCard as IConsumableCard).isPlayable(currentPlayer) && (dungeonCard as IConsumableCard).getTargets(currentPlayer).length === 0) {
                const effetTriggered = (dungeonCard as IConsumableCard).triggerEffect(game, currentPlayer)
                if (effetTriggered) {
                    game.discard.add(dungeonCard)
                    game.websocket.broadcast({ title: "card-played", data: { cardId: dungeonCard.id }, playerId: game.whoIsPlaying().deviantUser?.id })
                }
            } else {
                currentPlayer.hand.add(dungeonCard)
            }
            game.turnPhase = TurnPhase.STANDBY
        }
    }

    static combatPhase = (game: Game) => {
        // if (game.phasesThisTurn.filter(phase => phase === TurnPhase.COMBAT).length > 1) {
        //     game.phasesThisTurn = game.phasesThisTurn.filter(phase => phase !== TurnPhase.COMBAT)
        //     return
        // }

        game.currentBattle?.fight()
    }

    static standByPhase = (game: Game) => {
        const currentPlayer = game.whoIsPlaying()
        if (game.players.find(player => player.rolling && player.rolling.callback) && game.turnPhase !== TurnPhase.ROLL) {
            game.turnPhase = TurnPhase.ROLL
            return
        }

        if (game.phasesThisTurn.find(phase => phase === TurnPhase.COMBAT) === undefined) {
            game.websocket.broadcast({ title: "search-the-room", playerId: game.whoIsPlaying().deviantUser?.id })
            if (game.dungeonDeck.cards[0].type === CardType.CONSUMABLE && (game.dungeonDeck.cards[0] as IConsumableCard).isPlayable(currentPlayer) && (game.dungeonDeck.cards[0] as IConsumableCard).getTargets(currentPlayer).length === 0) {
                const effetTriggered = (game.dungeonDeck.cards[0] as IConsumableCard).triggerEffect(game, currentPlayer)
                if (effetTriggered) {
                    game.websocket.broadcast({ title: "card-played", data: { cardId: game.dungeonDeck.cards[0].id }, playerId: game.whoIsPlaying().deviantUser?.id })
                    game.discard.add(game.dungeonDeck.cards[0])
                    return
                }
            } else {
                currentPlayer.hand.add(game.dungeonDeck.cards[0])
            }
        }

        if (game.choiceCallbacks.length || game.turnPhase === TurnPhase.DEATH) {
            return
        }

        if (currentPlayer.hand.cards.length > (5 + (currentPlayer.hasPassiveSkill(PassiveSkill.CAN_HAVE_SIX_CARDS_IN_HAND) ? 1 : 0))) {
            game.turnPhase = TurnPhase.CHARITY
            return
        }

        game.goToNextPlayer()
        game.turnPhase = TurnPhase.FIRST_STANDBY
    }

    static charityPhase = (game: Game) => {
        const currentPlayer = game.whoIsPlaying()
        const lowestLevelPlayers = game.lowestLevelPlayers().filter(player => player.deviantUser?.id !== currentPlayer.deviantUser?.id)
        while (currentPlayer.hand.cards.length > (5 + (currentPlayer.hasPassiveSkill(PassiveSkill.CAN_HAVE_SIX_CARDS_IN_HAND) ? 1 : 0))) {
            const card = currentPlayer.hand.randomCard()
            if (currentPlayer.level > Math.min(...game.players.map(x => x.level))) {
                const targetedPlayer = lowestLevelPlayers[Math.floor(Math.random() * lowestLevelPlayers.length)]
                targetedPlayer.hand.add(card)
                game.websocket.broadcast({ title: 'card-given', data: { cardId: card.id, players: [(currentPlayer.deviantUser?.id || 'X'), (targetedPlayer.deviantUser?.id || 'X')] } })
            } else {
                game.discard.add(card)
            }
        }
        game.goToNextPlayer()
        game.turnPhase = TurnPhase.FIRST_STANDBY
    }

    static deathPhase = (game: Game) => {
        if (game.currentBattle?.allies.length) {
            game.currentBattle.lose()
        } else {
            game.goToNextPlayer()
            game.turnPhase = TurnPhase.FIRST_STANDBY
        }
    }

    static fleePhase = (game: Game) => {
        game.currentBattle?.concludeFleeRolls()
        if (!game.currentBattle?.enemies.cards.length) {
            GameController.standByPhase(game)
        } else {
            game.currentBattle?.allies.forEach(({ player }) => {
                player.roll()
            })
        }
    }

    static rollPhase = (game: Game) => {
        game.players.filter(player => player.rolling).forEach(rollingPlayer => {
            if (rollingPlayer.rolling?.callback) {
                rollingPlayer.rolling?.callback(rollingPlayer.rolling.dice + rollingPlayer.rolling.modifier)
            }
            rollingPlayer.rolling = undefined
        })

        GameController.standByPhase(game)
    }

    static equipGear = (myWs: MyWebSocket, cards: Array<number>) => {
        const player = GameController.playerFromMyWs(myWs)
        if (player) {
            const card = player.hand.cards.find(card => card.id === cards[0])
            if (card && card.isPlayable(player) && card.type === CardType.GEAR) {
                if (player.equip(card as GearCard, cards[1])) {
                    GameController.resetCountDownToNextTurnPhase(player.currentGame)
                }
            } else {
                myWs.send({ title: "error", data: { error: "Gear card not found !" } })
            }
        } else {
            myWs.send({ title: "error", data: { error: "Player not found !" } })
        }
    }

    static giveCard = (myWs: MyWebSocket, target: Target) => {
        const player = GameController.playerFromMyWs(myWs)
        const playerId = target.players ? target.players[0] : null
        const cardId = target.cards ? target.cards[0] : null
        if (player) {
            if (player.deviantUser?.id !== player.currentGame.whoIsPlaying().deviantUser?.id) {
                myWs.send({ title: "error", data: { error: "You are not the player that is playing at the moment !" } })
                return
            }
            if (player.currentGame.turnPhase !== TurnPhase.CHARITY) {
                myWs.send({ title: "error", data: { error: "Cannot give card when not in charity phase !" } })
                return
            }
            let targetedPlayer
            if (playerId) {
                targetedPlayer = player.currentGame.lowestLevelPlayers().find(player => player.deviantUser?.id === playerId)
                if (!targetedPlayer) {
                    myWs.send({ title: "error", data: { error: "Targeted player not found or not with the lowest level !" } })
                    return
                }
            }
            const card = player.hand.cards.find(card => card.id === cardId)
            if (card) {
                if (targetedPlayer) {
                    player.giveCard(card, targetedPlayer)
                } else {
                    player.currentGame.discard.add(card)
                }
                if (player.hand.cards.length > (5 + (player.hasPassiveSkill(PassiveSkill.CAN_HAVE_SIX_CARDS_IN_HAND) ? 1 : 0))) {
                    GameController.resetCountDownToNextTurnPhase(player.currentGame)
                } else {
                    GameController.tryEndTurn(myWs)
                }
            } else {
                myWs.send({ title: "error", data: { error: "Gear card not found !" } })
            }
        } else {
            myWs.send({ title: "error", data: { error: "Player not found !" } })
        }
    }

    static askTarget = (myWs: MyWebSocket, cardId?: number, skillId?: number) => {
        const player = GameController.playerFromMyWs(myWs)
        if (player) {
            if (cardId) {
                const card = player.hand.cards.find(card => card.id === cardId)
                if (card) {
                    if (player.currentGame.turnPhase === TurnPhase.CHARITY) {
                        let targets: Target[]
                        if (player.level > Math.min(...player.currentGame.players.map(x => x.level))) {
                            targets = player.currentGame.lowestLevelPlayers().map(player => new Target((player.deviantUser?.nickname || 'X'), [(player.deviantUser?.id || 'X')], [cardId]))
                        } else {
                            targets = [new Target("LA DÉFAUSSE", undefined, [cardId])]
                        }
                        myWs.send({ title: 'show-targets', data: { targets } })
                    } else if (card.isPlayable(player)) {
                        myWs.send({ title: 'show-targets', data: { targets: (card as unknown as IConsumableCard).getTargets(player) } })
                    } else {
                        myWs.send({ title: "error", data: { error: "Card not playable at the moment !" } })
                    }
                } else {
                    myWs.send({ title: "error", data: { error: "Card not found or not in hand !" } })
                }
            } else if (skillId !== undefined) {
                const skill = player.activeSkills.find(skill => skill.id === skillId)
                if (skill) {
                    const targets = skill.getTargets(player)
                    if (targets) {
                        myWs.send({ title: 'show-targets-skill', data: { targets, skills: [skillId] } })
                    } else {
                        myWs.send({ title: "error", data: { error: "Skill not triggerable at the moment !" } })
                    }
                } else {
                    myWs.send({ title: "error", data: { error: "Skill not found or not in hand !" } })
                }
            } else {
                myWs.send({ title: "error", data: { error: "Nothing to get targets !" } })
            }
        } else {
            myWs.send({ title: "error", data: { error: "Player not found !" } })
        }
    }

    static playSkill = (myWs: MyWebSocket, skillId: number, targets: Target[]) => {
        const player = GameController.playerFromMyWs(myWs)
        if (player) {
            const skill = player.activeSkills.find(skill => skill.id === skillId)
            if (skill) {
                const activation = skill.activate(player, targets)
                if (activation) {
                    const twoParts = player.currentGame.choiceCallbacks.length && !player.currentGame.choiceCallbacks[0].sent
                    myWs.broadcast({ title: "player-skill-launched", data: { skills: [skillId], targets }, playerId: player.deviantUser?.id })
                    // myWs.broadcast({ title: "player-skill-launched" + twoParts ? '-two-parts' : '', data: { skills: [skillId] }, playerId: player.deviantUser?.id })
                    GameController.resetCountDownToNextTurnPhase(player.currentGame)
                    if (twoParts) {
                        player.currentGame.choiceCallbacks[0].sent = true
                        player.currentGame.phasesThisTurn.push(player.currentGame.turnPhase)
                        return player.currentGame.choiceCallbacks[0].sendChoices()
                    }
                } else {
                    myWs.send({ title: "error", data: { error: "Skill not triggerable at the moment !" } })
                }
            } else {
                myWs.send({ title: "error", data: { error: "Skill not found or not in hand !" } })
            }
        } else {
            myWs.send({ title: "error", data: { error: "Player not found !" } })
        }
    }

    static playConsumable = (myWs: MyWebSocket, cardId: number, target: Target) => {
        const player = GameController.playerFromMyWs(myWs)
        if (player) {
            let card = player.hand.cards.find(card => card.id === cardId)
            if ([CardType.CONSUMABLE, CardType.CLASS, CardType.RACE].find(type => card && card.type === type)) {
                if (card && card.isPlayable(player)) {
                    const effetTriggered = (card as IConsumableCard).triggerEffect(
                        player.currentGame,
                        player,
                        target.players?.map(playerId => GameController.playerFromId(player.currentGame, playerId)),
                        target.cards,
                        target
                    )
                    if (effetTriggered) {
                        if (player.hand.cards.find(card => card.id === cardId)) {
                            player.currentGame.discard.add(card)
                        }
                        player.currentGame.websocket.broadcast({ title: "card-played", data: { cardId: card.id }, playerId: player.deviantUser?.id })
                        GameController.resetCountDownToNextTurnPhase(player.currentGame)
                        if (player.currentGame.choiceCallbacks.length && !player.currentGame.choiceCallbacks[0].sent) {
                            player.currentGame.choiceCallbacks[0].sent = true
                            player.currentGame.phasesThisTurn.push(player.currentGame.turnPhase)
                            return player.currentGame.choiceCallbacks[0].sendChoices()
                        }
                    }
                } else {
                    myWs.send({ title: "error", data: { error: "Card not playable at the moment !" } })
                }
            } else {
                myWs.send({ title: "error", data: { error: "Card not found or not in hand !" } })
            }
        } else {
            myWs.send({ title: "error", data: { error: "Player not found !" } })
        }
    }

    static playCurse = (myWs: MyWebSocket, cardId: number, target: Target) => {
        const player = GameController.playerFromMyWs(myWs)
        if (player) {
            let card = player.hand.cards.find(card => card.id === cardId && card.type === CardType.CURSE) as CurseCard
            if (card && card.isPlayable(player)) {
                const targetedPlayer = GameController.playerFromId(player.currentGame, (target.players ? target.players[0] : 'X'))
                if (targetedPlayer && targetedPlayer.hand.cards.find(card => card.id === WishingRing.cardId) !== undefined) {
                    player.currentGame.choiceCallbacks.push({
                        sendChoices: () => {
                            const choices: Choice[] = targetedPlayer.hand.cards.filter(card => card.id === WishingRing.cardId)
                                .map(card => ({ name: card.name, cardId: card.id, isVisible: true }))

                            targetedPlayer.websocket?.send(
                                {
                                    title: 'show-choices',
                                    data: {
                                        choices,
                                        name: 'CONTRER LA MALEDICTION?'
                                    }
                                }
                            )
                        },
                        choiceCallback: (choices: Choice[]) => {
                            if (!choices.length) {
                                const effetTriggered = card.triggerEffect(
                                    player.currentGame,
                                    player,
                                    target.players?.map(playerId => GameController.playerFromId(player.currentGame, playerId)),
                                    target.cards,
                                    target
                                )
                                if (effetTriggered) {
                                    if (player.hand.cards.find(card => card.id === cardId)) {
                                        player.currentGame.discard.add(card)
                                    }
                                    player.currentGame.websocket.broadcast({ title: "card-played", data: { cardId: card.id }, playerId: player.deviantUser?.id })
                                    GameController.resetCountDownToNextTurnPhase(player.currentGame)
                                    if (player.currentGame.choiceCallbacks.length && !player.currentGame.choiceCallbacks[0].sent) {
                                        player.currentGame.choiceCallbacks[0].sent = true
                                        player.currentGame.phasesThisTurn.push(player.currentGame.turnPhase)
                                        player.currentGame.choiceCallbacks[0].sendChoices()
                                    }
                                }
                            } else {
                                let wishingRing = targetedPlayer.hand.cards.find(cardItem => cardItem.id === choices[0].cardId)
                                if (card && wishingRing) {
                                    player.currentGame.discard.add(card)
                                    player.currentGame.discard.add(wishingRing)
                                    player.currentGame.websocket.broadcast({ title: "card-played", data: { cardId: wishingRing.id }, playerId: targetedPlayer.deviantUser?.id })
                                }
                            }

                            return false
                        }
                    })
                } else {
                    const effetTriggered = card.triggerEffect(
                        player.currentGame,
                        player,
                        target.players?.map(playerId => GameController.playerFromId(player.currentGame, playerId)),
                        target.cards,
                        target
                    )
                    if (effetTriggered) {
                        if (player.hand.cards.find(card => card.id === cardId)) {
                            player.currentGame.discard.add(card)
                        }
                        player.currentGame.websocket.broadcast({ title: "card-played", data: { cardId: card.id }, playerId: player.deviantUser?.id })
                        GameController.resetCountDownToNextTurnPhase(player.currentGame)
                        if (player.currentGame.choiceCallbacks.length && !player.currentGame.choiceCallbacks[0].sent) {
                            player.currentGame.choiceCallbacks[0].sent = true
                            player.currentGame.phasesThisTurn.push(player.currentGame.turnPhase)
                            player.currentGame.choiceCallbacks[0].sendChoices()
                        }
                    }
                }
            } else {
                myWs.send({ title: "error", data: { error: "Card not playable at the moment !" } })
            }
        } else {
            myWs.send({ title: "error", data: { error: "Player not found !" } })
        }
    }

    static playMonster = (myWs: MyWebSocket, cardId: number) => {
        const player = GameController.playerFromMyWs(myWs)
        if (player) {
            const card = player.hand.cards.find(card => card.id === cardId)
            if (card && card.type === CardType.MONSTER) {
                if (card.isPlayable(player)) {
                    player.currentGame.websocket.broadcast({ title: "card-played", data: { cardId: card.id }, playerId: player.deviantUser?.id })
                    player.currentGame.currentBattle = new Battle(player.currentGame, player, card as MonsterCard)
                    player.currentGame.turnPhase = TurnPhase.COMBAT
                    player.currentGame.phasesThisTurn.push(TurnPhase.COMBAT)
                    GameController.annonceNewTurn(player.currentGame)
                    GameController.resetCountDownToNextTurnPhase(player.currentGame, false)

                    //TEMP TEST HELP
                    // player.currentGame.websocket.broadcast({ title: "battle-help-request", data: { treasures: [1] }, playerId: '0' })
                } else {
                    myWs.send({ title: "error", data: { error: "Card not playable at the moment !" } })
                }
            } else {
                myWs.send({ title: "error", data: { error: "Card not found or not in hand !" } })
            }
        } else {
            myWs.send({ title: "error", data: { error: "Player not found !" } })
        }
    }

    static tryEndTurn = (myWs: MyWebSocket) => {
        const player = GameController.playerFromMyWs(myWs)
        if (player) {
            if (player.deviantUser?.id === player.currentGame.players[player.currentGame.currentPlayerIndex].deviantUser?.id) {
                GameController.nextTurnPhase(player.currentGame)
            } else {
                myWs.send({ title: "error", data: { error: "Player not actualy playing !" } })
            }
        } else {
            myWs.send({ title: "error", data: { error: "Player not found !" } })
        }
    }

    static tryFleeing = (game: Game) => {
        game.currentBattle?.lose()
    }

    static choiceMade = (game: Game, choices: Choice[]) => {
        if (game.choiceCallbacks.length && game.choiceCallbacks[0].sent) {
            const endTurn = game.choiceCallbacks[0].choiceCallback(choices)
            game.choiceCallbacks.shift()
            if (endTurn) {
                GameController.nextTurnPhase(game)
            } else {
                GameController.resetCountDownToNextTurnPhase(game)
            }
        }
    }

    static proposeHelp = (myWs: MyWebSocket, treasures: Array<number>) => {
        const player = GameController.playerFromMyWs(myWs)
        if (player) {
            if (treasures.reduce((a, b) => a + b, 0) > (player.currentGame.currentBattle?.enemiesTreasures || 0)) {
                myWs.send({ title: "error", data: { error: "You're asking for more treasures than reward can give!" } })
            } else if (player.currentGame.currentBattle?.allies.length === 2) {
                myWs.send({ title: "error", data: { error: "Party already full!" } })
            } else if (!player.currentGame.currentBattle?.canHelp) {
                myWs.send({ title: "error", data: { error: "Can't receive help for this fight!" } })
            } else {
                myWs.broadcast({ title: "battle-help-request", data: { treasures }, playerId: player.deviantUser?.id })
                GameController.resetCountDownToNextTurnPhase(player.currentGame, true)
            }
        } else {
            myWs.send({ title: "error", data: { error: "Player not found !" } })
        }
    }

    static acceptHelp = (myWs: MyWebSocket, playerId: string, treasures: Array<number>) => {
        const player = GameController.playerFromMyWs(myWs)
        const helpingPlayer = player?.currentGame.players.find(player => player.deviantUser?.id === playerId)
        if (player) {
            if (treasures.reduce((a, b) => a + b, 0) > (player.currentGame.currentBattle?.enemiesTreasures || 0)) {
                myWs.send({ title: "error", data: { error: "You're asking for more treasures than reward can give!" } })
            } else if (player.currentGame.currentBattle?.allies.length === 2) {
                myWs.send({ title: "error", data: { error: "Party already full!" } })
            } else if (!player.currentGame.currentBattle?.canHelp) {
                myWs.send({ title: "error", data: { error: "Can't receive help for this fight!" } })
            } else if (helpingPlayer) {
                const treasureCounts = new Array<number>(player.currentGame.currentBattle.enemies.cards.length).fill(0)
                player.currentGame.currentBattle.enemies.cards.forEach((monster, index) => {
                    if (treasures[index] > monster.reward.treasures) {
                        if (treasureCounts.length <= (player.currentGame.currentBattle?.enemies.cards.length || 0)) {
                            treasureCounts.push(treasures[index] - monster.reward.treasures)
                        } else {
                            treasureCounts[treasureCounts.length - 1] += treasures[index] - monster.reward.treasures
                        }
                    }
                    treasureCounts[index] = treasures[index]
                })
                player.currentGame.currentBattle.newAlly(helpingPlayer, treasureCounts)
                myWs.broadcast({ title: "battle-help-accept", data: { treasures, players: [(player.deviantUser?.id || 'X'), (helpingPlayer.deviantUser?.id || 'X')] } })
                GameController.resetCountDownToNextTurnPhase(player.currentGame, true)
            }
        } else {
            myWs.send({ title: "error", data: { error: "Player not found !" } })
        }
    }

    static playerFromMyWs = (myWs: MyWebSocket): Player | undefined => {
        if (myWs.currentGame && myWs.playerId) {
            return myWs.currentGame.playerWithId(myWs.playerId)
        }

        return undefined
    }

    static playerFromId = (game: Game, playerId: string): Player | undefined => {
        return game.players.find(player => player.deviantUser?.id === playerId)
    }

    static onDisconnect = (myWs: MyWebSocket) => {
        // TO DO
        const player = GameController.playerFromMyWs(myWs)
        if (player) {
            player.currentGame.websocket.broadcast({ title: 'player-disconnected', playerId: player.deviantUser?.id })
            player.currentGame.players = player.currentGame.players.filter(pPlayer => pPlayer.deviantUser?.id !== player.deviantUser?.id)
        } else if (myWs.currentGame) {
            GameController.endGame(myWs.currentGame)
        }

        myWs.close();
    }

    static endGame = (game: Game) => {
        console.log('---- CLEAR GAME [' + game.code + '] ----')
        game.websocket.close()
        game.players.forEach(player => player.websocket?.close())
        clearTimeout(game.timeout)
        GameController.games = GameController.games.filter(gameItem => gameItem.code !== game.code)
    }

    static init = (callback: Function) => {
        // DO STUFF

        if (callback) {
            callback()
        }
    }
}

export default GameController