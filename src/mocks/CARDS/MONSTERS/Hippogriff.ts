import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"
import Choice from "../../../models/Card/Choice"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"

const Hippogriff = new CardInstantiator(
    42,
    () => new MonsterCard(
        42,
        "Hippogriff",
        16,
        {
            experience: 2,
            treasures: 4
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            const players: Player[] = []
            while (players.length !== (player.currentGame.players.length - 1)) {
                players.push(player.currentGame.previousPlayer(players.length ? players[players.length - 1] : player))
            }
            const choiceCount = player.hand.cards.length + player.arsenalCards.length


            for (let index = 0; index < (choiceCount >= players.length ? players.length : choiceCount); index++) {
                const playerItem = players[index]
                player.currentGame.choiceCallbacks.push({
                    sendChoices: () => {
                        const choices: Choice[] = [
                            ...player.arsenalCards.map(card => ({ name: card.name, cardId: card.id })),
                            ...player.hand.cards.map(card => ({ name: card.name, cardId: card.id, isVisible: false })),
                        ]

                        playerItem.websocket?.send(
                            {
                                title: 'show-choices',
                                data: {
                                    choices,
                                    name: 'PILLE UNE CARTE DE ' + player.deviantUser?.nickname.toUpperCase()
                                }
                            }
                        )
                    },
                    choiceCallback: (choices: Choice[]) => {
                        let choice = choices[0]
                        if (!choice) {
                            const choices: Choice[] = [
                                ...player.arsenalCards.map(card => ({ name: card.name, cardId: card.id })),
                                ...player.hand.cards.map(card => ({ name: card.name, cardId: card.id, isVisible: false })),
                            ]
                            choice = choices[Math.floor(Math.random() * choices.length)]
                        }

                        let card = player.hand.cards.find(cardItem => cardItem.id === choice.cardId)
                        if (!card) {
                            card = player.arsenalCards.find(cardItem => cardItem.id === choice.cardId)
                        }

                        if (card) {
                            player.giveCard(card, playerItem)
                        }

                        return true
                    }
                })
            }
        },
        [{
            restriction: 3,
            modifier: MODIFIERS.AUTO_FLEE()
        }]
    )
)

export default Hippogriff