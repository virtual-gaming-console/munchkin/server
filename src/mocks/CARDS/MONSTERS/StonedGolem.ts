import CardInstantiator from "../../../models/Card/CardInstantiator"
import MonsterAction from "../../../models/Card/Monster/MonsterAction"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"
import PlayerRaceType from "../../../models/Player/PlayerRace/PlayerRaceType"

const StonedGolem = new CardInstantiator(
    65,
    () => new MonsterCard(
        65,
        "Stoned Golem",
        14,
        {
            experience: 1,
            treasures: 4
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            player.die()
        },
        [{
            restriction: [PlayerRaceType.DWARF, PlayerRaceType.ELVEN, PlayerRaceType.HUMAN], // ALL RACES EXCEPT HOBBIT
            action: MonsterAction.STONED_GOLEM
        }]
    )
)

export default StonedGolem