import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"
import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"
import PassiveSkill from "../../../models/Player/Skill/PassiveSkill"

const LargeAngryChicken = new CardInstantiator(
    49,
    () => new MonsterCard(
        49,
        "Large Angry Chicken",
        2,
        {
            experience: 1,
            treasures: 1
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            player.level--
        },
        [{
            restriction: PassiveSkill.FIRE_DAMAGE,
            modifier: MODIFIERS.LARGE_ANGRY_CHICKEN()
        }]
    )
)

export default LargeAngryChicken