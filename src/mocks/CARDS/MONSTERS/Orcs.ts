import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"
import PlayerRaceType from "../../../models/Player/PlayerRace/PlayerRaceType"

const Orcs = new CardInstantiator(
    1,
    () => new MonsterCard(
        1,
        "3,872 Orcs",
        10,
        {
            experience: 1,
            treasures: 3
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            player.roll((dice: number) => {
                if (dice <= 2) {
                    player.die()
                } else {
                    player.level -= dice
                }
            })
        },
        [{
            restriction: PlayerRaceType.DWARF,
            modifier: MODIFIERS.ORCS()
        }]
    )
)

export default Orcs