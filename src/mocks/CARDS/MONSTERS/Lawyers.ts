import CardInstantiator from "../../../models/Card/CardInstantiator"
import Choice from "../../../models/Card/Choice"
import MonsterAction from "../../../models/Card/Monster/MonsterAction"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"
import PlayerClassType from "../../../models/Player/PlayerClass/PlayerClassType"

const Lawyers = new CardInstantiator(
    50,
    () => new MonsterCard(
        50,
        "Lawyers",
        6,
        {
            experience: 1,
            treasures: 2
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            const players: Player[] = []
            while (players.length !== (player.currentGame.players.length - 1)) {
                players.push(player.currentGame.nextPlayer(players.length ? players[players.length - 1] : player))
            }

            for (let index = 0; index < (player.hand.cards.length >= players.length ? players.length : player.hand.cards.length); index++) {
                const playerItem = players[index]
                player.currentGame.choiceCallbacks.push({
                    sendChoices: () => {
                        const choices: Choice[] = player.hand.cards.map(card => ({ name: card.name, cardId: card.id, isVisible: false }))

                        playerItem.websocket?.send(
                            {
                                title: 'show-choices',
                                data: {
                                    choices,
                                    name: 'PILLE UNE CARTE DE ' + player.deviantUser?.nickname.toUpperCase()
                                }
                            }
                        )
                    },
                    choiceCallback: (choices: Choice[]) => {
                        let choice = choices[0]
                        if (!choice) {
                            const choices = player.hand.cards.map(card => ({ name: card.name, cardId: card.id }))
                            choice = choices[Math.floor(Math.random() * choices.length)]
                        }

                        let card = player.hand.cards.find(cardItem => cardItem.id === choice.cardId)
                        if (card) {
                            player.giveCard(card, playerItem)
                        }

                        if ((player.currentGame.choiceCallbacks.length % players.length) === 1) {
                            player.hand.cards.forEach(card => player.currentGame.discard.add(card))
                        }

                        return true
                    }
                })
            }
        },
        [{
            restriction: PlayerClassType.THIEF,
            action: MonsterAction.LAWYERS
        }]
    )
)

export default Lawyers