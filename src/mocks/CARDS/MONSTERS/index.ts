import MaulRat from "./MaulRat"
import DroolingSlime from "./DroolingSlime"
import LameGoblin from "./LameGoblin"
import Crabs from "./Crabs"
import PottedPlant from "./PottedPlant"
import FlyingFrogs from "./FlyingFrogs"
import GelatinousOctahedron from "./GelatinousOctahedron"
import Harpies from "./Harpies"
import LargeAngryChicken from "./LargeAngryChicken"
import MrBones from "./MrBones"
import PitBull from "./PitBull"
import UndeadHorse from "./UndeadHorse"
import SnailsOnSpeed from "./SnailsOnSpeed"
import Lawyers from "./Lawyers"
import Pukachu from "./Pukachu"
import ShriekingGeek from "./ShriekingGeek"
import Ghoulfiends from "./Ghoulfiends"
import Platycore from "./Platycore"
import Gazebo from "./Gazebo"
import Amazon from "./Amazon"
import FaceSucker from "./FaceSucker"
import NetTroll from "./NetTroll"
import Orcs from "./Orcs"
import FloatingNose from "./FloatingNose"
import TongueDemon from "./TongueDemon"
import WannabeVampire from "./WannabeVampire"
import Bigfoot from "./Bigfoot"
import StonedGolem from "./StonedGolem"
import UnspeakablyAwfulIndescribableHorror from "./UnspeakablyAwfulIndescribableHorror"
import WightBrothers from "./WightBrothers"
import Hippogriff from "./Hippogriff"
import KingTut from "./KingTut"
import Bullrog from "./Bullrog"
import Squidzilla from "./Squidzilla"
import PlutoniumDragon from "./PlutoniumDragon"
import Leperchaun from "./Leperchaun"
import InsuranceSalesman from "./InsuranceSalesman"

const MONSTERS = [
    MaulRat,
    DroolingSlime,
    LameGoblin,
    Crabs,
    PottedPlant,
    FlyingFrogs,
    GelatinousOctahedron,
    Harpies,
    LargeAngryChicken,
    MrBones,
    PitBull,
    UndeadHorse,
    SnailsOnSpeed,
    Leperchaun,
    Lawyers,
    Pukachu,
    ShriekingGeek,
    Ghoulfiends,
    Platycore,
    Gazebo,
    Amazon,
    FaceSucker,
    NetTroll,
    Orcs,
    FloatingNose,
    TongueDemon,
    WannabeVampire,
    Bigfoot,
    StonedGolem,
    UnspeakablyAwfulIndescribableHorror,
    InsuranceSalesman,
    WightBrothers,
    Hippogriff,
    KingTut,
    Bullrog,
    Squidzilla,
    PlutoniumDragon
]

export default MONSTERS