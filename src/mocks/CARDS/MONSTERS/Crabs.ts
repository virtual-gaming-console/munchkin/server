import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"

const Crabs = new CardInstantiator(
    9,
    () => new MonsterCard(
        9,
        "Crabs",
        1,
        {
            experience: 1,
            treasures: 1
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            const playerBoots = player.arsenalCards.find(card => card.placement === GearPlacement.BOOTS)

            if (playerBoots) {
                player.currentGame.discard.add(playerBoots)
            }

            const playerArmor = player.arsenalCards.find(card => card.placement === GearPlacement.ARMOR)

            if (playerArmor) {
                player.currentGame.discard.add(playerArmor)
            }
        },
        [{
            modifier: MODIFIERS.NO_FLEE()
        }]
    )
)

export default Crabs