import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"
import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"

const FlyingFrogs = new CardInstantiator(
    34,
    () => new MonsterCard(
        34,
        "Flying Frogs",
        2,
        {
            experience: 1,
            treasures: 1
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            player.level -= 2
        },
        [{
            modifier: MODIFIERS.FLYING_FROGS(),
        }]
    )
)

export default FlyingFrogs