import CardInstantiator from "../../../models/Card/CardInstantiator"
import MonsterAction from "../../../models/Card/Monster/MonsterAction"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"

const FloatingNose = new CardInstantiator(
    33,
    () => new MonsterCard(
        33,
        "Floating Nose",
        10,
        {
            experience: 1,
            treasures: 3
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            player.level -= 3
        },
        [{
            action: MonsterAction.FLOATING_NOZE
        }]
    )
)

export default FloatingNose