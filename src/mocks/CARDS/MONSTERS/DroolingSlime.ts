import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"
import PlayerRaceType from "../../../models/Player/PlayerRace/PlayerRaceType"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"

const DroolingSlime = new CardInstantiator(
    28,
    () => new MonsterCard(
        28,
        "Drooling Slime",
        1,
        {
            experience: 1,
            treasures: 1
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            const playerBoots = player.arsenalCards.find(card => card.placement === GearPlacement.BOOTS)

            if (playerBoots) {
                player.currentGame.discard.add(playerBoots)
            } else {
                player.level--
            }
        },
        [{
            restriction: PlayerRaceType.ELVEN,
            modifier: MODIFIERS.DROOLING_SLIME()
        }]
    )
)

export default DroolingSlime