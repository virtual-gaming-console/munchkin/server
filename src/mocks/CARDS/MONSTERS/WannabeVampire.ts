import CardInstantiator from "../../../models/Card/CardInstantiator"
import MonsterAction from "../../../models/Card/Monster/MonsterAction"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"
import PlayerClassType from "../../../models/Player/PlayerClass/PlayerClassType"

const WannabeVampire = new CardInstantiator(
    73,
    () => new MonsterCard(
        73,
        "Wannabe Vampire",
        12,
        {
            experience: 1,
            treasures: 3
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            player.level -= 3
        },
        [{
            restriction: PlayerClassType.PRIEST,
            action: MonsterAction.WANNABE_VAMPIRE
        }]
    )
)

export default WannabeVampire