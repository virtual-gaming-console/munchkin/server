import CardInstantiator from "../../../models/Card/CardInstantiator"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"

const Pukachu = new CardInstantiator(
    61,
    () => new MonsterCard(
        61,
        "Pukachu",
        6,
        {
            experience: 1,
            treasures: 2
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            player.hand.cards.forEach(card => player.currentGame.discard.add(card))
        }
    )
)

export default Pukachu