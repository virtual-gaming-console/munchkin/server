import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"
import MonsterType from "../../../models/Card/Monster/MonsterType"
import PlayerRaceType from "../../../models/Player/PlayerRace/PlayerRaceType"
import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"

const UndeadHorse = new CardInstantiator(
    70,
    () => new MonsterCard(
        70,
        "Undead Horse",
        4,
        {
            experience: 1,
            treasures: 2
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            player.level -= 2
        },
        [{
            restriction: PlayerRaceType.DWARF,
            modifier: MODIFIERS.UNDEAD_HORSE()
        }],
        undefined,
        MonsterType.UNDEAD
    )
)

export default UndeadHorse