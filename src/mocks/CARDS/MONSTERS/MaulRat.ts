import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"
import PlayerClassType from "../../../models/Player/PlayerClass/PlayerClassType"

const MaulRat = new CardInstantiator(
    53,
    () => new MonsterCard(
        53,
        "Maul Rat",
        1,
        {
            experience: 1,
            treasures: 1
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            player.level--
        },
        [{
            restriction: PlayerClassType.PRIEST,
            modifier: MODIFIERS.MAUL_RAT()
        }]
    )
)

export default MaulRat