import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"
import MonsterType from "../../../models/Card/Monster/MonsterType"
import MonsterAction from "../../../models/Card/Monster/MonsterAction"
import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"

const MrBones = new CardInstantiator(
    54,
    () => new MonsterCard(
        54,
        "Mr. Bones",
        2,
        {
            experience: 1,
            treasures: 1
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            player.level -= 2
        },
        [{
            modifier: MODIFIERS.MR_BONES()
        }],
        MonsterAction.MR_BONES,
        MonsterType.UNDEAD
    )
)

export default MrBones