import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"
import MonsterAction from "../../../models/Card/Monster/MonsterAction"
import CardInstantiator from "../../../models/Card/CardInstantiator"

const PitBull = new CardInstantiator(
    57,
    () => new MonsterCard(
        57,
        "Pit Bull",
        2,
        {
            experience: 1,
            treasures: 1
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            player.level -= 2
        },
        undefined,
        MonsterAction.PITBULL
    )
)

export default PitBull