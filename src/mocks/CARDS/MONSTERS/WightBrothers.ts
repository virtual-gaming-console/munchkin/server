import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import MonsterType from "../../../models/Card/Monster/MonsterType"
import Player from "../../../models/Player/Player"

const WightBrothers = new CardInstantiator(
    75,
    () => new MonsterCard(
        75,
        "Wight Brothers",
        16,
        {
            experience: 2,
            treasures: 4
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                player.level -= 2
            } else {
                player.level = 1
            }
        },
        [{
            restriction: 3,
            modifier: MODIFIERS.AUTO_FLEE()
        }],
        undefined,
        MonsterType.UNDEAD
    )
)

export default WightBrothers