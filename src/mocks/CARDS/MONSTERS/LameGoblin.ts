import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"

const LameGoblin = new CardInstantiator(
    48,
    () => new MonsterCard(
        48,
        "Lame Goblin",
        1,
        {
            experience: 1,
            treasures: 1
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            player.level--
        },
        [{
            modifier: MODIFIERS.LAME_GOBLIN()
        }]
    )
)

export default LameGoblin