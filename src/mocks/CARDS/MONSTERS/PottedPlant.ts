import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"
import PlayerRaceType from "../../../models/Player/PlayerRace/PlayerRaceType"

const PottedPlant = new CardInstantiator(
    60,
    () => new MonsterCard(
        60,
        "Potted Plant",
        1,
        {
            experience: 1,
            treasures: 1
        },
        (player: Player, fleeSuccess: boolean) => {

        },
        [{
            restriction: PlayerRaceType.ELVEN,
            modifier: MODIFIERS.POTTED_PLANT()
        }]
    )
)

export default PottedPlant