import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"

const Gazebo = new CardInstantiator(
    35,
    () => new MonsterCard(
        35,
        "Gazebo",
        8,
        {
            experience: 1,
            treasures: 2
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            player.level -= 3
        },
        [{
            modifier: MODIFIERS.NO_HELP()
        }]
    )
)

export default Gazebo