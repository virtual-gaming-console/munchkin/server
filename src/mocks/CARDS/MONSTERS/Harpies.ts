import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import PlayerClassType from "../../../models/Player/PlayerClass/PlayerClassType"
import Player from "../../../models/Player/Player"
import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"

const Harpies = new CardInstantiator(
    40,
    () => new MonsterCard(
        40,
        "Harpies",
        4,
        {
            experience: 1,
            treasures: 2
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            player.level -= 2
        },
        [{
            restriction: PlayerClassType.MAGE,
            modifier: MODIFIERS.HARPIES()
        }]
    )
)

export default Harpies