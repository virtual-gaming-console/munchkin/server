import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"
import Choice from "../../../models/Card/Choice"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"

const SnailsOnSpeed = new CardInstantiator(
    63,
    () => new MonsterCard(
        63,
        "Snails on Speed",
        4,
        {
            experience: 1,
            treasures: 2
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            if (player.hand.cards.length === 0) {
                return
            }

            player.roll((dice: number) => {
                if (dice >= player.hand.cards.length) {
                    player.hand.cards.forEach(card => {
                        player.currentGame.discard.add(card)
                    })
                } else {
                    for (let index = 0; index < dice; index++) {
                        player.currentGame.choiceCallbacks.push({
                            sendChoices: () => {
                                const choices: Choice[] = player.hand.cards.map(card => ({ name: card.name, cardId: card.id, isVisible: true }))

                                player.websocket?.send(
                                    {
                                        title: 'show-choices',
                                        data: {
                                            choices,
                                            name: 'DEFAUSSE UNE CARTE (' + (index + 1) + '/' + dice + ')'
                                        }
                                    }
                                )
                            },
                            choiceCallback: (choices: Choice[]) => {
                                let choice = choices[0]
                                if (!choice) {
                                    const choices: Choice[] = player.hand.cards.map(card => ({ name: card.name, cardId: card.id }))
                                    choice = choices[Math.floor(Math.random() * choices.length)]
                                }

                                let card = player.hand.cards.find(cardItem => cardItem.id === choice.cardId)

                                if (card) {
                                    player.currentGame.discard.add(card)
                                }

                                return true
                            }
                        })
                    }
                }
            })
        },
        [{
            modifier: MODIFIERS.SNAILS_ON_SPEED()
        }]
    )
)

export default SnailsOnSpeed