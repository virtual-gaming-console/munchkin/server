import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"

const PlutoniumDragon = new CardInstantiator(
    59,
    () => new MonsterCard(
        59,
        "Plutonium Dragon",
        18,
        {
            experience: 2,
            treasures: 5
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            player.die()
        },
        [
            {
                restriction: 5,
                modifier: MODIFIERS.AUTO_FLEE()
            }
        ]
    )
)

export default PlutoniumDragon