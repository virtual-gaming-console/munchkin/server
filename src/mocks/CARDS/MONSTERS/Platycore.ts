import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"
import Choice from "../../../models/Card/Choice"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"
import PlayerClassType from "../../../models/Player/PlayerClass/PlayerClassType"

enum choiceNames {
    DISCARD_ALL_HAND = "DEFAUSSE TOUTE TA MAIN",
    LOSE_TWO_LEVELS = "PERDS DEUX NIVEAUX"
}

const Platycore = new CardInstantiator(
    58,
    () => new MonsterCard(
        58,
        "Platycore",
        6,
        {
            experience: 1,
            treasures: 2
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            player.currentGame.choiceCallbacks.push({
                sendChoices: () => {
                    player.websocket?.send(
                        {
                            title: 'show-choices',
                            data: {
                                choices: [{ name: choiceNames.DISCARD_ALL_HAND }, { name: choiceNames.LOSE_TWO_LEVELS }],
                                name: 'CHOISI LA PLUS BELLE MORT !'
                            }
                        }
                    )
                },
                choiceCallback: (choices: Choice[]) => {
                    let choice = choices[0]
                    if (!choice) {
                        if (player.level <= 2 && player.hand.cards.length > 2) {
                            choice = { name: choiceNames.LOSE_TWO_LEVELS }
                        } else {
                            choice = { name: choiceNames.DISCARD_ALL_HAND }
                        }
                    }

                    switch (choice.name) {
                        case choiceNames.LOSE_TWO_LEVELS:
                            player.level -= 2
                            break
                        case choiceNames.DISCARD_ALL_HAND:
                        default:
                            player.hand.cards.forEach(card => player.currentGame.discard.add(card))
                            break
                    }

                    return true
                }
            })
        },
        [{
            restriction: PlayerClassType.MAGE,
            modifier: MODIFIERS.PLATYCORE()
        }]
    )
)

export default Platycore