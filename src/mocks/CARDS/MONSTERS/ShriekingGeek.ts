import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"
import PlayerClassType from "../../../models/Player/PlayerClass/PlayerClassType"
import Human from "../../../models/Player/PlayerRace/Human"

const ShriekingGeek = new CardInstantiator(
    62,
    () => new MonsterCard(
        62,
        "Shrieking Geek",
        6,
        {
            experience: 1,
            treasures: 2
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            player.class = undefined
            player.race = new Human()
        },
        [{
            restriction: PlayerClassType.WAR,
            modifier: MODIFIERS.SHRIEKING_GEEK()
        }]
    )
)

export default ShriekingGeek