import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"
import Choice from "../../../models/Card/Choice"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"
import PlayerRaceType from "../../../models/Player/PlayerRace/PlayerRaceType"

const Leperchaun = new CardInstantiator(
    51,
    () => new MonsterCard(
        51,
        "Leperchaun",
        4,
        {
            experience: 1,
            treasures: 2
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            if (!player.arsenalCards.length) {
                return
            }

            const players = [
                player.currentGame.nextPlayer(player),
                player.currentGame.previousPlayer(player)
            ]

            for (let index = 0; index < (player.arsenalCards.length >= players.length ? players.length : player.arsenalCards.length); index++) {
                const playerItem = players[index]
                player.currentGame.choiceCallbacks.push({
                    sendChoices: () => {
                        const choices = player.arsenalCards.map(card => ({ name: card.name, cardId: card.id }))

                        playerItem.websocket?.send(
                            {
                                title: 'show-choices',
                                data: {
                                    choices,
                                    name: 'PILLE UN OBJET DE ' + player.deviantUser?.nickname.toUpperCase()
                                }
                            }
                        )
                    },
                    choiceCallback: (choices: Choice[]) => {
                        let choice = choices[0]
                        if (!choice) {
                            const choices = player.arsenalCards.map(card => ({ name: card.name, cardId: card.id }))
                            choice = choices[Math.floor(Math.random() * choices.length)]
                        }

                        let card = player.arsenalCards.find(cardItem => cardItem.id === choice.cardId)
                        if (card) {
                            player.giveCard(card, playerItem)
                        }

                        return true
                    }
                })
            }
        },
        [{
            restriction: PlayerRaceType.ELVEN,
            modifier: MODIFIERS.LEPERCHAUN()
        }]
    )
)

export default Leperchaun