import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"
import PlayerClassType from "../../../models/Player/PlayerClass/PlayerClassType"

const UnspeakablyAwfulIndescribableHorror = new CardInstantiator(
    71,
    () => new MonsterCard(
        71,
        "Unspeakably Awful Indescribable Horror",
        14,
        {
            experience: 1,
            treasures: 4
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            if (player.classes.find(_class => _class === PlayerClassType.MAGE)) {
                if (player.class?.type === PlayerClassType.MAGE) {
                    player.class = undefined
                } else {
                    player.superMunchkinClass = undefined
                }
            } else {
                player.die()
            }
        },
        [{
            restriction: PlayerClassType.WAR,
            modifier: MODIFIERS.UAIH()
        }]
    )
)

export default UnspeakablyAwfulIndescribableHorror