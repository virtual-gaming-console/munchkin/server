import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"
import Choice from "../../../models/Card/Choice"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import TreasureCard from "../../../models/Card/TreasureCard"
import Player from "../../../models/Player/Player"

const InsuranceSalesman = new CardInstantiator(
    45,
    () => new MonsterCard(
        45,
        "Insurance Salesman",
        14,
        {
            experience: 1,
            treasures: 4
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            const allCards = [
                ...player.arsenalCards.filter(card => card.sellingPrice),
                ...player.hand.cards.filter(card => (card as TreasureCard).sellingPrice)
            ]
            if (allCards.map(cardItem => (cardItem as TreasureCard).sellingPrice).reduce((a, b) => a + b, 0) <= 1000) {
                allCards.forEach(card => {
                    player.currentGame.discard.add(card)
                })
            } else {
                player.currentGame.choiceCallbacks.push({
                    sendChoices: () => {
                        const choices: Choice[] = allCards.map(card => ({ name: card.name, cardId: card.id, isVisible: true }))
                        //     ...player.arsenalCards.map(card => ({ name: card.name, cardId: card.id, isVisible: true })),
                        //     ...player.hand.cards.filter(card => (card as TreasureCard).sellingPrice).map(card => ({ name: card.name, cardId: card.id, isVisible: true }))
                        // ]

                        player.websocket?.send(
                            {
                                title: 'show-choices',
                                data: {
                                    choices,
                                    min: 1000,
                                    name: 'DEFAUSSE DES OBJETS POUR 1000 GOLDS'
                                }
                            }
                        )
                    },
                    choiceCallback: (choices: Choice[]) => {
                        let cards = choices.map(choice => allCards.find(card => card.id === choice.cardId))
                        if (!cards.length || cards.map(cardItem => (cardItem as TreasureCard).sellingPrice).reduce((a, b) => a + b, 0) < 1000) {
                            while (cards.map(cardItem => (cardItem as TreasureCard).sellingPrice).reduce((a, b) => a + b, 0) < 1000) {
                                const randomCard = allCards.splice(Math.floor(Math.random() * allCards.length), 1)[0]
                                cards.push(randomCard)
                            }
                        }

                        cards.forEach(card => {
                            if (card) {
                                player.currentGame.discard.add(card)
                            }
                        })

                        return false
                    }
                })
            }
        },
        [{
            modifier: MODIFIERS.NO_LEVEL()
        }]
    )
)

export default InsuranceSalesman