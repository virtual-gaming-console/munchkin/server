import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"
import PlayerRaceType from "../../../models/Player/PlayerRace/PlayerRaceType"

const Squidzilla = new CardInstantiator(
    64,
    () => new MonsterCard(
        64,
        "Squidzilla",
        18,
        {
            experience: 2,
            treasures: 4
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            player.die()
        },
        [
            {
                restriction: PlayerRaceType.ELVEN,
                modifier: MODIFIERS.SQUIDILLA()
            },
            {
                restriction: 4,
                modifier: MODIFIERS.AUTO_FLEE()
            }
        ]
    )
)

export default Squidzilla