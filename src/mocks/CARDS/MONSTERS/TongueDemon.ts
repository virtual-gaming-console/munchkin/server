import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"
import Choice from "../../../models/Card/Choice"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"
import PlayerClassType from "../../../models/Player/PlayerClass/PlayerClassType"
import PlayerRaceType from "../../../models/Player/PlayerRace/PlayerRaceType"

const TongueDemon = new CardInstantiator(
    68,
    () => {
        let card = new MonsterCard(
            68,
            "Tongue Demon",
            12,
            {
                experience: 1,
                treasures: 3
            },
            (player: Player, fleeSuccess: boolean) => {
                if (fleeSuccess) {
                    return
                }

                if (player.races.find(race => race === PlayerRaceType.ELVEN)) {
                    player.level = player.level <= 3 ? 1 : player.level - 3
                } else {
                    player.level = player.level <= 2 ? 1 : player.level - 2
                }
            },
            [{
                restriction: PlayerClassType.PRIEST,
                modifier: MODIFIERS.TONGUE_DEMON()
            }]
        )

        card.encounter = (players: Array<Player>) => {
            for (let index = 0; index < players.length; index++) {
                const playerItem = players[index]
                const choiceCount = playerItem.hand.cards.filter(card => card.isTreasure).length + playerItem.arsenalCards.length
                if (choiceCount === 0) {
                    continue
                }

                playerItem.currentGame.choiceCallbacks.push({
                    sendChoices: () => {
                        const choices = [
                            ...playerItem.arsenalCards,
                            ...playerItem.hand.cards.filter(card => card.isTreasure),
                        ].map(card => ({ name: card.name, cardId: card.id, isVisible: true }))

                        playerItem.websocket?.send(
                            {
                                title: 'show-choices',
                                data: {
                                    choices,
                                    name: 'CHOISI UN OBJET A DEFAUSSER ' + playerItem.deviantUser?.nickname.toUpperCase()
                                }
                            }
                        )
                    },
                    choiceCallback: (choices: Choice[]) => {
                        let choice = choices[0]
                        if (!choice) {
                            const choices = [
                                ...playerItem.arsenalCards,
                                ...playerItem.hand.cards.filter(card => card.isTreasure),
                            ].map(card => ({ name: card.name, cardId: card.id }))
                            choice = choices[Math.floor(Math.random() * choices.length)]
                        }

                        let card = playerItem.hand.cards.find(cardItem => cardItem.id === choice.cardId)
                        if (!card) {
                            card = playerItem.arsenalCards.find(cardItem => cardItem.id === choice.cardId)
                        }

                        if (card) {
                            playerItem.currentGame.discard.add(card)
                            return false
                        }
                    }
                })
            }
        }

        return card
    }
)

export default TongueDemon