import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"

const Bullrog = new CardInstantiator(
    6,
    () => new MonsterCard(
        6,
        "Bullrog",
        18,
        {
            experience: 2,
            treasures: 5
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            player.die()
        },
        [{
            restriction: 4,
            modifier: MODIFIERS.AUTO_FLEE()
        }]
    )
)

export default Bullrog