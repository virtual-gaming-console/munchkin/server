import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"

const Ghoulfiends = new CardInstantiator(
    37,
    () => new MonsterCard(
        37,
        "Ghoulfiends",
        8,
        {
            experience: 1,
            treasures: 2
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            player.level = Math.min(...player.currentGame.players.map(player => player.level))
        },
        [{
            modifier: MODIFIERS.NO_BONUS()
        }]
    )
)

export default Ghoulfiends