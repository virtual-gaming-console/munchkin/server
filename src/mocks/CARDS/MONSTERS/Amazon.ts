import CardInstantiator from "../../../models/Card/CardInstantiator"
import MonsterAction from "../../../models/Card/Monster/MonsterAction"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Gender from "../../../models/Player/Gender"
import Player from "../../../models/Player/Player"

const Amazon = new CardInstantiator(
    2,
    () => new MonsterCard(
        2,
        "Amazon",
        8,
        {
            experience: 1,
            treasures: 2
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            if (player.class) {
                player.class = undefined
            } else if (player.superMunchkinClass) {
                player.superMunchkinClass = undefined
            } else {
                player.level -= 3
            }
        },
        [{
            restriction: Gender.FEMALE,
            action: MonsterAction.AMAZON
        }]
    )
)

export default Amazon