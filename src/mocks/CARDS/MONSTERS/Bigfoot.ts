import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"
import PlayerRaceType from "../../../models/Player/PlayerRace/PlayerRaceType"

const Bigfoot = new CardInstantiator(
    5,
    () => new MonsterCard(
        5,
        "Bigfoot",
        12,
        {
            experience: 1,
            treasures: 3
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            const playerHead = player.arsenalCards.find(card => card.placement === GearPlacement.HEAD)

            if (playerHead) {
                player.currentGame.discard.add(playerHead)
            }
        },
        [{
            restriction: [PlayerRaceType.DWARF, PlayerRaceType.HOBBIT],
            modifier: MODIFIERS.BIG_FOOT()
        }]
    )
)

export default Bigfoot