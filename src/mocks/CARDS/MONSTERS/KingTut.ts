import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import MonsterType from "../../../models/Card/Monster/MonsterType"
import Player from "../../../models/Player/Player"

const KingTut = new CardInstantiator(
    47,
    () => new MonsterCard(
        47,
        "King Tut",
        16,
        {
            experience: 2,
            treasures: 4
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                player.level -= 2
            } else {
                player.hand.cards.forEach(card => {
                    player.currentGame.discard.add(card)
                })

                player.arsenalCards.forEach(card => {
                    player.currentGame.discard.add(card)
                })
            }
        },
        [{
            restriction: 3,
            modifier: MODIFIERS.AUTO_FLEE()
        }],
        undefined,
        MonsterType.UNDEAD
    )
)

export default KingTut