import CardInstantiator from "../../../models/Card/CardInstantiator"
import Choice from "../../../models/Card/Choice"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"

const NetTroll = new CardInstantiator(
    55,
    () => new MonsterCard(
        55,
        "Net Troll",
        10,
        {
            experience: 1,
            treasures: 3
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            const players = player.currentGame.highestLevelPlayers().filter(playerItem => playerItem.deviantUser?.id !== player.deviantUser?.id)
            for (let index = 0; index < (player.arsenalCards.length >= players.length ? players.length : player.arsenalCards.length); index++) {
                const playerItem = players[index]
                player.currentGame.choiceCallbacks.push({
                    sendChoices: () => {
                        const choices = player.arsenalCards.map(card => ({ name: card.name, cardId: card.id }))

                        playerItem.websocket?.send(
                            {
                                title: 'show-choices',
                                data: {
                                    choices,
                                    name: 'PILLE UN OBJET DE ' + player.deviantUser?.nickname.toUpperCase()
                                }
                            }
                        )
                    },
                    choiceCallback: (choices: Choice[]) => {
                        let choice = choices[0]
                        if (!choice) {
                            const choices = player.arsenalCards.map(card => ({ name: card.name, cardId: card.id }))
                            choice = choices[Math.floor(Math.random() * choices.length)]
                        }

                        let card = player.arsenalCards.find(cardItem => cardItem.id === choice.cardId)
                        if (card) {
                            player.giveCard(card, playerItem)
                        }

                        return true
                    }
                })
            }
        }
    )
)

export default NetTroll