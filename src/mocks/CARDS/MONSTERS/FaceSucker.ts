import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"
import PlayerRaceType from "../../../models/Player/PlayerRace/PlayerRaceType"

const FaceSucker = new CardInstantiator(
    32,
    () => new MonsterCard(
        32,
        "Face Sucker",
        8,
        {
            experience: 1,
            treasures: 2
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            const playerHead = player.arsenalCards.find(card => card.placement === GearPlacement.HEAD)

            if (playerHead) {
                player.currentGame.discard.add(playerHead)
            }

            player.level--
        },
        [{
            restriction: PlayerRaceType.ELVEN,
            modifier: MODIFIERS.FACE_SUCKER()
        }]
    )
)

export default FaceSucker