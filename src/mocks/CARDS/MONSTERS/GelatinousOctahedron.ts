import MODIFIERS from "../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import MonsterCard from "../../../models/Card/Monster/MonsterCard"
import Player from "../../../models/Player/Player"

const GelatinousOctahedron = new CardInstantiator(
    36,
    () => new MonsterCard(
        36,
        "Gelatinous Octahedron",
        2,
        {
            experience: 1,
            treasures: 1
        },
        (player: Player, fleeSuccess: boolean) => {
            if (fleeSuccess) {
                return
            }

            const playerArmor = player.arsenalCards.find(card => card.placement === GearPlacement.ARMOR)

            if (playerArmor) {
                player.currentGame.discard.add(playerArmor)
            }
        },
        [{
            modifier: MODIFIERS.GELATINOUS_OCTAHEDRON()
        }]
    )
)

export default GelatinousOctahedron