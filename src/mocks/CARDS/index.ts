import GEARS from './GEARS'
import MONSTERS from './MONSTERS'
import TREASURE_CONSUMABLES from './CONSUMABLES/TREASURES'
import DUNGEON_CONSUMABLES from './CONSUMABLES/DUNGEONS'
import CURSES from './CURSES'

const TREASURES = [
    ...GEARS,
    ...TREASURE_CONSUMABLES,
]

let DUNGEONS = [
    ...MONSTERS,
    ...DUNGEON_CONSUMABLES,
    ...CURSES,
]

// TEMPORARY
// const SecondCardId = 1
// const SecondCard = DUNGEONS.find(card => card.id === SecondCardId)
// DUNGEONS = [
//     ...DUNGEONS.filter(cards => cards.id !== SecondCardId)
// ]
// if (SecondCard) {
//     DUNGEONS.splice(8, 0, SecondCard)
// }

// const CardId = 15
// const Card = DUNGEONS.find(instantiator => instantiator.cardId === CardId)
// DUNGEONS = [
//     ...DUNGEONS.filter(instantiator => instantiator.cardId !== CardId)
// ]
// if (Card) {
//     DUNGEONS.splice(4, 0, Card)
// }


export default {
    TREASURES,
    DUNGEONS,
}