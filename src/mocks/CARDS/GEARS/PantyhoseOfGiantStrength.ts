import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import PlayerClassType from "../../../models/Player/PlayerClass/PlayerClassType"
import CardInstantiator from "../../../models/Card/CardInstantiator"

const PantyhoseOfGiantStrength = new CardInstantiator(
    121,
    () => new GearCard(
        121,
        "Collants de force de géant",
        3,
        GearPlacement.NONE,
        600,
        undefined,
        PlayerClassType.WAR,
    )
)

export default PantyhoseOfGiantStrength