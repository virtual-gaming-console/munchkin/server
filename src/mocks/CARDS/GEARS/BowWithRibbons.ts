import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import PlayerRaceType from "../../../models/Player/PlayerRace/PlayerRaceType"

const BowWithRibbons = new CardInstantiator(
    82,
    () => new GearCard(
        82,
        "Arc enrubannés",
        4,
        GearPlacement.TWO_HAND,
        900,
        PlayerRaceType.ELVEN
    )
)

export default BowWithRibbons