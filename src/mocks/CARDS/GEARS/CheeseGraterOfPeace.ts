import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import PlayerClassType from "../../../models/Player/PlayerClass/PlayerClassType"

const CheeseGraterOfPeace = new CardInstantiator(
    87,
    () => new GearCard(
        87,
        "Rape a fromage de la paix",
        3,
        GearPlacement.ONE_HAND,
        400,
        PlayerClassType.PRIEST
    )
)

export default CheeseGraterOfPeace