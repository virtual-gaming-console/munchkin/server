import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"

const SneakyBastardSword = new CardInstantiator(
    137,
    () => new GearCard(
        137,
        "Epee (de) batard(e)",
        2,
        GearPlacement.ONE_HAND,
        400,
    )
)

export default SneakyBastardSword