import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import PassiveSkill from "../../../models/Player/Skill/PassiveSkill"

const BootsOfRunningReallyFast = new CardInstantiator(
    81,
    () => new GearCard(
        81,
        "Bottes de célérité",
        0,
        GearPlacement.BOOTS,
        400,
        undefined,
        undefined,
        [PassiveSkill.BOOTS_OF_RUNNING_REALLY_FAST]
    )
)

export default BootsOfRunningReallyFast