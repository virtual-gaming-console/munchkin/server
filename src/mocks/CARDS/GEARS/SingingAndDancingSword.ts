import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import PlayerClassType from "../../../models/Player/PlayerClass/PlayerClassType"
import CardInstantiator from "../../../models/Card/CardInstantiator"

const SingingAndDancingSword = new CardInstantiator(
    134,
    () => new GearCard(
        134,
        "Epée karaoké",
        2,
        GearPlacement.NONE,
        400,
        undefined,
        PlayerClassType.THIEF,
    )
)

export default SingingAndDancingSword