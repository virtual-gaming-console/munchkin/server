import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import PassiveSkill from "../../../models/Player/Skill/PassiveSkill"

const SandalsOfProtection = new CardInstantiator(
    131,
    () => new GearCard(
        131,
        "Sandales de protection",
        0,
        GearPlacement.BOOTS,
        700,
        undefined,
        undefined,
        [PassiveSkill.NO_CURSE_ON_DUNGEON]
    )
)

export default SandalsOfProtection