import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import PlayerClassType from "../../../models/Player/PlayerClass/PlayerClassType"

const MaceOfSharpness = new CardInstantiator(
    115,
    () => new GearCard(
        115,
        "Massue a pointes",
        4,
        GearPlacement.ONE_HAND,
        600,
        PlayerClassType.PRIEST
    )
)

export default MaceOfSharpness