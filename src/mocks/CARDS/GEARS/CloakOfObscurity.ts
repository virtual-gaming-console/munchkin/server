import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import PlayerClassType from "../../../models/Player/PlayerClass/PlayerClassType"

const CloakOfObscurity = new CardInstantiator(
    88,
    () => new GearCard(
        88,
        "Cape d'ombre",
        4,
        GearPlacement.NONE,
        600,
        PlayerClassType.THIEF
    )
)

export default CloakOfObscurity