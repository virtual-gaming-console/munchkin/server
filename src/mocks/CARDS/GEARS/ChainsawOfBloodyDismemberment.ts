import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"

const ChainsawOfBloodyDismemberment = new CardInstantiator(
    86,
    () => new GearCard(
        86,
        "Tronçonneuse de la mort",
        3,
        GearPlacement.TWO_HAND,
        600,
    )
)

export default ChainsawOfBloodyDismemberment