import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"

const SlimyArmor = new CardInstantiator(
    136,
    () => new GearCard(
        136,
        "Armure gluante",
        1,
        GearPlacement.ARMOR,
        200,
    )
)

export default SlimyArmor