import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import PlayerRaceType from "../../../models/Player/PlayerRace/PlayerRaceType"

const HammerOfKneecapping = new CardInstantiator(
    101,
    () => new GearCard(
        101,
        "Marteau de destruction des genoux",
        4,
        GearPlacement.ONE_HAND,
        600,
        PlayerRaceType.DWARF
    )
)

export default HammerOfKneecapping