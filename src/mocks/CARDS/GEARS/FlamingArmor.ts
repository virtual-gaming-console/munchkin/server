import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import PassiveSkill from "../../../models/Player/Skill/PassiveSkill"

const FlamingArmor = new CardInstantiator(
    95,
    () => new GearCard(
        95,
        "Armure de flamme",
        2,
        GearPlacement.ARMOR,
        400,
        undefined,
        undefined,
        [PassiveSkill.FIRE_DAMAGE]
    )
)

export default FlamingArmor