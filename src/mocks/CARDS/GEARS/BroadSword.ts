import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import Gender from "../../../models/Player/Gender"

const BroadSword = new CardInstantiator(
    84,
    () => new GearCard(
        84,
        "Epee féministe",
        3,
        GearPlacement.ONE_HAND,
        400,
        Gender.FEMALE
    )
)

export default BroadSword