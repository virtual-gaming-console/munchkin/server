import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"

const HugeRock = new CardInstantiator(
    106,
    () => new GearCard(
        106,
        "Enorme rocher",
        3,
        GearPlacement.TWO_HAND,
        0,
    )
)

export default HugeRock