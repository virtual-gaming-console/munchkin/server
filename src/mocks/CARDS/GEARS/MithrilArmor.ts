import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import PlayerClassType from "../../../models/Player/PlayerClass/PlayerClassType"
import CardInstantiator from "../../../models/Card/CardInstantiator"

const MithrilArmor = new CardInstantiator(
    118,
    () => new GearCard(
        118,
        "Armure de mithril",
        3,
        GearPlacement.ARMOR,
        600,
        undefined,
        PlayerClassType.MAGE,
    )
)

export default MithrilArmor