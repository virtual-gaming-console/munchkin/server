import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import PlayerClassType from "../../../models/Player/PlayerClass/PlayerClassType"

const PointyHatOfPower = new CardInstantiator(
    122,
    () => new GearCard(
        122,
        "Chapeau pointue de puissance",
        3,
        GearPlacement.HEAD,
        400,
        PlayerClassType.MAGE,
    )
)

export default PointyHatOfPower