import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import PlayerRaceType from "../../../models/Player/PlayerRace/PlayerRaceType"

const ShortWideArmor = new CardInstantiator(
    133,
    () => new GearCard(
        133,
        "Armure trappue",
        3,
        GearPlacement.ARMOR,
        400,
        PlayerRaceType.DWARF
    )
)

export default ShortWideArmor