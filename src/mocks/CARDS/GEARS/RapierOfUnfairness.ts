import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import PlayerRaceType from "../../../models/Player/PlayerRace/PlayerRaceType"

const RapierOfUnfairness = new CardInstantiator(
    128,
    () => new GearCard(
        128,
        "Rapiere d'injustice",
        3,
        GearPlacement.ONE_HAND,
        600,
        PlayerRaceType.ELVEN
    )
)

export default RapierOfUnfairness