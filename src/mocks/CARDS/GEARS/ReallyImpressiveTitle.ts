import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"

const ReallyImpressiveTitle = new CardInstantiator(
    130,
    () => new GearCard(
        130,
        "Titre qui en jette vraiment grave",
        3,
        GearPlacement.NONE,
        0,
    )
)

export default ReallyImpressiveTitle