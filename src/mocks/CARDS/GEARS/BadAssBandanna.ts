import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import PlayerRaceType from "../../../models/Player/PlayerRace/PlayerRaceType"

const BadAssBandanna = new CardInstantiator(
    78,
    () => new GearCard(
        78,
        "Bandana de gros dur",
        3,
        GearPlacement.HEAD,
        400,
        PlayerRaceType.HUMAN,
    )
)

export default BadAssBandanna