import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import PassiveSkill from "../../../models/Player/Skill/PassiveSkill"

const TubaOfCharm = new CardInstantiator(
    144,
    () => new GearCard(
        144,
        "Trompette de charme",
        0,
        GearPlacement.ONE_HAND,
        300,
        undefined,
        undefined,
        [
            PassiveSkill.BOOTS_OF_RUNNING_REALLY_FAST,
            PassiveSkill.STEAL_ONE_TREASURE_ON_ESCAPE
        ]
    )
)

export default TubaOfCharm