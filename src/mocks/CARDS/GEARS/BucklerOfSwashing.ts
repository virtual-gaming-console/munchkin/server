import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"

const BucklerOfSwashing = new CardInstantiator(
    85,
    () => new GearCard(
        85,
        "Targe d'inconscience suicidaire",
        2,
        GearPlacement.ONE_HAND,
        400,
    )
)

export default BucklerOfSwashing