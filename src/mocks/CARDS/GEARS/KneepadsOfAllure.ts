import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import PlayerClassType from "../../../models/Player/PlayerClass/PlayerClassType"
import Seduce from "../../../models/Player/Skill/Gear/Seduce"

const KneepadsOfAllure = new CardInstantiator(
    111,
    () => new GearCard(
        111,
        "Talisman de séduction",
        0,
        GearPlacement.NONE,
        600,
        undefined,
        PlayerClassType.PRIEST,
        [new Seduce()],
    )
)

export default KneepadsOfAllure