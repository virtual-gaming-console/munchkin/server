import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import MillToFlee from "../../../models/Player/Skill/Gear/MillToFlee"

const Hireling = new CardInstantiator(
    103,
    () => new GearCard(
        103,
        "Fidèle serviteur",
        1,
        GearPlacement.NONE,
        0,
        undefined,
        undefined,
        [new MillToFlee()]
    )
)

export default Hireling