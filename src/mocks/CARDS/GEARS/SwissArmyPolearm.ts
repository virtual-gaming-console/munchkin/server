import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import PlayerRaceType from "../../../models/Player/PlayerRace/PlayerRaceType"

const SwissArmyPolearm = new CardInstantiator(
    142,
    () => new GearCard(
        142,
        "Hallebarde suisse",
        4,
        GearPlacement.TWO_HAND,
        600,
        PlayerRaceType.HUMAN
    )
)

export default SwissArmyPolearm