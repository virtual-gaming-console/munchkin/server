import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"

const LeatherArmor = new CardInstantiator(
    112,
    () => new GearCard(
        112,
        "Armure de cuir",
        1,
        GearPlacement.ARMOR,
        200,
    )
)

export default LeatherArmor