import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import PlayerClassType from "../../../models/Player/PlayerClass/PlayerClassType"

const DaggerOfTreachery = new CardInstantiator(
    91,
    () => new GearCard(
        91,
        "Dague de traitrise",
        3,
        GearPlacement.ONE_HAND,
        400,
        PlayerClassType.THIEF
    )
)

export default DaggerOfTreachery