import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import PlayerRaceType from "../../../models/Player/PlayerRace/PlayerRaceType"

const HornyHelmet = new CardInstantiator(
    105,
    () => new GearCard(
        105,
        "Casque a cornes",
        1,
        GearPlacement.HEAD,
        600,
        undefined,
        undefined,
        undefined,
        {
            type: PlayerRaceType.ELVEN,
            bonus: 3
        }
    )
)

export default HornyHelmet