import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"

const SpikyKnees = new CardInstantiator(
    138,
    () => new GearCard(
        138,
        "Genouillères perforantes",
        1,
        GearPlacement.NONE,
        200
    )
)

export default SpikyKnees