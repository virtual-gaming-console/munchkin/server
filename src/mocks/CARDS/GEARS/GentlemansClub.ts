import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import Gender from "../../../models/Player/Gender"

const GentlemansClub = new CardInstantiator(
    100,
    () => new GearCard(
        100,
        "Masse de gentlman",
        3,
        GearPlacement.TWO_HAND,
        400,
        Gender.MALE
    )
)

export default GentlemansClub