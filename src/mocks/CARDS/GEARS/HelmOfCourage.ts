import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"

const HelmOfCourage = new CardInstantiator(
    102,
    () => new GearCard(
        102,
        "Casque de courage",
        1,
        GearPlacement.HEAD,
        200,
    )
)

export default HelmOfCourage