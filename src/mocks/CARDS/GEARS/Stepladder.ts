import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import PlayerRaceType from "../../../models/Player/PlayerRace/PlayerRaceType"

const Stepladder = new CardInstantiator(
    141,
    () => new GearCard(
        141,
        "Tabouret",
        3,
        GearPlacement.NONE,
        400,
        PlayerRaceType.HOBBIT,
    )
)

export default Stepladder