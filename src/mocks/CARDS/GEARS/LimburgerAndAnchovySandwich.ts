import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import PlayerRaceType from "../../../models/Player/PlayerRace/PlayerRaceType"

const LimburgerAndAnchovySandwich = new CardInstantiator(
    113,
    () => new GearCard(
        113,
        "Sandwich nutella-anchois",
        3,
        GearPlacement.NONE,
        100,
        PlayerRaceType.HOBBIT,
    )
)

export default LimburgerAndAnchovySandwich