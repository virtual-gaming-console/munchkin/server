import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import RatOnAStickSkill from "../../../models/Player/Skill/Gear/RatOnAStickSkill"

const RatOnAStick = new CardInstantiator(
    129,
    () => new GearCard(
        129,
        "Pic à rat",
        1,
        GearPlacement.ONE_HAND,
        0,
        undefined,
        undefined,
        [new RatOnAStickSkill()]
    )
)

export default RatOnAStick