import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import PlayerClassType from "../../../models/Player/PlayerClass/PlayerClassType"

const StaffOfNapalm = new CardInstantiator(
    139,
    () => new GearCard(
        139,
        "Baton de napalm",
        5,
        GearPlacement.ONE_HAND,
        900,
        PlayerClassType.MAGE
    )
)

export default StaffOfNapalm