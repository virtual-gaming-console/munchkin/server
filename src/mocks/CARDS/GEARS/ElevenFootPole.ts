import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"

const ElevenFootPole = new CardInstantiator(
    94,
    () => new GearCard(
        94,
        "Lance de 3m45",
        1,
        GearPlacement.TWO_HAND,
        200
    )
)

export default ElevenFootPole