import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"

const BootsOfButtKicking = new CardInstantiator(
    80,
    () => new GearCard(
        80,
        "Bottes de confrontation d'hemorroids",
        2,
        GearPlacement.BOOTS,
        400,
    )
)

export default BootsOfButtKicking