import SpikyKnees from './SpikyKnees'
import ElevenFootPole from './ElevenFootPole'
import RatOnAStick from './RatOnAStick'
import HelmOfCourage from './HelmOfCourage'
import Hireling from './Hireling'
import HornyHelmet from './HornyHelmet'
import SlimyArmor from './SlimyArmor'
import LeatherArmor from './LeatherArmor'
import BucklerOfSwashing from './BucklerOfSwashing'
import SingingAndDancingSword from './SingingAndDancingSword'
import BootsOfButtKicking from './BootsOfButtKicking'
import FlamingArmor from './FlamingArmor'
import LimburgerAndAnchovySandwich from './LimburgerAndAnchovySandwich'
import Stepladder from './Stepladder'
import ReallyImpressiveTitle from './ReallyImpressiveTitle'
import PantyhoseOfGiantStrength from './PantyhoseOfGiantStrength'
import PointyHatOfPower from './PointyHatOfPower'
import BadAssBandanna from './BadAssBandanna'
import MithrilArmor from './MithrilArmor'
import ShortWideArmor from './ShortWideArmor'
import ChainsawOfBloodyDismemberment from './ChainsawOfBloodyDismemberment'
import HugeRock from './HugeRock'
import BroadSword from './BroadSword'
import GentlemansClub from './GentlemansClub'
import DaggerOfTreachery from './DaggerOfTreachery'
import RapierOfUnfairness from './RapierOfUnfairness'
import CheeseGraterOfPeace from './CheeseGraterOfPeace'
import CloakOfObscurity from './CloakOfObscurity'
import SwissArmyPolearm from './SwissArmyPolearm'
import BowWithRibbons from './BowWithRibbons'
import MaceOfSharpness from './MaceOfSharpness'
import HammerOfKneecapping from './HammerOfKneecapping'
import ShieldOfUbiquity from './ShieldOfUbiquity'
import StaffOfNapalm from './StaffOfNapalm'
import SandalsOfProtection from './SandalsOfProtection'
import BootsOfRunningReallyFast from './BootsOfRunningReallyFast'
import TubaOfCharm from './TubaOfCharm'
import KneepadsOfAllure from './KneepadsOfAllure'
import SneakyBastardSword from './SneakyBastardSword'

const GEARS = [
    SpikyKnees,
    ElevenFootPole,
    RatOnAStick,
    HelmOfCourage,
    Hireling,
    HornyHelmet,
    SlimyArmor,
    LeatherArmor,
    BucklerOfSwashing,
    SingingAndDancingSword,
    BootsOfButtKicking,
    FlamingArmor,
    LimburgerAndAnchovySandwich,
    Stepladder,
    ReallyImpressiveTitle,
    PantyhoseOfGiantStrength,
    PointyHatOfPower,
    BadAssBandanna,
    MithrilArmor,
    ShortWideArmor,
    ChainsawOfBloodyDismemberment,
    HugeRock,
    BroadSword,
    GentlemansClub,
    DaggerOfTreachery,
    RapierOfUnfairness,
    CheeseGraterOfPeace,
    CloakOfObscurity,
    SwissArmyPolearm,
    BowWithRibbons,
    MaceOfSharpness,
    HammerOfKneecapping,
    ShieldOfUbiquity,
    StaffOfNapalm,
    SandalsOfProtection,
    BootsOfRunningReallyFast,
    TubaOfCharm,
    KneepadsOfAllure,
    SneakyBastardSword
]

export default GEARS