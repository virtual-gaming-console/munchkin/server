import CardInstantiator from "../../../models/Card/CardInstantiator"
import GearCard from "../../../models/Card/Gear/GearCard"
import GearPlacement from "../../../models/Card/Gear/GearPlacement"
import PlayerClassType from "../../../models/Player/PlayerClass/PlayerClassType"

const ShieldOfUbiquity = new CardInstantiator(
    132,
    () => new GearCard(
        132,
        "Bouclier gigantesque",
        4,
        GearPlacement.ONE_HAND,
        600,
        PlayerClassType.WAR
    )
)

export default ShieldOfUbiquity