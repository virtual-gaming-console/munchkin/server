import CardInstantiator from "../../../../models/Card/CardInstantiator"
import TreasureConsumableCard from "../../../../models/Card/Consumable/TreasureConsumableCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"
import TurnPhase from "../../../../models/TurnPhase"

const TransferralPotion = new CardInstantiator(
    143,
    () => new TreasureConsumableCard(
        143,
        "Potion de transfert",
        200,
        (handler: Player) => {
            return true
        },
        (handler: Player) => {
            return handler.currentGame.players
                .filter(player => handler.currentGame.currentBattle?.allies.find(ally => player.deviantUser?.id === ally.player.deviantUser?.id) === undefined)
                .map(player => new Target((player.deviantUser?.nickname || ''), [(player.deviantUser?.id || 'X')]))
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (targets && targets.length === 1) {
                game.currentBattle?.replaceAlly(game.whoIsPlaying(), targets[0])
                return true
            }

            return false
        },
        [
            TurnPhase.COMBAT
        ],
    )
)

export default TransferralPotion