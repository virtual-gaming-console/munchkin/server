import CardInstantiator from "../../../../models/Card/CardInstantiator"
import TreasureConsumableCard from "../../../../models/Card/Consumable/TreasureConsumableCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"

const StealALevel = new CardInstantiator(
    140,
    () => new TreasureConsumableCard(
        140,
        "Vol de niveau",
        0,
        (handler: Player) => {
            return handler.level <= 8 && handler.currentGame.players
                .filter(player => player.deviantUser?.id !== handler.deviantUser?.id && player.level > 1).length
        },
        (handler: Player) => {
            return handler.currentGame.players
                .filter(player => player.deviantUser?.id !== handler.deviantUser?.id && player.level > 1)
                .map(player => new Target((player.deviantUser?.nickname || 'X'), [(player.deviantUser?.id || 'X')]))
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (targets && targets.length === 1) {
                targets[0].level--
                launcher.level++
            }

            return true
        }
    )
)

export default StealALevel