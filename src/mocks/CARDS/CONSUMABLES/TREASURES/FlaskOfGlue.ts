import CardInstantiator from "../../../../models/Card/CardInstantiator"
import TreasureConsumableCard from "../../../../models/Card/Consumable/TreasureConsumableCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"
import TurnPhase from "../../../../models/TurnPhase"

const FlaskOfGlue = new CardInstantiator(
    97,
    () => new TreasureConsumableCard(
        97,
        "Flaque de colle",
        100,
        (handler: Player) => {
            for (let x = 0; x < (handler.currentGame.currentBattle?.allies || []).length; x++) {
                const ally = handler.currentGame.currentBattle?.allies[x]
                if (ally) {
                    const { player } = ally
                    const monster = handler.currentGame.currentBattle?.enemies.cards[0]
                    if (monster && player.rolling && player.rolling.dice + player.rolling.modifier >= 5) {
                        return true
                    }
                }
            }

            return false
        },
        (handler: Player) => {
            const targets: Array<Target> = []
            handler.currentGame.currentBattle?.allies.forEach(({ player }) => {
                const monster = handler.currentGame.currentBattle?.enemies.cards[0]
                if (monster && player.rolling && player.rolling.dice + player.rolling.modifier >= 5) {
                    targets.push(new Target(`${player.deviantUser?.nickname}'s escape from ${monster.name}`, [(player.deviantUser?.id || '')]))
                }
            })

            return targets
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (targets && targets.length === 1) {
                const currentTargetInBattle = game.currentBattle?.allies.find(({ player }) => player.deviantUser?.id === targets[0].deviantUser?.id)
                if (currentTargetInBattle) {
                    currentTargetInBattle.player.roll(currentTargetInBattle.player.rolling?.callback)
                }
            }
            return false
        },
        [
            TurnPhase.FLEE
        ]
    )
)

export default FlaskOfGlue