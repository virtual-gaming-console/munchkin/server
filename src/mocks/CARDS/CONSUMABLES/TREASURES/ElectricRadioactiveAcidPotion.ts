import MODIFIERS from "../../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../../models/Card/CardInstantiator"
import TreasureConsumableCard from "../../../../models/Card/Consumable/TreasureConsumableCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"
import TurnPhase from "../../../../models/TurnPhase"

const ElectricRadioactiveAcidPotion = new CardInstantiator(
    93,
    () => new TreasureConsumableCard(
        93,
        "Potion acide radioactive et électrique",
        200,
        (handler: Player) => {
            return handler.currentGame.currentBattle?.enemies.cards.length
        },
        (handler: Player) => {
            return [
                new Target("Les monstres", undefined, handler.currentGame.currentBattle?.enemies.cards.map(card => card.id)),
                new Target("Le groupe", [(handler.currentGame.whoIsPlaying().deviantUser?.id || 'X')]),
            ]
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            game.currentBattle?.newModifier(MODIFIERS.ELECTRIC_RADIOACTIVE_ACID_POTION(), (cardIds !== undefined && cardIds.length > 0))

            return true
        },
        [
            TurnPhase.COMBAT
        ]
    )
)

export default ElectricRadioactiveAcidPotion