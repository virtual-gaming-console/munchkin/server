import MODIFIERS from "../../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../../models/Card/CardInstantiator"
import TreasureConsumableCard from "../../../../models/Card/Consumable/TreasureConsumableCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"
import TurnPhase from "../../../../models/TurnPhase"

const PollymorphPotion = new CardInstantiator(
    123,
    () => new TreasureConsumableCard(
        123,
        "Potion de polymorphie",
        1300,
        (handler: Player) => {
            return handler.currentGame.currentBattle?.enemies.cards.length
        },
        (handler: Player) => {
            return handler.currentGame.currentBattle?.enemies.cards.map(monster => new Target(monster.name, undefined, [monster.id]))
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (cardIds && cardIds.length === 1) {
                const monsterInBattle = game.currentBattle?.enemies.cards.find(monster => monster.id === cardIds[0])
                if (monsterInBattle) {
                    if (game.currentBattle?.enemies && game.currentBattle?.enemies.cards.length > 1) {
                        game.currentBattle?.newModifier(MODIFIERS.POLLYMORPH_POTION(), false, { treasures: [monsterInBattle.reward.treasures] })
                    } else {
                        game.currentBattle?.allies.forEach(ally => {
                            ally.player.draw(ally.treasureRewards.reduce((a, b) => a + b, 0), true)
                        })
                    }
                    game.discard.add(monsterInBattle)

                    return true
                }

                return false
            }
        },
        [
            TurnPhase.COMBAT,
            TurnPhase.FLEE
        ]
    )
)

export default PollymorphPotion