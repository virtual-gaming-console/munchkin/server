import CardInstantiator from "../../../../models/Card/CardInstantiator"
import TreasureConsumableCard from "../../../../models/Card/Consumable/TreasureConsumableCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"
import TurnPhase from "../../../../models/TurnPhase"

const Hoard = new CardInstantiator(
    104,
    () => new TreasureConsumableCard(
        104,
        "Pillaaaaaaaaaage!",
        0,
        (handler: Player) => {
            return handler.currentGame.whoIsPlaying().deviantUser?.id === handler.deviantUser?.id
        },
        (handler: Player) => {
            return [
                new Target("Moi"),
            ]
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            launcher.draw(3, true)

            return true
        },
        [
            TurnPhase.FIRST_STANDBY,
            TurnPhase.STANDBY,
        ]
    ),
)

export default Hoard