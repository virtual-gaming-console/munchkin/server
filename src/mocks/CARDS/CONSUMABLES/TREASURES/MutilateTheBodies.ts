import CardInstantiator from "../../../../models/Card/CardInstantiator"
import TreasureConsumableCard from "../../../../models/Card/Consumable/TreasureConsumableCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"
import TurnPhase from "../../../../models/TurnPhase"

const MutilateTheBodies = new CardInstantiator(
    119,
    () => new TreasureConsumableCard(
        119,
        "Mutiler les cadavres",
        0,
        (handler: Player) => {
            return handler.level <= 8 && handler.currentGame.phasesThisTurn.find(phase => phase === TurnPhase.COMBAT)
        },
        (handler: Player) => {
            return [new Target('Moi')]
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            launcher.level++

            return true
        },
        [
            TurnPhase.STANDBY
        ]
    )
)

export default MutilateTheBodies