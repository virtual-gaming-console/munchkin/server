import CardInstantiator from "../../../../models/Card/CardInstantiator"
import TreasureConsumableCard from "../../../../models/Card/Consumable/TreasureConsumableCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"
import TurnPhase from "../../../../models/TurnPhase"

const InstantWall = new CardInstantiator(
    107,
    () => new TreasureConsumableCard(
        107,
        "Mur instantané",
        200,
        (handler: Player) => {
            return true
        },
        (handler: Player) => {
            return [
                new Target("Moi", [(handler.deviantUser?.id || 'x')]),
                new Target("Le groupe", handler.currentGame.currentBattle?.allies.map(({ player }) => (player.deviantUser?.id || 'x')))
            ]
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (targets && targets.length <= 2) {
                targets.forEach(target => {
                    game.currentBattle?.removeAlly(target)
                })

                return true
            }

            return false
        },
        [
            TurnPhase.FLEE
        ],
    )
)

export default InstantWall