import CardInstantiator from "../../../../models/Card/CardInstantiator"
import TreasureConsumableCard from "../../../../models/Card/Consumable/TreasureConsumableCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"
import TurnPhase from "../../../../models/TurnPhase"

const Doppleganger = new CardInstantiator(
    92,
    () => new TreasureConsumableCard(
        92,
        "Doppleganger",
        300,
        (handler: Player) => {
            return handler.currentGame.whoIsPlaying().deviantUser?.id === handler.deviantUser?.id && handler.currentGame.currentBattle?.canHelp
        },
        (handler: Player) => {
            return [new Target('Moi')]
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (game.whoIsPlaying().deviantUser?.id === launcher.deviantUser?.id && game.currentBattle?.allies.length === 1) {
                game.currentBattle.newAlly(launcher, game.currentBattle.enemies.cards.map(() => 0))

                return true
            }

            return false
        },
        [
            TurnPhase.COMBAT
        ]
    )
)

export default Doppleganger