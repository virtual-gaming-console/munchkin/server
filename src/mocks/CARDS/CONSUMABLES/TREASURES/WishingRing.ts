import CardInstantiator from "../../../../models/Card/CardInstantiator"
import CardType from "../../../../models/Card/CardType"
import TreasureConsumableCard from "../../../../models/Card/Consumable/TreasureConsumableCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"

const WishingRing = new CardInstantiator(
    147,
    () => new TreasureConsumableCard(
        147,
        "Anneau de souhait",
        500,
        (handler: Player) => {
            return handler.currentGame.players.filter(player => player.curses.cards.length).length
        },
        (handler: Player) => {
            const targets: Target[] = []
            handler.currentGame.players
                .filter(player => player.curses.cards.length)
                .forEach(player => {
                    player.curses.cards.forEach(card => {
                        targets.push(new Target((player.deviantUser?.nickname || 'X'), [(player.deviantUser?.id || 'X')], [card.id]))
                    })
                })

            return targets
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (targets && targets.length === 1 && cardIds && cardIds.length === 1) {
                const card = targets[0].curses.cards.find(cardItem => cardItem.id === cardIds[0])
                if (card && card.type === CardType.CURSE) {
                    game.discard.add(card)

                    return true
                }
            }

            return false
        }
    )
)

export default WishingRing