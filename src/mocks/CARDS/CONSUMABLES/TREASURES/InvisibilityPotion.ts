import CardInstantiator from "../../../../models/Card/CardInstantiator"
import TreasureConsumableCard from "../../../../models/Card/Consumable/TreasureConsumableCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"
import TurnPhase from "../../../../models/TurnPhase"

const InvisibilityPotion = new CardInstantiator(
    108,
    () => new TreasureConsumableCard(
        108,
        "Potion d'invisibilité",
        200,
        (handler: Player) => {
            const monster = handler.currentGame.currentBattle?.enemies.cards[0]
            const ally = handler.currentGame.currentBattle?.allies.find(allyItem => allyItem.player.deviantUser?.id === handler.deviantUser?.id)
            if (monster && ally && handler.rolling && handler.rolling.dice + handler.rolling.modifier < 5) {
                return true
            }

            return false
        },
        (handler: Player) => {
            return [
                new Target("Moi")
            ]
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            const monster = launcher.currentGame.currentBattle?.enemies.cards[0]
            const ally = launcher.currentGame.currentBattle?.allies.find(allyItem => allyItem.player.deviantUser?.id === launcher.deviantUser?.id)
            if (monster && ally && launcher.rolling && launcher.rolling.dice + launcher.rolling.modifier < 5) {
                game.currentBattle?.removeAlly(launcher)
                return true
            }

            return false
        },
        [
            TurnPhase.FLEE
        ],
    )
)

export default InvisibilityPotion