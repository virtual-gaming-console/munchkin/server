import CardInstantiator from "../../../../models/Card/CardInstantiator"
import Choice from "../../../../models/Card/Choice"
import TreasureConsumableCard from "../../../../models/Card/Consumable/TreasureConsumableCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"

const WandOfDowsing = new CardInstantiator(
    145,
    () => new TreasureConsumableCard(
        145,
        "Bâton de sourcier",
        1100,
        (handler: Player) => {
            return handler.currentGame.discard.cards.length
        },
        (handler: Player) => {
            return [
                new Target('La défausse')
            ]
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            launcher.currentGame.choiceCallbacks.push({
                sendChoices: () => {
                    const choices: Choice[] = [
                        ...game.discard.cards.map(card => ({ name: card.name, cardId: card.id, isVisible: true })),
                    ]

                    launcher.websocket?.send(
                        {
                            title: 'show-choices',
                            data: {
                                choices,
                                allowCancel: true,
                                name: 'CHOISI UNE CARTE DE LA DEFAUSSE'
                            }
                        }
                    )
                },
                choiceCallback: (choices: Choice[]) => {
                    if (!choices.length) {
                        return false
                    }

                    let choice = choices[0]
                    const card = game.discard.cards.find(card => card.id === choice.cardId)
                    if (card) {
                        launcher.hand.add(card)
                    }

                    return false
                }
            })
        }
    )
)

export default WandOfDowsing