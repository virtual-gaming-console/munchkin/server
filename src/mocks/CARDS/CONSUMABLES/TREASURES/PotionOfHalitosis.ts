import MODIFIERS from "../../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../../models/Card/CardInstantiator"
import TreasureConsumableCard from "../../../../models/Card/Consumable/TreasureConsumableCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"
import TurnPhase from "../../../../models/TurnPhase"

const PotionOfHalitosis = new CardInstantiator(
    125,
    () => new TreasureConsumableCard(
        125,
        "Potion de mauvaise haleine",
        300,
        (handler: Player) => {
            return handler.currentGame.currentBattle?.enemies.cards.length
        },
        (handler: Player) => {
            return [
                new Target("Les monstres", undefined, handler.currentGame.currentBattle?.enemies.cards.map(card => card.id)),
                new Target("Le groupe", [(handler.currentGame.whoIsPlaying().deviantUser?.id || 'X')]),
            ]
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            const floatingNozeInBattle = game.currentBattle?.enemies.cards.find(card => card.id === 33)
            if (floatingNozeInBattle) {
                game.currentBattle?.allies[0].player.kill(floatingNozeInBattle)
                if (game.currentBattle?.enemies.cards.length === 1) {
                    game.currentBattle?.alliesModifiers.filter(modifier => modifier.experience).forEach(modifier => {
                        if (game.currentBattle) {
                            game.currentBattle.allies[0].player.level += modifier.experience as number
                        }
                    })
                    game.currentBattle?.allies.forEach(ally => {
                        ally.player.draw(ally.treasureRewards.reduce((a, b) => a + b, 0), true)
                    })
                } else {
                    const index = game.currentBattle?.enemies.cards.findIndex(card => card.id === 33)
                    if (index) {
                        game.currentBattle?.allies.forEach(ally => {
                            ally.player.draw(ally.treasureRewards[index], true)
                        })
                    }
                }
                game.discard.add(floatingNozeInBattle)
            } else {
                game.currentBattle?.newModifier(MODIFIERS.POTION_OF_HALITOSIS(), (cardIds !== undefined && cardIds.length > 0))
            }

            return true
        },
        [
            TurnPhase.COMBAT
        ]
    )
)

export default PotionOfHalitosis