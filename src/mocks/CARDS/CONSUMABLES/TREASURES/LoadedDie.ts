import CardInstantiator from "../../../../models/Card/CardInstantiator"
import TreasureConsumableCard from "../../../../models/Card/Consumable/TreasureConsumableCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"
import TurnPhase from "../../../../models/TurnPhase"

const LoadedDie = new CardInstantiator(
    114,
    () => new TreasureConsumableCard(
        114,
        "Dé pipé",
        300,
        (handler: Player) => {
            return handler.rolling !== undefined
        },
        (handler: Player) => {
            return [1, 2, 3, 4, 5, 6]
                .filter(dice => dice !== handler.rolling?.dice)
                .map(dice => new Target("Jet à " + dice), undefined)
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (launcher.rolling && target) {
                const dice = parseInt(target.name.replace("Jet à ", ""))
                launcher.roll(launcher.rolling.callback, dice)
                return true
            }
            return false
        },
        [
            TurnPhase.FLEE,
            TurnPhase.ROLL
        ]
    )
)

export default LoadedDie