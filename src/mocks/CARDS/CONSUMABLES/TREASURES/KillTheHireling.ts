import CardInstantiator from "../../../../models/Card/CardInstantiator"
import TreasureConsumableCard from "../../../../models/Card/Consumable/TreasureConsumableCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"

const KillTheHireling = new CardInstantiator(
    110,
    () => new TreasureConsumableCard(
        110,
        "Tuer le serviteur",
        0,
        (handler: Player) => {
            return handler.level <= 8 && handler.currentGame.players.find(player => player.arsenalCards.find(card => card.id === 103))
        },
        (handler: Player) => {
            return [new Target('Moi')]
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            const player = game.players.find(player => player.arsenalCards.find(card => card.id === 103))
            const hireling = player?.arsenalCards.find(card => card.id === 103)
            if (hireling) {
                game.discard.add(hireling)
            }
            launcher.level++

            return true
        }
    )
)

export default KillTheHireling