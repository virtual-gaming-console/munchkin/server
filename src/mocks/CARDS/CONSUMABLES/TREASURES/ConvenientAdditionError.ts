import CardInstantiator from "../../../../models/Card/CardInstantiator"
import TreasureConsumableCard from "../../../../models/Card/Consumable/TreasureConsumableCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"


const ConvenientAdditionError = new CardInstantiator(
    89,
    () => new TreasureConsumableCard(
        89,
        "Erreur de calcul avantageuse",
        0,
        (handler: Player) => {
            return handler.level <= 8
        },
        (handler: Player) => {
            return [new Target('Moi')]
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            launcher.level++

            return true
        }
    )
)

export default ConvenientAdditionError