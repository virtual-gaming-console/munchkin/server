import CardInstantiator from "../../../../models/Card/CardInstantiator"
import TreasureConsumableCard from "../../../../models/Card/Consumable/TreasureConsumableCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"

const WhineAtTheGM = new CardInstantiator(
    146,
    () => new TreasureConsumableCard(
        146,
        "Pleurnicher dans les jupes du MJ",
        0,
        (handler: Player) => {
            return handler.level <= 8 && handler.currentGame.highestLevelPlayers().find(player => player.deviantUser?.id === handler.deviantUser?.id) === undefined
        },
        (handler: Player) => {
            return [new Target('Moi')]
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            launcher.level++

            return true
        }
    )
)

export default WhineAtTheGM