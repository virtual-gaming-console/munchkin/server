import MODIFIERS from "../../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../../models/Card/CardInstantiator"
import TreasureConsumableCard from "../../../../models/Card/Consumable/TreasureConsumableCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"
import TurnPhase from "../../../../models/TurnPhase"

const FlamingPoisonPotion = new CardInstantiator(
    96,
    () => new TreasureConsumableCard(
        96,
        "Potion de poison enflammé",
        100,
        (handler: Player) => {
            return handler.currentGame.currentBattle?.enemies.cards.length
        },
        (handler: Player) => {
            return [
                new Target("Les monstres", undefined, handler.currentGame.currentBattle?.enemies.cards.map(card => card.id)),
                new Target("Le groupe", [(handler.currentGame.whoIsPlaying().deviantUser?.id || 'X')]),
            ]
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            game.currentBattle?.newModifier(MODIFIERS.FLAMING_POISON_POTION(), (cardIds !== undefined && cardIds.length > 0))

            return true
        },
        [
            TurnPhase.COMBAT
        ]
    )
)

export default FlamingPoisonPotion