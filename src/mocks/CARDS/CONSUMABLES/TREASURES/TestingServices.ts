import CardInstantiator from "../../../../models/Card/CardInstantiator"
import Choice from "../../../../models/Card/Choice"
import TreasureConsumableCard from "../../../../models/Card/Consumable/TreasureConsumableCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"

const TestingServices = new CardInstantiator(
    5555,
    () => new TreasureConsumableCard(
        5555,
        "Testing Services",
        1000,
        (handler: Player) => {
            return true
        },
        (handler: Player) => {
            return [new Target('Moi')]
        },
        (game: Game, laucher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            laucher.currentGame.choiceCallbacks.push({
                sendChoices: () => {
                    const choices: Choice[] = [
                        new Choice('Aller chercher un café'),
                        new Choice('Jouer a Genshin Impact'),
                        new Choice('Regarder Naruto'),
                        new Choice('Ne pas oublier de s\'hydrater'),
                        new Choice('Arreter de coder'),
                        new Choice('Manger 5 fruits et légumes'),
                        new Choice('Boire une bière'),
                        new Choice('Se bagnatte'),
                    ]

                    laucher.websocket?.send(
                        {
                            title: 'show-choices',
                            data: {
                                choices,
                                name: 'PILLE UNE CARTE DE ' + laucher.deviantUser?.nickname.toUpperCase()
                            }
                        }
                    )
                },
                choiceCallback: (choices: Choice[]) => {
                    let choice = choices[0]
                    if (choice) {
                        console.log(choice)
                    }

                    return false
                }
            })

            return true
        }
    )
)

export default TestingServices