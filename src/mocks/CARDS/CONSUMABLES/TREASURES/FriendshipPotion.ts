import CardInstantiator from "../../../../models/Card/CardInstantiator"
import TreasureConsumableCard from "../../../../models/Card/Consumable/TreasureConsumableCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"
import TurnPhase from "../../../../models/TurnPhase"

const FriendshipPotion = new CardInstantiator(
    99,
    () => new TreasureConsumableCard(
        99,
        "Potion d'amitié",
        200,
        (handler: Player) => {
            return handler.currentGame.currentBattle?.enemies.cards.length
        },
        (handler: Player) => {
            return [
                new Target(
                    (handler.currentGame.currentBattle?.enemies.cards || []).length > 1 ? 'Les monstres' : 'Le monstre',
                    undefined,
                    handler.currentGame.currentBattle?.enemies.cards.map(card => card.id)
                )
            ]
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (cardIds && cardIds.length) {
                cardIds.forEach(cardId => {
                    const monsterInBattle = game.currentBattle?.enemies.cards.find(monster => monster.id === cardId)
                    if (monsterInBattle) {
                        game.discard.add(monsterInBattle)
                    }
                })
                game.whoIsPlaying().draw(1, false)
                return true
            }
            return false
        },
        [
            TurnPhase.COMBAT,
            TurnPhase.FLEE
        ]
    )
)

export default FriendshipPotion