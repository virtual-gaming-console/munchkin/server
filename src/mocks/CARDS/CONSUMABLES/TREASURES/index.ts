import BoilAnAnthill from "./BoilAnAnthill"
import BribeGMWithFood from "./BribeGMWithFood"
import ConvenientAdditionError from "./ConvenientAdditionError"
import CotionOfPonfusion from "./CotionOfPonfusion"
import Doppleganger from "./Doppleganger"
import ElectricRadioactiveAcidPotion from "./ElectricRadioactiveAcidPotion"
import FlamingPoisonPotion from "./FlamingPoisonPotion"
import FlaskOfGlue from "./FlaskOfGlue"
import FreezingExplosivePotion from "./FreezingExplosivePotion"
import FriendshipPotion from "./FriendshipPotion"
import Hoard from "./Hoard"
import InstantWall from "./InstantWall"
import InvisibilityPotion from "./InvisibilityPotion"
import InvokeObscureRules from "./InvokeObscureRules"
import KillTheHireling from "./KillTheHireling"
import LoadedDie from "./LoadedDie"
import MagicLamp from "./MagicLamp"
import MagicMissile from "./MagicMissile"
import MutilateTheBodies from "./MutilateTheBodies"
import NastyTastingSportsDrink from "./NastyTastingSportsDrink"
import OneThousandGoldPieces from "./OneThousandGoldPieces"
import PollymorphPotion from "./PollymorphPotion"
import PotionOfGeneralStudliness from "./PotionOfGeneralStudliness"
import PotionOfHalitosis from "./PotionOfHalitosis"
import PotionOfIdioticBravery from "./PotionOfIdioticBravery"
import PrettyBalloons from "./PrettyBalloons"
import SleepPotion from "./SleepPotion"
import StealALevel from "./StealALevel"
import TestingServices from "./TestingServices"
import TransferralPotion from "./TransferralPotion"
import WandOfDowsing from "./WandOfDowsing"
import WhineAtTheGM from "./WhineAtTheGM"
import WishingRing from "./WishingRing"
import YuppieWater from "./YuppieWater"

const TREASURE_CONSUMABLES = [
    InstantWall,
    WishingRing,
    WishingRing,
    LoadedDie,
    MagicLamp,
    Doppleganger,
    FlaskOfGlue,
    PrettyBalloons,
    WandOfDowsing,
    InvisibilityPotion,
    FriendshipPotion,
    PollymorphPotion,
    TransferralPotion,
    PotionOfIdioticBravery,
    YuppieWater,
    FreezingExplosivePotion,
    FlamingPoisonPotion,
    SleepPotion,
    ElectricRadioactiveAcidPotion,
    NastyTastingSportsDrink,
    CotionOfPonfusion,
    PotionOfHalitosis,
    MagicMissile,
    Hoard,
    StealALevel,
    MutilateTheBodies,
    WhineAtTheGM,
    ConvenientAdditionError,
    OneThousandGoldPieces,
    BribeGMWithFood,
    BoilAnAnthill,
    InvokeObscureRules,
    KillTheHireling,
    PotionOfGeneralStudliness,
    // TestingServices // TEST CARD
]

export default TREASURE_CONSUMABLES