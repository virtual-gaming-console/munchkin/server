import MODIFIERS from "../../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../../models/Card/CardInstantiator"
import TreasureConsumableCard from "../../../../models/Card/Consumable/TreasureConsumableCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"
import PlayerRaceType from "../../../../models/Player/PlayerRace/PlayerRaceType"
import TurnPhase from "../../../../models/TurnPhase"

const YuppieWater = new CardInstantiator(
    148,
    () => new TreasureConsumableCard(
        148,
        "Champagne",
        100,
        (handler: Player) => {
            return handler.currentGame.currentBattle?.allies.filter(({ player }) => player.races.find(race => race === PlayerRaceType.ELVEN)).length
        },
        (handler: Player) => {
            return [
                new Target("Le groupe", [(handler.currentGame.whoIsPlaying().deviantUser?.id || 'X')])
            ]
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            game.currentBattle?.newModifier(
                MODIFIERS.YUPPIE_WATER(),
                false,
                {
                    power: game.currentBattle.allies.filter(({ player }) => player.races.find(race => race === PlayerRaceType.ELVEN)).length * 2
                }
            )

            return true
        },
        [
            TurnPhase.COMBAT
        ]
    )
)

export default YuppieWater