import CardInstantiator from "../../../../models/Card/CardInstantiator"
import CardType from "../../../../models/Card/CardType"
import ClassCard from "../../../../models/Card/Consumable/ClassCard"
import DungeonConsumableCard from "../../../../models/Card/Consumable/DungeonConsumableCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"
import CLASSES from "../../../../models/Player/PlayerClass/CLASSES"
import TurnPhase from "../../../../models/TurnPhase"

const SuperMunchkin = new CardInstantiator(
    66,
    () => new DungeonConsumableCard(
        66,
        "Super Munchkin",
        (handler: Player) => {
            return handler.hand.cards.find(card => card.type === CardType.CLASS) !== undefined && handler.currentGame.whoIsPlaying().deviantUser?.id === handler.deviantUser?.id
        },
        (handler: Player) => {
            return handler.hand.cards
                .filter(card => card.type === CardType.CLASS && (card as ClassCard).value !== handler.class?.type)
                .map(card => new Target(
                    '',
                    undefined,
                    [card.id]
                ))
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (cardIds && cardIds.length === 1) {
                const card = launcher.hand.cards.find(card => card.id === cardIds[0])
                if (card) {
                    launcher.superMunchkinClass = new CLASSES[(card as ClassCard).value]
                    const superMunchkinCard = launcher.hand.cards.find(card => card.id === 66)
                    if (superMunchkinCard) {
                        launcher.hand.remove(card)
                        launcher.hand.remove(superMunchkinCard)
                    }
                }
            }

            return true
        },
        [
            TurnPhase.STANDBY,
            TurnPhase.FIRST_STANDBY,
        ]
    )
)

export default SuperMunchkin