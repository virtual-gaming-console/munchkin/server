import CardInstantiator from "../../../../models/Card/CardInstantiator"
import DungeonConsumableCard from "../../../../models/Card/Consumable/DungeonConsumableCard"
import MonsterCard from "../../../../models/Card/Monster/MonsterCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"
import TurnPhase from "../../../../models/TurnPhase"
import MONSTERS from "../../MONSTERS"

const Mate = new CardInstantiator(
    52,
    () => new DungeonConsumableCard(
        52,
        "Petite amie",
        (handler: Player) => {
            return handler.currentGame.currentBattle && handler.currentGame.currentBattle.enemies.cards.length < 2
        },
        (handler: Player) => {
            return handler.currentGame.currentBattle?.enemies.cards
                .map(card => new Target('', undefined, [card.id]))
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (cardIds && cardIds.length === 1) {
                const card = game.currentBattle?.enemies.cards.find(cardItem => cardItem.id === cardIds[0])
                if (card) {
                    const modifiers = [...((game.currentBattle?.enemiesModifiers || []).filter(modifier => modifier.id < 1000))]
                    game.currentBattle?.enemies.add(MONSTERS.find(monsterInstantiator => monsterInstantiator.cardId === cardIds[0])?.instantiate())
                    modifiers.forEach(modifier => game.currentBattle?.newModifier(modifier, true))
                }
            }

            return true
        },
        [
            TurnPhase.COMBAT,
        ]
    )
)

export default Mate