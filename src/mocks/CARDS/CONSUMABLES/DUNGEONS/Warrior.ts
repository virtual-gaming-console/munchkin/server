import CardInstantiator from "../../../../models/Card/CardInstantiator"
import ClassCard from "../../../../models/Card/Consumable/ClassCard"
import PlayerClassType from "../../../../models/Player/PlayerClass/PlayerClassType"

const Warrior = new CardInstantiator(
    74,
    () => new ClassCard(
        74,
        "Guerrier",
        PlayerClassType.WAR
    )
)

export default Warrior