import CardInstantiator from "../../../../models/Card/CardInstantiator"
import RaceCard from "../../../../models/Card/Consumable/RaceCard"
import PlayerRaceType from "../../../../models/Player/PlayerRace/PlayerRaceType"

const Hobbit = new CardInstantiator(
    39,
    () => new RaceCard(
        39,
        "Hobbit",
        PlayerRaceType.HOBBIT
    )
)

export default Hobbit