import MODIFIERS from "../../../../models/Battle/MODIFIERS"
import CardInstantiator from "../../../../models/Card/CardInstantiator"
import DungeonConsumableCard from "../../../../models/Card/Consumable/DungeonConsumableCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"
import TurnPhase from "../../../../models/TurnPhase"

const Intelligent = new CardInstantiator(
    46,
    () => new DungeonConsumableCard(
        46,
        "Intelligent",
        (handler: Player) => {
            return handler.currentGame.currentBattle?.enemies.cards.length
        },
        (handler: Player) => {
            return [
                new Target(
                    (handler.currentGame.currentBattle?.enemies.cards || []).length > 1 ? 'Les monstres' : 'Le monstre',
                    undefined,
                    handler.currentGame.currentBattle?.enemies.cards.map(card => card.id)
                )
            ]
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            game.currentBattle?.newModifier(MODIFIERS.INTELLIGENT(), true)

            return true
        },
        [
            TurnPhase.COMBAT
        ]
    )
)

export default Intelligent