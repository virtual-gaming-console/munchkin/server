import CardInstantiator from "../../../../models/Card/CardInstantiator"
import RaceCard from "../../../../models/Card/Consumable/RaceCard"
import PlayerRaceType from "../../../../models/Player/PlayerRace/PlayerRaceType"

const Dwarf = new CardInstantiator(
    29,
    () => new RaceCard(
        29,
        "Nain",
        PlayerRaceType.DWARF
    )
)

export default Dwarf