import CardInstantiator from "../../../../models/Card/CardInstantiator"
import CardType from "../../../../models/Card/CardType"
import DungeonConsumableCard from "../../../../models/Card/Consumable/DungeonConsumableCard"
import RaceCard from "../../../../models/Card/Consumable/RaceCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"
import RACES from "../../../../models/Player/PlayerRace/RACES"
import TurnPhase from "../../../../models/TurnPhase"

const HalfBreed = new CardInstantiator(
    38,
    () => new DungeonConsumableCard(
        38,
        "Sang-mêlée",
        (handler: Player) => {
            return handler.hand.cards.find(card => card.type === CardType.RACE) !== undefined && handler.currentGame.whoIsPlaying().deviantUser?.id === handler.deviantUser?.id
        },
        (handler: Player) => {
            return handler.hand.cards
                .filter(card => card.type === CardType.RACE && (card as RaceCard).value !== handler.race?.type)
                .map(card => new Target(
                    '',
                    undefined,
                    [card.id]
                ))
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (cardIds && cardIds.length === 1) {
                const card = launcher.hand.cards.find(card => card.id === cardIds[0])
                if (card) {
                    launcher.halvenRace = new RACES[(card as RaceCard).value]
                    const halvenCard = launcher.hand.cards.find(card => card.id === 38)
                    if (halvenCard) {
                        launcher.hand.remove(card)
                        launcher.hand.remove(halvenCard)
                    }
                }
            }

            return true
        },
        [
            TurnPhase.STANDBY,
            TurnPhase.FIRST_STANDBY,
        ]
    )
)

export default HalfBreed