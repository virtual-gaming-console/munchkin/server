import CardInstantiator from "../../../../models/Card/CardInstantiator"
import DungeonConsumableCard from "../../../../models/Card/Consumable/DungeonConsumableCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"
import PlayerClassType from "../../../../models/Player/PlayerClass/PlayerClassType"

const DivineIntervention = new CardInstantiator(
    27,
    () => new DungeonConsumableCard(
        27,
        "Intervention divine",
        (handler: Player) => {
            return true
        },
        (handler: Player) => {
            return []
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            game.players
                .filter(player => player.classes.find(classType => classType === PlayerClassType.PRIEST))
                .forEach(player => {
                    player.level++
                })

            return true
        }
    )
)

export default DivineIntervention