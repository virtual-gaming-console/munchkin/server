import CardInstantiator from "../../../../models/Card/CardInstantiator"
import CardType from "../../../../models/Card/CardType"
import Choice from "../../../../models/Card/Choice"
import DungeonConsumableCard from "../../../../models/Card/Consumable/DungeonConsumableCard"
import MonsterCard from "../../../../models/Card/Monster/MonsterCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"
import TurnPhase from "../../../../models/TurnPhase"

const Illusion = new CardInstantiator(
    44,
    () => new DungeonConsumableCard(
        44,
        "Illusion",
        (handler: Player) => {
            return handler.hand.cards.filter(cardItem => cardItem.type === CardType.MONSTER).length
        },
        (handler: Player) => {
            return handler.hand.cards
                .filter(cardItem => cardItem.type === CardType.MONSTER)
                .map(card => new Target('', undefined, [card.id]))
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (cardIds && cardIds.length === 1) {
                const card = launcher.hand.cards.find(cardItem => cardItem.id === cardIds[0])
                if (game.currentBattle?.enemies.cards.length === 1) {
                    game.currentBattle.enemies.add(card as MonsterCard)
                    game.discard.add(game.currentBattle.enemies.cards[0])
                } else {
                    game.choiceCallbacks.push({
                        sendChoices: () => {
                            const choices: Choice[] = (game.currentBattle?.enemies.cards || []).map(card => ({ name: card.name, cardId: card.id, isVisible: true }))

                            launcher.websocket?.send(
                                {
                                    title: 'show-choices',
                                    data: {
                                        choices,
                                        name: 'QUEL MONSTRE SOUHAITE TU REMPLACER ?'
                                    }
                                }
                            )
                        },
                        choiceCallback: (choices: Choice[]) => {
                            let choice = choices[0]
                            if (!choice) {
                                const choices: Choice[] = (game.currentBattle?.enemies.cards || []).map(card => ({ name: card.name, cardId: card.id }))
                                choice = choices[Math.floor(Math.random() * choices.length)]
                            }

                            let cardToRemove = game.currentBattle?.enemies.cards.find(cardItem => cardItem.id === choice.cardId)

                            if (cardToRemove && game.currentBattle) {
                                game.currentBattle.enemies.add(card as MonsterCard)
                                game.discard.add(cardToRemove)
                            }

                            return true
                        }
                    })
                }
            }

            return true
        },
        [
            TurnPhase.COMBAT,
        ]
    )
)

export default Illusion