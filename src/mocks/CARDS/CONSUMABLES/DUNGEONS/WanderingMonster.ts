import CardInstantiator from "../../../../models/Card/CardInstantiator"
import CardType from "../../../../models/Card/CardType"
import DungeonConsumableCard from "../../../../models/Card/Consumable/DungeonConsumableCard"
import MonsterCard from "../../../../models/Card/Monster/MonsterCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"
import TurnPhase from "../../../../models/TurnPhase"

const WanderingMonster = new CardInstantiator(
    72,
    () => new DungeonConsumableCard(
        72,
        "Monstre errant",
        (handler: Player) => {
            return handler.hand.cards.filter(cardItem => cardItem.type === CardType.MONSTER).length && handler.currentGame.currentBattle && handler.currentGame.currentBattle.enemies.cards.length < 2
        },
        (handler: Player) => {
            return handler.hand.cards
                .filter(cardItem => cardItem.type === CardType.MONSTER)
                .map(card => new Target('', undefined, [card.id]))
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (cardIds && cardIds.length === 1) {
                const card = launcher.hand.cards.find(cardItem => cardItem.id === cardIds[0])
                if (card) {
                    game.currentBattle?.enemies.add(card as MonsterCard)
                }
            }

            return true
        },
        [
            TurnPhase.COMBAT,
        ]
    )
)

export default WanderingMonster