import CardInstantiator from "../../../../models/Card/CardInstantiator"
import CardType from "../../../../models/Card/CardType"
import DungeonConsumableCard from "../../../../models/Card/Consumable/DungeonConsumableCard"
import Equipable from "../../../../models/Card/Gear/Equipable"
import GearCard from "../../../../models/Card/Gear/GearCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"
import TurnPhase from "../../../../models/TurnPhase"

const Cheat = new CardInstantiator(
    7,
    () => new DungeonConsumableCard(
        7,
        "Tricheur!",
        (handler: Player) => {
            return handler.hand.cards.filter(card => card.type === CardType.GEAR && (card as GearCard).canEquip(handler) !== Equipable.YES).length
        },
        (handler: Player) => {
            return handler.hand.cards
                .filter(card => card.type === CardType.GEAR && (card as GearCard).canEquip(handler) !== Equipable.YES)
                .map(card => new Target('', undefined, [card.id]))
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (cardIds && cardIds.length === 1) {
                const card = launcher.hand.cards.find(cardItem => cardItem.id === cardIds[0])
                if (card) {
                    launcher.equip(card as GearCard, undefined, true)
                    const cheat = launcher.hand.cards.find(cardItem => cardItem.id === 7)
                    if (cheat) {
                        launcher.hand.remove(cheat)
                    }
                }
            }

            return true
        },
        [
            TurnPhase.STANDBY,
            TurnPhase.FIRST_STANDBY,
        ]
    )
)

export default Cheat