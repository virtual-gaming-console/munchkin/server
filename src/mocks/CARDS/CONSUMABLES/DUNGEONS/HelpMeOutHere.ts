import CardInstantiator from "../../../../models/Card/CardInstantiator"
import DungeonConsumableCard from "../../../../models/Card/Consumable/DungeonConsumableCard"
import Equipable from "../../../../models/Card/Gear/Equipable"
import GearPlacement from "../../../../models/Card/Gear/GearPlacement"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"
import TurnPhase from "../../../../models/TurnPhase"

const getTargets = (handler: Player) => {
    let targets: Array<Target | undefined> = []
    const otherPlayers = handler.currentGame.players.filter(player => player.deviantUser?.id !== handler.deviantUser?.id)
    otherPlayers.forEach(player => {
        targets = [
            ...targets,
            ...(
                player.arsenalCards
                    .filter(gearCard => gearCard.canEquip(handler) !== Equipable.NO)
                    .map(gearCard => {
                        if (gearCard.canEquip(handler) === Equipable.YES) {
                            if ((handler.currentGame.currentBattle?.alliesPower || 0) + gearCard.getTotalBonus(handler) > (handler.currentGame.currentBattle?.enemiesPower || 0)) {
                                return new Target('' + player.deviantUser?.nickname.toUpperCase(), [(player.deviantUser?.id || 'X')], [gearCard.id])
                            } else {
                                return undefined
                            }
                        } else {
                            switch (gearCard.placement) {
                                case GearPlacement.ARMOR:
                                case GearPlacement.BOOTS:
                                case GearPlacement.HEAD:
                                    const cardToSwap = handler.arsenalCards.find(gearCardToSwap => gearCardToSwap.placement === gearCard.placement)
                                    if (cardToSwap) {
                                        if ((handler.currentGame.currentBattle?.alliesPower || 0) - cardToSwap.getTotalBonus(handler) + gearCard.getTotalBonus(handler) > (handler.currentGame.currentBattle?.enemiesPower || 0)) {
                                            return new Target('' + player.deviantUser?.nickname.toUpperCase(), [(player.deviantUser?.id || 'X')], [gearCard.id, cardToSwap.id])
                                        } else {
                                            return undefined
                                        }
                                    }
                                    break
                                case GearPlacement.ONE_HAND:
                                    const sameSlotGears = handler.arsenalCards
                                        .filter(gear => gear.placement === gearCard.placement)
                                        .sort((a, b) => a.bonus - b.bonus)
                                    const twoHandGearEquipped = handler.arsenalCards.find(gear => gear.placement === GearPlacement.TWO_HAND)
                                    if (twoHandGearEquipped) {
                                        if ((handler.currentGame.currentBattle?.alliesPower || 0) - twoHandGearEquipped.getTotalBonus(handler) + gearCard.getTotalBonus(handler) > (handler.currentGame.currentBattle?.enemiesPower || 0)) {
                                            return new Target('' + player.deviantUser?.nickname.toUpperCase(), [(player.deviantUser?.id || 'X')], [gearCard.id, twoHandGearEquipped.id])
                                        } else {
                                            return undefined
                                        }
                                    } else if (sameSlotGears.length === 2) {
                                        const gearToSwap = sameSlotGears.find(sameGear => (handler.currentGame.currentBattle?.alliesPower || 0) - sameGear.getTotalBonus(handler) + gearCard.getTotalBonus(handler) > (handler.currentGame.currentBattle?.enemiesPower || 0))
                                        if (gearToSwap) {
                                            return new Target('' + player.deviantUser?.nickname.toUpperCase(), [(player.deviantUser?.id || 'X')], [gearCard.id, gearToSwap.id])
                                        } else {
                                            return undefined
                                        }
                                    }
                                    break
                                case GearPlacement.TWO_HAND:
                                    const oneHandGearEquipped = handler.arsenalCards.filter(gear => gear.placement === GearPlacement.ONE_HAND)
                                    const oneHandGearEquippedBonuses = oneHandGearEquipped.map(gear => gear.getTotalBonus(handler)).reduce((a, b) => a + b, 0)
                                    if ((handler.currentGame.currentBattle?.alliesPower || 0) - oneHandGearEquippedBonuses + gearCard.getTotalBonus(handler) > (handler.currentGame.currentBattle?.enemiesPower || 0)) {
                                        return new Target('' + player.deviantUser?.nickname.toUpperCase(), [(player.deviantUser?.id || 'X')], [gearCard.id, ...oneHandGearEquipped.map(gear => gear.id)])
                                    } else {
                                        return undefined
                                    }
                                    break
                                default:
                                    return undefined
                            }
                        }
                    })
                    .filter(target => target !== undefined)
            )
        ]
    })
    return targets
}

const HelpMeOutHere = new CardInstantiator(
    41,
    () => new DungeonConsumableCard(
        41,
        "Sors-moi de là!",
        (handler: Player) => {
            if (handler.currentGame.currentBattle?.allies.find(({ player }) => player.deviantUser?.id === handler.deviantUser?.id) === undefined) {
                return false
            }

            if (handler.currentGame.currentBattle?.alliesAreWinning) {
                return false
            }

            return getTargets(handler).length
        },
        getTargets,
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            console.log(targets)
            console.log(cardIds)
            if (targets && targets.length === 1 && cardIds && cardIds.length) {
                const target = targets[0]
                const cardIdToEquip = cardIds.shift()
                const card = target.arsenalCards.find(card => card.id === cardIdToEquip)
                if (card) {
                    cardIds.forEach(cardId => {
                        const cardToDiscard = launcher.arsenalCards.find(cardItem => cardItem.id === cardId)
                        if (cardToDiscard) {
                            launcher.currentGame.discard.add(cardToDiscard)
                        }
                    })
                    launcher.equip(card)
                }
            }

            return true
        },
        [
            TurnPhase.COMBAT
        ]
    )
)

export default HelpMeOutHere