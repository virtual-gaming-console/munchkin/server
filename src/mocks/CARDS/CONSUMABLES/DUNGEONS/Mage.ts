import CardInstantiator from "../../../../models/Card/CardInstantiator"
import ClassCard from "../../../../models/Card/Consumable/ClassCard"
import PlayerClassType from "../../../../models/Player/PlayerClass/PlayerClassType"

const Mage = new CardInstantiator(
    76,
    () => new ClassCard(
        76,
        "Mage",
        PlayerClassType.MAGE
    )
)

export default Mage