import CardInstantiator from "../../../../models/Card/CardInstantiator"
import ClassCard from "../../../../models/Card/Consumable/ClassCard"
import PlayerClassType from "../../../../models/Player/PlayerClass/PlayerClassType"

const Priest = new CardInstantiator(
    8,
    () => new ClassCard(
        8,
        "Pretre",
        PlayerClassType.PRIEST
    )
)

export default Priest