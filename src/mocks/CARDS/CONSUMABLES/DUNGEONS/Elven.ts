import CardInstantiator from "../../../../models/Card/CardInstantiator"
import RaceCard from "../../../../models/Card/Consumable/RaceCard"
import PlayerRaceType from "../../../../models/Player/PlayerRace/PlayerRaceType"

const Elven = new CardInstantiator(
    30,
    () => new RaceCard(
        30,
        "Elfe",
        PlayerRaceType.ELVEN
    )
)

export default Elven