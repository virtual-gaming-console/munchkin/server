import Ancient from "./Ancient";
import Baby from "./Baby";
import Cheat from "./Cheat";
import DivineIntervention from "./DivineIntervention";
import Enraged from "./Enraged";
import HelpMeOutHere from "./HelpMeOutHere";
import Humongous from "./Humongous";
import Illusion from "./Illusion";
import Intelligent from "./Intelligent";
import Mate from "./Mate";
import OutToLunch from "./OutToLunch";
import Elven from "./Elven";
import WanderingMonster from "./WanderingMonster";
import Dwarf from "./Dwarf";
import Hobbit from "./Hobbit";
import Mage from "./Mage";
import Priest from "./Priest";
import Thief from "./Thief";
import Warrior from "./Warrior";
import HalfBreed from "./HalfBreed";
import SuperMunchkin from "./SuperMunchkin";

const DUNGEON_CONSUMABLES = [
    Ancient,
    Baby,
    Enraged,
    HelpMeOutHere,
    Humongous,
    Intelligent,
    Cheat,
    WanderingMonster,
    WanderingMonster,
    WanderingMonster,
    DivineIntervention,
    Mate,
    OutToLunch,
    Illusion,
    Elven,
    Elven,
    Elven,
    Dwarf,
    Dwarf,
    Dwarf,
    Hobbit,
    Hobbit,
    Hobbit,
    Mage,
    Mage,
    Mage,
    Priest,
    Priest,
    Priest,
    Thief,
    Thief,
    Thief,
    Warrior,
    Warrior,
    Warrior,
    HalfBreed,
    HalfBreed,
    SuperMunchkin,
    SuperMunchkin,
]

export default DUNGEON_CONSUMABLES