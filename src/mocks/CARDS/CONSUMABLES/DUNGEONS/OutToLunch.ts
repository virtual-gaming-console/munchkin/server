import CardInstantiator from "../../../../models/Card/CardInstantiator"
import CardType from "../../../../models/Card/CardType"
import DungeonConsumableCard from "../../../../models/Card/Consumable/DungeonConsumableCard"
import MonsterCard from "../../../../models/Card/Monster/MonsterCard"
import Target from "../../../../models/Card/Target"
import Game from "../../../../models/Game"
import Player from "../../../../models/Player/Player"
import TurnPhase from "../../../../models/TurnPhase"

const OutToLunch = new CardInstantiator(
    56,
    () => new DungeonConsumableCard(
        56,
        "Pause déjeuner",
        (handler: Player) => {
            return true
        },
        (handler: Player) => {
            return [
                new Target(
                    (handler.currentGame.currentBattle?.enemies.cards || []).length > 1 ? 'Les monstres' : 'Le monstre',
                    undefined,
                    handler.currentGame.currentBattle?.enemies.cards.map(card => card.id)
                )
            ]
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            game.currentBattle?.allies[0].player.draw(2, true)
            game.currentBattle?.enemies.cards.forEach(card => {
                game.discard.add(card)
            })

            return true
        },
        [
            TurnPhase.COMBAT,
        ]
    )
)

export default OutToLunch