import CardInstantiator from "../../../../models/Card/CardInstantiator"
import ClassCard from "../../../../models/Card/Consumable/ClassCard"
import PlayerClassType from "../../../../models/Player/PlayerClass/PlayerClassType"

const Thief = new CardInstantiator(
    67,
    () => new ClassCard(
        67,
        "Voleur",
        PlayerClassType.THIEF
    )
)

export default Thief