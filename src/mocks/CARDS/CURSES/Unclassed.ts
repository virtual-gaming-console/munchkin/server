import CardInstantiator from "../../../models/Card/CardInstantiator";
import CurseCard from "../../../models/Card/Consumable/CurseCard";
import Target from "../../../models/Card/Target";
import Game from "../../../models/Game";
import Player from "../../../models/Player/Player";

const Unclassed = new CardInstantiator(
    23,
    () => new CurseCard(
        23,
        "Déclassé!",
        (handler: Player) => {
            return handler.currentGame.players
                .filter(player => player.deviantUser?.id !== handler.deviantUser?.id).length
        },
        (handler: Player) => {
            return handler.currentGame.players
                .filter(player => player.deviantUser?.id !== handler.deviantUser?.id)
                .map(player => new Target((player.deviantUser?.nickname || 'X'), [(player.deviantUser?.id || 'X')]))
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (targets && targets.length === 1) {
                if (!targets[0].classes.length) {
                    targets[0].level--
                } else {
                    targets[0].superMunchkinClass = undefined
                    targets[0].class = undefined
                }
            }

            return true
        }
    )
)

export default Unclassed