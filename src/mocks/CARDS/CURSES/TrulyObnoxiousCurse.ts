import CardInstantiator from "../../../models/Card/CardInstantiator";
import Choice from "../../../models/Card/Choice";
import CurseCard from "../../../models/Card/Consumable/CurseCard";
import Target from "../../../models/Card/Target";
import Game from "../../../models/Game";
import Player from "../../../models/Player/Player";

const TrulyObnoxiousCurse = new CardInstantiator(
    69,
    () => new CurseCard(
        69,
        "Malédiction vraiment trop injuste!",
        (handler: Player) => {
            return handler.currentGame.players
                .filter(player => player.deviantUser?.id !== handler.deviantUser?.id && player.arsenalCards.length).length
        },
        (handler: Player) => {
            return handler.currentGame.players
                .filter(player => player.deviantUser?.id !== handler.deviantUser?.id && player.arsenalCards.length)
                .map(player => new Target((player.deviantUser?.nickname || 'X'), [(player.deviantUser?.id || 'X')]))
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (targets && targets.length === 1) {
                const highestBonus = Math.max(...targets[0].arsenalCards.map(card => card.getTotalBonus(targets[0])))
                const highestBonusCards = targets[0].arsenalCards.filter(card => card.getTotalBonus(targets[0]) === highestBonus)
                if (!highestBonusCards.length) {
                    return false
                } else if (highestBonusCards.length === 1) {
                    game.discard.add(highestBonusCards[0])
                } else {
                    launcher.currentGame.choiceCallbacks.push({
                        sendChoices: () => {
                            const choices: Choice[] = [
                                ...highestBonusCards.map(card => ({ name: card.name, cardId: card.id, isVisible: true })),
                            ]

                            targets[0].websocket?.send(
                                {
                                    title: 'show-choices',
                                    data: {
                                        choices,
                                        name: 'CHOISI UN OBJET A DEFAUSSER'
                                    }
                                }
                            )
                        },
                        choiceCallback: (choices: Choice[]) => {
                            let choice = choices[0]
                            if (!choice) {
                                const choices: Choice[] = [
                                    ...highestBonusCards.map(card => ({ name: card.name, cardId: card.id })),
                                ]
                                choice = choices[Math.floor(Math.random() * choices.length)]
                            }

                            let card = targets[0].arsenalCards.find(cardItem => cardItem.id === choice.cardId)

                            if (card) {
                                game.discard.add(card)
                            }

                            return false
                        }
                    })
                }
            }

            return true
        }
    )
)

export default TrulyObnoxiousCurse