import MODIFIERS from "../../../models/Battle/MODIFIERS";
import CardInstantiator from "../../../models/Card/CardInstantiator";
import CurseCard from "../../../models/Card/Consumable/CurseCard";
import RemovalAction from "../../../models/Card/Consumable/RemovalAction";
import Target from "../../../models/Card/Target";
import Game from "../../../models/Game";
import Gender from "../../../models/Player/Gender";
import Player from "../../../models/Player/Player";

const ChickenOnTheHead = new CardInstantiator(
    13,
    () => new CurseCard(
        13,
        "Poulet sur la tête!",
        (handler: Player) => {
            return handler.currentGame.players
                .filter(player => player.deviantUser?.id !== handler.deviantUser?.id).length
        },
        (handler: Player) => {
            return handler.currentGame.players
                .filter(player => player.deviantUser?.id !== handler.deviantUser?.id)
                .map(player => new Target((player.deviantUser?.nickname || 'X'), [(player.deviantUser?.id || 'X')]))
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (targets && targets.length === 1) {
                const card = launcher.hand.cards.find(cardItem => cardItem.id === 25)
                if (card) {
                    targets[0].curses.add(card as CurseCard)
                }
            }

            return true
        },
        {
            modifier: MODIFIERS.CHICKEN_ON_THE_HEAD(),
            removalAction: RemovalAction.LOSING_HEAD
        }
    )
)

export default ChickenOnTheHead