import CardInstantiator from "../../../models/Card/CardInstantiator";
import Choice from "../../../models/Card/Choice";
import CurseCard from "../../../models/Card/Consumable/CurseCard";
import Target from "../../../models/Card/Target";
import Game from "../../../models/Game";
import Player from "../../../models/Player/Player";

const LoseAnItem = new CardInstantiator(
    18,
    () => new CurseCard(
        18,
        "Perte!",
        (handler: Player) => {
            return handler.currentGame.players
                .filter(player => player.deviantUser?.id !== handler.deviantUser?.id && player.arsenalCards.length).length
        },
        (handler: Player) => {
            return handler.currentGame.players
                .filter(player => player.deviantUser?.id !== handler.deviantUser?.id && player.arsenalCards.length)
                .map(player => new Target((player.deviantUser?.nickname || 'X'), [(player.deviantUser?.id || 'X')]))
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (targets && targets.length === 1) {
                if (!targets[0].arsenalCards.length) {
                    return false
                }
                launcher.currentGame.choiceCallbacks.push({
                    sendChoices: () => {
                        const choices: Choice[] = [
                            ...targets[0].arsenalCards.map(card => ({ name: card.name, cardId: card.id, isVisible: true })),
                        ]

                        targets[0].websocket?.send(
                            {
                                title: 'show-choices',
                                data: {
                                    choices,
                                    name: 'CHOISI UN OBJET A DEFAUSSER'
                                }
                            }
                        )
                    },
                    choiceCallback: (choices: Choice[]) => {
                        let choice = choices[0]
                        if (!choice) {
                            const choices: Choice[] = [
                                ...targets[0].arsenalCards.map(card => ({ name: card.name, cardId: card.id })),
                            ]
                            choice = choices[Math.floor(Math.random() * choices.length)]
                        }

                        let card = targets[0].arsenalCards.find(cardItem => cardItem.id === choice.cardId)

                        if (card) {
                            game.discard.add(card)
                        }

                        return false
                    }
                })
            }

            return true
        }
    )
)

export default LoseAnItem