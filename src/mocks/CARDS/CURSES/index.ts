import BurningCards from "./BurningCards";
import ChickenOnTheHead from "./ChickenOnTheHead";
import CommonPeople from "./CommonPeople";
import DuckOfDoom from "./DuckOfDoom";
import HorizontalThunder from "./HorizontalThunder";
import IncomeTaxe from "./IncomeTaxe";
import InfamousMolasses from "./InfamousMolasses";
import LoseAnItem from "./LoseAnItem";
import MalignMirror from "./MalignMirror";
import MeltingArmor from "./MeltingArmor";
import Smite from "./Smite";
import SwapClass from "./SwapClass";
import SwapRace from "./SwapRace";
import SwapSex from "./SwapSex";
import TrulyObnoxiousCurse from "./TrulyObnoxiousCurse";
import Unclassed from "./Unclassed";

const CURSES = [
    LoseAnItem,
    LoseAnItem,
    LoseAnItem,
    BurningCards,
    MeltingArmor,
    InfamousMolasses,
    TrulyObnoxiousCurse,
    Unclassed,
    CommonPeople,
    SwapRace,
    SwapClass,
    MalignMirror,
    Smite,
    SwapSex,
    ChickenOnTheHead,
    DuckOfDoom,
    IncomeTaxe,
    HorizontalThunder,
]

export default CURSES