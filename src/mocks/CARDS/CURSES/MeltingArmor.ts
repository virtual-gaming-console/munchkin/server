import CardInstantiator from "../../../models/Card/CardInstantiator";
import Choice from "../../../models/Card/Choice";
import CurseCard from "../../../models/Card/Consumable/CurseCard";
import GearPlacement from "../../../models/Card/Gear/GearPlacement";
import Target from "../../../models/Card/Target";
import Game from "../../../models/Game";
import Player from "../../../models/Player/Player";

const MeltingArmor = new CardInstantiator(
    19,
    () => new CurseCard(
        19,
        "Fonte de l'armure!",
        (handler: Player) => {
            return handler.currentGame.players
                .filter(player => player.deviantUser?.id !== handler.deviantUser?.id && player.arsenalCards.find(card => card.placement === GearPlacement.ARMOR)).length
        },
        (handler: Player) => {
            return handler.currentGame.players
                .filter(player => player.deviantUser?.id !== handler.deviantUser?.id && player.arsenalCards.find(card => card.placement === GearPlacement.ARMOR))
                .map(player => new Target((player.deviantUser?.nickname || 'X'), [(player.deviantUser?.id || 'X')]))
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (targets && targets.length === 1) {
                const armorCards = targets[0].arsenalCards.filter(card => card.placement === GearPlacement.ARMOR)
                if (!armorCards.length) {
                    return false
                } else if (armorCards.length === 1) {
                    game.discard.add(armorCards[0])
                } else {
                    launcher.currentGame.choiceCallbacks.push({
                        sendChoices: () => {
                            const choices: Choice[] = [
                                ...armorCards.map(card => ({ name: card.name, cardId: card.id, isVisible: true })),
                            ]

                            targets[0].websocket?.send(
                                {
                                    title: 'show-choices',
                                    data: {
                                        choices,
                                        name: 'CHOISI UNE ARMURE A DEFAUSSER'
                                    }
                                }
                            )
                        },
                        choiceCallback: (choices: Choice[]) => {
                            let choice = choices[0]
                            if (!choice) {
                                const choices: Choice[] = [
                                    ...armorCards.map(card => ({ name: card.name, cardId: card.id })),
                                ]
                                choice = choices[Math.floor(Math.random() * choices.length)]
                            }

                            let card = targets[0].arsenalCards.find(cardItem => cardItem.id === choice.cardId)

                            if (card) {
                                game.discard.add(card)
                            }

                            return false
                        }
                    })
                }
            }

            return true
        }
    )
)

export default MeltingArmor