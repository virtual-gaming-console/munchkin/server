import CardInstantiator from "../../../models/Card/CardInstantiator";
import Choice from "../../../models/Card/Choice";
import CurseCard from "../../../models/Card/Consumable/CurseCard";
import Target from "../../../models/Card/Target";
import Game from "../../../models/Game";
import Player from "../../../models/Player/Player";

const BurningCards = new CardInstantiator(
    22,
    () => new CurseCard(
        22,
        "Cartes brûlantes!",
        (handler: Player) => {
            return handler.currentGame.players
                .filter(player => player.deviantUser?.id !== handler.deviantUser?.id && player.hand.cards.length).length
        },
        (handler: Player) => {
            return handler.currentGame.players
                .filter(player => player.deviantUser?.id !== handler.deviantUser?.id && player.hand.cards.length)
                .map(player => new Target((player.deviantUser?.nickname || 'X'), [(player.deviantUser?.id || 'X')]))
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (targets && targets.length === 1) {
                if (!targets[0].hand.cards.length) {
                    return false
                }

                const players = [game.nextPlayer(targets[0]), game.previousPlayer(targets[0])]
                for (let index = 0; index < (targets[0].hand.cards.length >= players.length ? players.length : targets[0].hand.cards.length); index++) {
                    const player = players[index];
                    launcher.currentGame.choiceCallbacks.push({
                        sendChoices: () => {
                            const choices: Choice[] = [
                                ...targets[0].hand.cards.map(card => ({ name: card.name, cardId: card.id, isVisible: false })),
                            ]

                            player.websocket?.send(
                                {
                                    title: 'show-choices',
                                    data: {
                                        choices,
                                        name: 'VOLE UNE CARTE DE ' + targets[0].deviantUser?.nickname
                                    }
                                }
                            )
                        },
                        choiceCallback: (choices: Choice[]) => {
                            let choice = choices[0]
                            if (!choice) {
                                const choices: Choice[] = [
                                    ...targets[0].hand.cards.map(card => ({ name: card.name, cardId: card.id })),
                                ]
                                choice = choices[Math.floor(Math.random() * choices.length)]
                            }

                            let card = targets[0].hand.cards.find(cardItem => cardItem.id === choice.cardId)

                            if (card) {
                                player.hand.add(card)
                            }

                            return false
                        }
                    })
                }

                return true
            }
        }
    )
)

export default BurningCards