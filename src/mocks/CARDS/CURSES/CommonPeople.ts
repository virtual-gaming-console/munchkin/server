import CardInstantiator from "../../../models/Card/CardInstantiator";
import CurseCard from "../../../models/Card/Consumable/CurseCard";
import Target from "../../../models/Card/Target";
import Game from "../../../models/Game";
import Player from "../../../models/Player/Player";
import Human from "../../../models/Player/PlayerRace/Human";

const CommonPeople = new CardInstantiator(
    24,
    () => new CurseCard(
        24,
        "Commun des mortels!",
        (handler: Player) => {
            return handler.currentGame.players
                .filter(player => player.deviantUser?.id !== handler.deviantUser?.id && player.races.length).length
        },
        (handler: Player) => {
            return handler.currentGame.players
                .filter(player => player.deviantUser?.id !== handler.deviantUser?.id && player.races.length)
                .map(player => new Target((player.deviantUser?.nickname || 'X'), [(player.deviantUser?.id || 'X')]))
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (targets && targets.length === 1) {
                if (!targets[0].races.length) {
                    return false
                }

                targets[0].race = new Human()
                targets[0].halvenRace = new Human()
            }

            return true
        }
    )
)

export default CommonPeople