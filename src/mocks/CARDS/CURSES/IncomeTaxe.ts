import CardInstantiator from "../../../models/Card/CardInstantiator";
import Choice from "../../../models/Card/Choice";
import CurseCard from "../../../models/Card/Consumable/CurseCard";
import ICard from "../../../models/Card/ICard";
import Target from "../../../models/Card/Target";
import TreasureCard from "../../../models/Card/TreasureCard";
import Game from "../../../models/Game";
import Player from "../../../models/Player/Player";

const IncomeTaxe = new CardInstantiator(
    15,
    () => new CurseCard(
        15,
        "Impôt sur le revenu!",
        (handler: Player) => {
            return handler.currentGame.players
                .filter(player => player.deviantUser?.id !== handler.deviantUser?.id).length
        },
        (handler: Player) => {
            return handler.currentGame.players
                .filter(player => player.deviantUser?.id !== handler.deviantUser?.id)
                .map(player => new Target((player.deviantUser?.nickname || 'X'), [(player.deviantUser?.id || 'X')]))
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (targets && targets.length === 1) {
                if (targets[0].arsenalCards.length + targets[0].hand.cards.filter(card => (card as TreasureCard).sellingPrice).length === 0) {
                    return false
                }
                launcher.currentGame.choiceCallbacks.push({
                    sendChoices: () => {
                        const choices: Choice[] = [
                            ...targets[0].arsenalCards.filter(card => card.sellingPrice).map(card => ({ name: card.name, cardId: card.id, isVisible: true })),
                            ...targets[0].hand.cards.filter(card => (card as TreasureCard).sellingPrice).map(card => ({ name: card.name, cardId: card.id, isVisible: true }))
                        ]

                        targets[0].websocket?.send(
                            {
                                title: 'show-choices',
                                data: {
                                    choices,
                                    name: 'CHOISI UN OBJET A DEFAUSSER'
                                }
                            }
                        )
                    },
                    choiceCallback: (choices: Choice[]) => {
                        let choice = choices[0]
                        if (!choice) {
                            const choices: Choice[] = [
                                ...targets[0].arsenalCards.filter(card => card.sellingPrice).map(card => ({ name: card.name, cardId: card.id })),
                                ...targets[0].hand.cards.filter(card => (card as TreasureCard).sellingPrice).map(card => ({ name: card.name, cardId: card.id, isVisible: true }))
                            ]
                            choice = choices[Math.floor(Math.random() * choices.length)]
                        }

                        let card: ICard | undefined = targets[0].arsenalCards.find(cardItem => cardItem.id === choice.cardId)
                        if (!card) {
                            card = targets[0].hand.cards.find(cardItem => cardItem.id === choice.cardId)
                        }

                        if (card) {
                            game.discard.add(card)
                            const soldPrice = (card as TreasureCard).sellingPrice
                            launcher.currentGame.players
                                .filter(player => player.deviantUser?.id !== targets[0].deviantUser?.id)
                                .forEach(player => {
                                    const allCards = [
                                        ...player.arsenalCards.filter(card => card.sellingPrice),
                                        ...player.hand.cards.filter(card => (card as TreasureCard).sellingPrice)
                                    ]
                                    if (allCards.map(cardItem => (cardItem as TreasureCard).sellingPrice).reduce((a, b) => a + b, 0) <= soldPrice) {
                                        allCards.forEach(card => {
                                            game.discard.add(card)
                                        })
                                        player.level--
                                    } else {
                                        launcher.currentGame.choiceCallbacks.push({
                                            sendChoices: () => {
                                                const choices: Choice[] = [
                                                    ...player.arsenalCards.map(card => ({ name: card.name, cardId: card.id, isVisible: true })),
                                                    ...player.hand.cards.filter(card => (card as TreasureCard).sellingPrice).map(card => ({ name: card.name, cardId: card.id, isVisible: true }))
                                                ]

                                                const msg = {
                                                    title: 'show-choices',
                                                    data: {
                                                        choices,
                                                        min: (card as TreasureCard).sellingPrice,
                                                        name: 'DEFAUSSE DES OBJETS POUR ' + (card as TreasureCard).sellingPrice + ' GOLDS'
                                                    }
                                                }
                                                console.log(msg)

                                                player.websocket?.send(
                                                    {
                                                        title: 'show-choices',
                                                        data: {
                                                            choices,
                                                            min: (card as TreasureCard).sellingPrice,
                                                            name: 'DEFAUSSE DES OBJETS POUR ' + (card as TreasureCard).sellingPrice + ' GOLDS'
                                                        }
                                                    }
                                                )
                                            },
                                            choiceCallback: (choices: Choice[]) => {
                                                const allCards = [
                                                    ...player.arsenalCards,
                                                    ...player.hand.cards.filter(card => (card as TreasureCard).sellingPrice)
                                                ]
                                                let cards = choices.map(choice => allCards.find(card => card.id === choice.cardId))
                                                if (!cards.length || cards.map(cardItem => (cardItem as TreasureCard).sellingPrice).reduce((a, b) => a + b, 0) < soldPrice) {
                                                    while (cards.map(cardItem => (cardItem as TreasureCard).sellingPrice).reduce((a, b) => a + b, 0) < soldPrice) {
                                                        const randomCard = allCards.splice(Math.floor(Math.random() * allCards.length), 1)[0]
                                                        cards.push(randomCard)
                                                    }
                                                }

                                                cards.forEach(card => {
                                                    if (card) {
                                                        game.discard.add(card)
                                                    }
                                                })

                                                return false
                                            }
                                        })
                                    }
                                })
                        }

                        return false
                    }
                })
            }

            return true
        }
    )
)

export default IncomeTaxe