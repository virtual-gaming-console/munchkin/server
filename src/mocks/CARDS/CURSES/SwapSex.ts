import MODIFIERS from "../../../models/Battle/MODIFIERS";
import CardInstantiator from "../../../models/Card/CardInstantiator";
import CurseCard from "../../../models/Card/Consumable/CurseCard";
import RemovalAction from "../../../models/Card/Consumable/RemovalAction";
import Target from "../../../models/Card/Target";
import Game from "../../../models/Game";
import Gender from "../../../models/Player/Gender";
import Player from "../../../models/Player/Player";

const SwapSex = new CardInstantiator(
    12,
    () => new CurseCard(
        12,
        "Boulversement hormonal excessif!",
        (handler: Player) => {
            return handler.currentGame.players
                .filter(player => player.deviantUser?.id !== handler.deviantUser?.id).length
        },
        (handler: Player) => {
            return handler.currentGame.players
                .filter(player => player.deviantUser?.id !== handler.deviantUser?.id)
                .map(player => new Target((player.deviantUser?.nickname || 'X'), [(player.deviantUser?.id || 'X')]))
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (targets && targets.length === 1) {
                targets[0].gender = targets[0].gender === Gender.FEMALE ? Gender.MALE : Gender.FEMALE
                const card = launcher.hand.cards.find(cardItem => cardItem.id === 25)
                if (card) {
                    targets[0].curses.add(card as CurseCard)
                }
            }

            return true
        },
        {
            modifier: MODIFIERS.SWAP_SEX(),
            removalAction: RemovalAction.AFTER_BATTLE
        }
    )
)

export default SwapSex