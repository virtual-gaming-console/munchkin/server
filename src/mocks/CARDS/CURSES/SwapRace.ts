import CardInstantiator from "../../../models/Card/CardInstantiator";
import CardType from "../../../models/Card/CardType";
import CurseCard from "../../../models/Card/Consumable/CurseCard";
import RaceCard from "../../../models/Card/Consumable/RaceCard";
import Target from "../../../models/Card/Target";
import Game from "../../../models/Game";
import Player from "../../../models/Player/Player";
import Human from "../../../models/Player/PlayerRace/Human";
import PlayerRaceType from "../../../models/Player/PlayerRace/PlayerRaceType";
import RACES from "../../../models/Player/PlayerRace/RACES";

const SwapRace = new CardInstantiator(
    11,
    () => new CurseCard(
        11,
        "Changement de race !",
        (handler: Player) => {
            return handler.currentGame.players
                .filter(player => player.deviantUser?.id !== handler.deviantUser?.id && player.races.find(raceType => raceType !== PlayerRaceType.HUMAN)).length
        },
        (handler: Player) => {
            return handler.currentGame.players
                .filter(player => player.deviantUser?.id !== handler.deviantUser?.id && player.races.find(raceType => raceType !== PlayerRaceType.HUMAN))
                .map(player => new Target((player.deviantUser?.nickname || 'X'), [(player.deviantUser?.id || 'X')]))
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (targets && targets.length === 1) {
                if (!targets[0].races.find(raceType => raceType !== PlayerRaceType.HUMAN)) {
                    return false
                } else {
                    const newRace = game.discard.cards.find(card => card.type === CardType.RACE)
                    const realRaces = targets[0].races.filter(raceType => raceType !== PlayerRaceType.HUMAN)
                    if (realRaces.length === 1 && targets[0].race.type === PlayerRaceType.HUMAN) {
                        targets[0].halvenRace = new Human()
                    }
                    targets[0].race = newRace ? new RACES[(newRace as RaceCard).value]() : new Human()
                    if (newRace) {
                        game.discard.remove(newRace)
                    }
                }
            }

            return true
        }
    )
)

export default SwapRace