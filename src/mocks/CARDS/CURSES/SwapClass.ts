import CardInstantiator from "../../../models/Card/CardInstantiator";
import CardType from "../../../models/Card/CardType";
import ClassCard from "../../../models/Card/Consumable/ClassCard";
import CurseCard from "../../../models/Card/Consumable/CurseCard";
import Target from "../../../models/Card/Target";
import Game from "../../../models/Game";
import Player from "../../../models/Player/Player";
import CLASSES from "../../../models/Player/PlayerClass/CLASSES";

const SwapClass = new CardInstantiator(
    10,
    () => new CurseCard(
        10,
        "Changement de classe !",
        (handler: Player) => {
            return handler.currentGame.players
                .filter(player => player.deviantUser?.id !== handler.deviantUser?.id && player.classes.length).length
        },
        (handler: Player) => {
            return handler.currentGame.players
                .filter(player => player.deviantUser?.id !== handler.deviantUser?.id && player.classes.length)
                .map(player => new Target((player.deviantUser?.nickname || 'X'), [(player.deviantUser?.id || 'X')]))
        },
        (game: Game, launcher: Player, targets?: Array<Player>, cardIds?: Array<number>, target?: Target) => {
            if (targets && targets.length === 1) {
                if (!targets[0].classes.length) {
                    return false
                } else {
                    const newClass = game.discard.cards.find(card => card.type === CardType.CLASS)
                    if (targets[0].classes.length === 1 && targets[0].superMunchkinClass) {
                        targets[0].superMunchkinClass = undefined
                    }
                    targets[0].class = newClass ? new CLASSES[(newClass as ClassCard).value]() : undefined
                    if (newClass) {
                        game.discard.remove(newClass)
                    }
                }
            }

            return true
        }
    )
)

export default SwapClass