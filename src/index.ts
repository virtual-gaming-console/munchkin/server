import compression from 'compression'
import express, { static as _static } from 'express'
import helmet from 'helmet'
import cookieParser from 'cookie-parser'
import methodOverride from 'method-override'
import { urlencoded, json } from 'body-parser'
import cors from 'cors'
import CONFIG from './config'
import WebSocket from 'ws'

const app = express()
const http = require('http').createServer(app)
const wss = new WebSocket.Server({ server: http });
const { StringDecoder } = require('string_decoder');
const decoder = new StringDecoder('utf8');

app.disable('x-powered-by');
app.use(cors())
app.use(compression())
app.use(helmet())
app.use(methodOverride('X-HTTP-Method-Override'))
app.use(cookieParser())
app.use(urlencoded({ extended: false }))
app.use(json())

app.get('/', (req, res) => {
    res.send('Hello ! This is the Munchkin server oO')
})

import GameController from './controllers/GameController'
import MyWebSocket from './models/WebSocket/MyWebSocket'

GameController.init(() => {
    wss.on('connection', socket => {
        const mySocket = new MyWebSocket(socket)

        socket.on('message', message => {
            const text = decoder.write(message)
            const json = JSON.parse(text)

            mySocket.lastMessageTime = Date.now()
            GameController.onMessage(mySocket, json)
        });
    });

    const interval = setInterval(() => {
        MyWebSocket.sockets.forEach((myWs) => {
            if (myWs.isAlive === false && (Date.now() - myWs.lastMessageTime) > 30000) {
                return GameController.onDisconnect(myWs)
            }

            myWs.isAlive = false
            myWs.socket.ping()
        });
    }, 30000)

    http.listen(CONFIG.port, () => {
        console.log(`Munchkin Game Server listening on port ${CONFIG.port}!`)
    })
})