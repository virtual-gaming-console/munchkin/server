import DEV_CONFIG from './config.dev'
import PROD_CONFIG from './config.prod'

const CONFIG = process && process.env && process.env.NODE_ENV && process.env.NODE_ENV.trim() === "development" ? DEV_CONFIG : PROD_CONFIG

export default CONFIG