const CONFIG = {
  host: 'server.munchkin.playdeviant.com',
  protocol: 'https',
  port: 8081,
  // clientUrl: 'https://playdeviant.com'
}

export default CONFIG