FROM node:latest

RUN mkdir server
WORKDIR /server

COPY package.json /server/package.json
RUN npm install --only=prod

COPY . /server

EXPOSE 8081

CMD [ "npm", "start" ]